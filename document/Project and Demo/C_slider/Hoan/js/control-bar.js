$(document).ready(function(){
  var controls =  $('input, select'),
    opt = {};

  $('form.control-bar').on('submit', function(e){
    e.preventDefault();
    controls.each(function(){
      var name = $(this).attr('name'),
        value = $(this).val();

      switch(name) {
        case 'effect':
          opt.effect = value || 'slide';
        break;
        case 'slide':
          opt.slide =  parseInt(value) || 0 ;
        break;
        case 'duration':
          opt.duration =  parseInt(value) || 1000; 
        break;
        case 'interval':
          opt.interval = parseInt(value) || 5000;
        break;
        case 'autoplay':
          opt.autoplay = $(this).value || false;
        break;
      }
    });

     $('[data-slider]')['slider']('changeOption', opt);
    return false;
  });
  
});