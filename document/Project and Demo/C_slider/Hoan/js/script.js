/**
 *  @name -slider
 *  @description create responsive slider
 *  @version 1.0
 *  @options
 *    effect: default effect when initial slider
 *    duration: animate time of each effect
 *    autoplay: identify whether slider has automatic
 *    interval: switch time between slides
 *  @events
 *  @methods
 *    init
 *    changeEffect
 *    goTo
 *    destroy
 */
(function($, window, undefined) {
  var pluginName = 'slider';

  var Effects = {
    FADE: 'fade',
    SLIDE: 'slide',
    SHOW: 'show'
  };

  var Keys = {
    LEFT: 37,
    RIGHT: 39
  };

  var Directions = {
    LEFT: 'left',
    RIGHT: 'right'
  };

  var PADDING_SLIDER = 2;
  var LIMIT_SLIDE = 50;

  function Plugin(element, options) {
    this.element = $(element);
    this.options = $.extend({}, $.fn[pluginName].defaults, this.element.data(), options);
    this.init();
  }

  var fakeSlider = function() {
    var that = this,
      slider = that.element,
      wrapper = slider.wrap('<div class = "' + pluginName + '"></div>').parent(),
      indicators = wrapper.append('<ul class = "indicators">').find('.indicators'),
      controls = wrapper.append('<ul class = "controls">').find('.controls');

    for (var i = 0, total = slider.find('li').length; i < total; i++) {
      indicators.append('<li ' + (!i ? 'class = "active">' : '>'));
    }

    if (!$.isEmptyObject(L10n.control)) {
      for (var key in L10n.control) {
        controls.append('<li class = "' + L10n.control[key] + '">');
      }
    }
    return slider;
  };

  var isInt = function(value) {
    var that = this,
      totalSlide = that.vars.slides.length;

    return $.isNumeric(value) && value === Math.floor(value) && value >= 0 &&
      value < totalSlide;
  };

  var isString = function(value) {
    return $.type(value) === 'string' && (value === L10n.control.prev ||
      value === L10n.control.next);
  };

  var getIndex = function(action) {
    var that = this,
      current = that.vars.currentIndex,
      total = that.vars.slides.length,
      next = 0;

    switch (action) {
      case L10n.control.next:
        next = (current + 1 >= total) ? 0 : current + 1;
        break;
      case L10n.control.prev:
        next = (current - 1 < 0) ? total - 1 : current - 1;
        break;
    }
    return {
      current: current,
      next: next
    };
  };

  var slideLeft = function(index) {

    var that = this,
      opt = that.options,
      slides = that.vars.slides;

    slides
      .eq(index.current)
      .addClass('out-prev')
      .animate({
        left: -(100 + PADDING_SLIDER) + '%'
      }, {
        duration: opt.duration,
        queue: false,
        complete: function() {
          $(this).removeClass().removeAttr('style');
        }
      });

    slides
      .eq(index.next)
      .addClass('in-prev')
      .animate({
        right: 0
      }, {
        duration: opt.duration,
        queue: false,
        complete: function() {
          $(this).removeClass().removeAttr('style').addClass('active');
        }
      });
  };

  var slideRight = function(index) {
    var that = this,
      opt = that.options,
      slides = that.vars.slides;

    slides
      .eq(index.current).addClass('out-next')
      .animate({
        right: -(100 + PADDING_SLIDER) + '%'
      }, {
        duration: opt.duration,
        queue: false,
        complete: function() {
          $(this).removeClass().removeAttr('style');
        }
      });

    slides.eq(index.next).addClass('in-next')
      .animate({
        left: 0
      }, {
        duration: opt.duration,
        queue: false,
        complete: function() {
          $(this).removeClass().removeAttr('style').addClass('active');
        }
      });
  };

  var changeSlide = function(index, direction) {
    var that = this,
      opt = that.options,
      effect = opt.effect,
      slides = that.vars.slides,
      indicator = that.vars.indicators.children('li');

    switch (effect) {
      case Effects.FADE:
        slides
          .fadeOut(opt.duration)
          .eq(index.next)
          .fadeIn(opt.duration)
          .addClass('active');
        break;
      case Effects.SLIDE:
        if (direction === Directions.LEFT) {
          slideLeft.call(that, index);
        } else {
          slideRight.call(that, index);
        }
        break;
      default:
        slides
          .hide()
          .eq(index.next)
          .show()
          .addClass('active');
    }
    that.vars.currentIndex = index.next;
    indicator.removeClass('active').eq(index.next).addClass('active');

  };

  var startTimer = function() {
    var that = this,
      opt = that.options,
      timer = null;

    if (opt.autoplay) {
      timer = setInterval(function() {
        that.goTo('next');
      }, opt.interval);
    }
    return timer;
  };

  var resetTimer = function(timer) {
    var that = this;

    clearInterval(timer);
    return startTimer.call(that);
  };

  var getSize = function() {
    var that = this,
      images = that.vars.slides.children('img'),
      size = {
        width: 0,
        height: 0
      };

    images.each(function() {
      size.width = Math.max($(this).width(), size.width);
      size.height = Math.max($(this).height(), size.height);
    });

    return size;

  };

  var getCoordinate = function(e) {
    return (e.clientX || e.originalEvent.clientX);
  };

  var endMoveLeft = function(el, e) {
    var that = this,
      opt = that.options,
      index = getIndex.call(that, 'next');

    if (that.vars.pointer.percent > LIMIT_SLIDE) {
      changeSlide.call(that, index, Directions.LEFT);
    } else {
      el.animate({
        'left': '0'
      }, {
        duration: opt.duration,
        queue: false,
        complete: function() {
          el.removeClass().removeAttr('style').addClass('active');
        }
      });

      that.vars.slides.eq(index.next).animate({
        'right': -(100 + PADDING_SLIDER) + '%'
      }, {
        duration: opt.duration,
        queue: false,
        complete: function() {
          $(this).removeClass().removeAttr('style');
        }
      });
    }
  };

  var endMoveRight = function(el, e) {
    var that = this,
      opt = that.options,
      index = getIndex.call(that, 'prev');
    if (that.vars.pointer.percent > LIMIT_SLIDE) {
      changeSlide.call(that, index, Directions.RIGHT);
    } else {
      el.animate({
        'right': '0'
      }, {
        duration: opt.duration,
        queue: false,
        complete: function() {
          el.removeClass().removeAttr('style').addClass('active');
        }
      });

      that.vars.slides.eq(index.next).animate({
        left: -(100 + PADDING_SLIDER) + '%'
      }, {
        duration: opt.duration,
        queue: false,
        complete: function() {
          $(this).removeClass().removeAttr('style');
        }
      });
    }
  };


  var touchStart = function(el, e) {
    var that = this;
    e.preventDefault();
    el.addClass('grabbing');
    that.vars.pointer.startPos = getCoordinate(e);
  };

  var moveRight = function(el, e) {
    var that = this,
      index = getIndex.call(that, 'prev'),
      currentPos = getCoordinate(e),
      size = getSize.call(that),
      distance = currentPos - that.vars.pointer.startPos;

    that.vars.pointer.percent = Math.floor((distance / size.width) * 100);

    var percentShift = (100 + PADDING_SLIDER) - that.vars.pointer.percent;

    el.css('right', -that.vars.pointer.percent + '%');

    that.vars.slides.eq(index.next).css({
      'display': 'block',
      'left': -percentShift + '%'
    });
  };

  var moveLeft = function(el, e) {
    var that = this,
      index = getIndex.call(that, 'next'),
      currentPos = getCoordinate(e),
      distance = that.vars.pointer.startPos - currentPos,
      size = getSize.call(that);

    that.vars.pointer.percent = Math.floor((distance / size.width) * 100);

    var percentShift = (100 + PADDING_SLIDER) - that.vars.pointer.percent;

    el.css('left', -that.vars.pointer.percent + '%');

    that.vars.slides.eq(index.next).css({
      'right': -(percentShift) + '%',
      'display': 'block'
    });

  };

  Plugin.prototype = {
    init: function() {
      var that = this,
        opt = this.options,
        slider = fakeSlider.call(that),
        wrapper = slider.parent(),
        isTouchStart = false,
        doc = $(document),
        win = $(window);

      that.vars = {
        slides: slider.find('li'),
        controls: wrapper.find('.controls'),
        indicators: wrapper.find('.indicators'),
        currentIndex: slider.find('li.active').index(),
        pointer: {
          startPos: 0,
          endPos: 0,
          percent: 0
        }
      };

      var timer = startTimer.call(that);

      that.vars.controls
        .on('click.' + pluginName, '.prev', function() {
          that.goTo('prev');
          if (!timer) {
            resetTimer.call(that);
          }
        })
        .on('click.' + pluginName, '.next', function() {
          that.goTo('next');
          if (!timer) {
            resetTimer.call(that);
          }
        });

      that.vars.slides.children('img').on('load.' + pluginName, function() {
        var size = getSize.call(that);
        $(this).css('height', size.height + 'px');
        $(this).parents('.' + pluginName).css('height', size.height + 'px');
      });

      that.vars.slides.each(function() {
        var el = $(this),
          direction = '';

        if(opt.effect !== Effects.SLIDE) {
          return false;
        }

        $(this)
          .on('touchstart mousedown', function(e) {
            touchStart.call(that, el, e);
            isTouchStart = true;
          })
          .on('touchmove mousemove', function(e) {
            e.preventDefault();

            var startPos = that.vars.pointer.startPos,
              currentPos = getCoordinate(e);

            if (isTouchStart) {
              if (currentPos > startPos) {
                moveRight.call(that, el, e);
                direction = Directions.RIGHT;
              } else {
                moveLeft.call(that, el, e);
                direction = Directions.LEFT;
              }
            }
          })
          .on('touchend mouseup', function(e) {
            e.preventDefault();
            el.removeClass('grabbing');
            that.vars.pointer.end = getCoordinate(e);

            if (direction === Directions.LEFT) {
              endMoveLeft.call(that, el, e);
            } else {
              endMoveRight.call(that, el, e);
            }
            isTouchStart = false;
          });
      });



      that.vars.indicators.on('click.' + pluginName, 'li', function() {
        if (!$(this).hasClass('active')) {
          that.goTo($(this).index());
          if (!timer) {
            resetTimer.call(that);
          }
        }
      });

      doc.on('keydown.' + pluginName, function(e) {
        switch (e.which) {
          case Keys.LEFT:
            that.goTo('prev');
            break;
          case Keys.RIGHT:
            that.goTo('next');
            break;
        }
      });

      win.on('resize.' + pluginName, function() {
        var size = getSize.call(that);
        that.vars.slides.parents('.' + pluginName).css('height', size.height + 'px');
      });

    },
    goTo: function(target) {
      var that = this,
        index = null,
        direction = 'right';

      switch (true) {
        case isInt.call(that, target):
          index = {
            current: that.vars.currentIndex,
            next: target
          };
          direction = (index.next > index.current) ? Directions.RIGHT : Directions.LEFT;
          changeSlide.call(that, index, direction);
          break;
        case isString(target):
          index = getIndex.call(that, target);
          direction = (target === L10n.control.next) ? Directions.RIGHT : Directions.LEFT;
          changeSlide.call(that, index, direction);
          break;
      }
    },
    changeEffect: function(effect) {
      var that = this;
      this.options.effect = effect;
      resetTimer.call(that);
    },
    changeOption: function(params) {

      var that = this,
        opt = this.options,
        userOpt = {};

      if(typeof(opt) === 'object' && !$.isEmptyObject(opt)) {
        userOpt = {
          effect: params.effect || opt.effect,
          slide: parseInt(params.slide) || opt.slide,
          duration: parseInt(params.duration) || opt.duration,
          interval: parseInt(params.interval) || opt.duration,
          autoplay: params.autoplay || opt.autoplay
        };
      }
      that.changeEffect(userOpt.effect);
      that.goTo(userOpt.slide);
      if(!!userOpt.autoplay) {
        startTimer.call(that);
      }
      this.options = userOpt;
    },
    destroy: function() {

      var that = this;
      that.vars.controls.off('click.' + pluginName);
      that.vars.slides.children().off();
      that.vars.indicators.off('click.' + pluginName);
      that.vars.slides.children('img').off('load.' + pluginName);
      $(document).off('keydow.' + pluginName);
      $(window).off('resize.' + pluginName);
      $.removeData(this.element[0], pluginName);
    }
  };


  $.fn[pluginName] = function(options, params) {
    return this.each(function() {
      var instance = $.data(this, pluginName);
      if (!instance) {
        $.data(this, pluginName, new Plugin(this, options));
      } else if (instance[options]) {
        instance[options](params);
      } else {
        window.console && console.log(options ? options + ' method is not exists in ' + pluginName : pluginName + ' plugin has been initialized');
      }
    });
  };

  $.fn[pluginName].defaults = {
    effect: 'slide',
    slide: 0,
    duration: 1000,
    autoplay: false,
    interval: 5000
  };

  $(function() {
    $('[data-' + pluginName + ']')[pluginName]();
  });

}(jQuery, window));
