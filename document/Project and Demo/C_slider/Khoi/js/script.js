var getInitNormal = function() {
  $('.init-btn').attr('disabled', 'true');
  $('.destroy-btn').removeAttr('disabled');
  $('#demo')['slider-plugin']({effect: 'normal'});
};
var getInitFade = function() {
  $('.init-btn').attr('disabled', 'true');
  $('.destroy-btn').removeAttr('disabled');
  $('#demo')['slider-plugin']({effect: 'fade'});
};
var getInitSlide = function() {
  $('.init-btn').attr('disabled', 'true');
  $('.destroy-btn').removeAttr('disabled');
  $('#demo')['slider-plugin']({effect: 'slide'});
};
var getDestroy = function() {
  $('.init-btn').removeAttr('disabled');
  $('.destroy-btn').attr('disabled', 'true');
  $('#demo')['slider-plugin']('destroy');
};
var changeEffect = function(val) {
  if(val !== '') {
    $('#demo')['slider-plugin']('changeEffect', val);
  }
};
var goTo = function() {
  var value = $('#goto').val();
  $('#demo')['slider-plugin']('goTo', value);
};

/**
 *  @name slider-plugin
 *  @description description
 *  @version 1.0
 *  @options
 *    effect
 *    duration
 *    autoplay
 *    interval
 *  @events
 *
 *  @methods
 *    init
 *    goTo
 *    changeEffect
 *    destroy
 */
;(function($, window, undefined) {
  var KeyCodes = {
    LEFT_ARROW: 37,
    RIGHT_ARROW: 39
  };
  var Effects = {
    FADE: 'fade',
    SLIDE: 'slide',
    NORMAL: 'normal'
  };
  var PointType = {
    TOUCH: 'touch',
    MOUSE: 'mouse'
  };
  var ClassNames = {
    WRAPPER: 'slider-wrapper',
    ITEM: 'slider-item',
    BUTTON: 'slider-btn',
    BTN_LEFT: 'slider-btn-left',
    BTN_RIGHT: 'slider-btn-right',
    BULLET_WR: 'slider-bullet-wrapper',
    BULLET: 'slider-bullet',
    TEMP: 'temp-img',
    LIST: 'slider-list'
  };
  var AttrNames = {
    INDEX: 'slider-index'
  };

  var pluginName = 'slider-plugin';
  var wrapper = '<div class="' + ClassNames.WRAPPER + '"></div>',
    slideItem = '<div class="' + ClassNames.ITEM + '"></div>',
    btnLeftCtrl = '<a class="' + ClassNames.BUTTON + ' ' + ClassNames.BTN_LEFT +'"></a>',
    btnRightCtrl = '<a class="' + ClassNames.BUTTON + ' ' + ClassNames.BTN_RIGHT +'"></a>',
    bulletWrapper = '<div class="' + ClassNames.BULLET_WR + '"></div>';
  var curIdx = 0,
    nextIdx = 0,
    prevIdx = 0,
    listLength = 0;
  var isSliding = false,
    isMouseDown = false;
  var srcPoint = null,
    desPoint = null;
  var sliderInterval = null;

  var createElements = function() {
    this.btnLeftCtrl.html(L10n.IconCode.LEFT_ARROW);
    this.btnRightCtrl.html(L10n.IconCode.RIGHT_ARROW);
    this.wrapper
      .append(this.btnLeftCtrl)
      .append(this.btnRightCtrl)
      .append(this.bulletWrapper);
    this.element.append(this.slideItem);
  };

  var initLayers = function() {
    var el = this.element;

    listLength = this.element.find('img').length;
    if(listLength < 1) {return null;}

    for(var i = 0; i < listLength; i++) {
      var bullet = '<span class="' + ClassNames.BULLET + '"></span>';
      this.bulletWrapper.append(bullet);
    }

    el.find('img').each(function() {
      $(this).attr(AttrNames.INDEX, $(this).index());
    });
    el.find('[' + AttrNames.INDEX + '="' + curIdx + '"]').appendTo(this.slideItem);
    if(listLength < 2) {return null;}

    if(this.options.effect === Effects.SLIDE) {
      setSlideLayers.call(this);
    }
    resizeSlider.call(this);
  };

  var setSlideLayers = function() {
    var el = this.element;

    this.slideItem.addClass('slide');
    nextIdx = curIdx + 1 < listLength ? curIdx + 1 : 0;
    el.find('[' + AttrNames.INDEX + '="' + nextIdx + '"]').appendTo(this.slideItem);

    if(listLength > 2) {
      prevIdx = curIdx - 1 < 0 ? listLength - 1 : curIdx - 1;
      el.find('[' + AttrNames.INDEX + '="' + prevIdx + '"]').prependTo(this.slideItem);
    }
    else {
      cloneImg.call(this, curIdx);
      cloneImg.call(this, nextIdx);
    }
  };

  var cloneImg = function(idx) {
    this.element.find('[' + AttrNames.INDEX + '="' + idx + '"]').clone()
    .addClass(ClassNames.TEMP)
    .attr(AttrNames.INDEX, listLength)
    .appendTo(this.slideItem);
    listLength++;
  };

  var resizeSlider = function() {
    var el = this.element;
    var ratio = this.options.height / this.options.width;
    var elHeight = el.outerWidth() * ratio;
    el.css('height', elHeight)
    .find('img').each(function() {
      resizeImg($(this), ratio);
    });
  };

  var resizeImg = function(el, ratio) {
    var imgRatio = el.height() / el.width();
    if(imgRatio <= ratio) {
      el.css({
        'width': '100%', 'height': 'auto'
      });
    }
    else {
      el.css({
        'width': 'auto', 'height': '100%'
      });
    }
  };

  var syncImages = function(idx) {
    this.slideItem.children().css('left', '').end()
    .find('[' + AttrNames.INDEX + '!="' + idx + '"]').css('left','').prependTo(this.element);

    if(this.options.effect === Effects.SLIDE) {
      addNext.call(this, idx);
      addPrev.call(this, idx);
    }
    isSliding = false;
  };

  var addNext = function(idx) {
    curIdx = parseInt(idx);
    nextIdx = curIdx + 1 < listLength ? curIdx + 1 : 0;
    this.element.find('[' + AttrNames.INDEX + '="' + nextIdx + '"]').appendTo(this.slideItem);
  };

  var addPrev = function(idx) {
    curIdx = parseInt(idx);
    prevIdx = curIdx - 1 < 0 ? listLength - 1 : curIdx - 1;
    this.element.find('[' + AttrNames.INDEX + '="' + prevIdx + '"]').prependTo(this.slideItem);
  };

  var goNext = function(dragX) {
    if(listLength < 1) {return null;}

    if(this.options.effect === Effects.SLIDE) {
      var width = this.slideItem.innerWidth();
      var idx = this.slideItem.children().last().attr(AttrNames.INDEX);
      isSliding = true;
      if(Math.abs(dragX) > 0) {width += dragX;}

      var effect = function() {
        return this.slideItem.children().animate({
          left: '-=' + width
        }, this.options.duration, 'swing');
      }.call(this);

      $.when(effect).done(function() {
        syncImages.call(this, idx);
      }.bind(this));
      indexBullet.call(this, idx);
      return null;
    }
  };

  var goPrev = function(dragX) {
    if(listLength < 1) {return null;}

    if(this.options.effect === Effects.SLIDE) {
      var width = this.slideItem.innerWidth();
      var idx = this.slideItem.children().first().attr(AttrNames.INDEX);
      isSliding = true;
      if(Math.abs(dragX) > 0) {width -= dragX;}

      var effect = function() {
        return this.slideItem.children().animate({
          left: '+=' + width
        }, this.options.duration, 'swing');
      }.call(this);

      $.when(effect).done(function() {
        syncImages.call(this, idx);
      }.bind(this));
      indexBullet.call(this, idx);
      return null;
    }
  };

  var onTouchDownHandle = function(e, type) {
    if(isSliding || this.options.effect !== Effects.SLIDE) {return null;}
    var posX;
    if(type === PointType.TOUCH) {
      posX = e.originalEvent.touches[0].clientX;
      srcPoint = posX;
      desPoint = posX;
    }
    else {
      isMouseDown = true;
      posX = e.pageX;
      srcPoint = posX;
      desPoint = posX;
    }
  };

  var onTouchMoveHandle = function(e, type) {
    if(isSliding || this.options.effect !== Effects.SLIDE) {return null;}
    if(type === PointType.MOUSE && !isMouseDown) {return null;}

    desPoint = type === PointType.TOUCH ? e.originalEvent.touches[0].clientX : e.pageX;
    var eq = desPoint - srcPoint;
    var width = this.slideItem.innerWidth();
    var arr = this.slideItem.children();
    var count = arr.length;
    for(var i = 0; i < count; i++) {
      $(arr[i]).css({
        'left': width * (i-1) + eq + width / 2,
      });
    }
  };

  var onTouchUpHandle = function() {
    if(isSliding || this.options.effect !== Effects.SLIDE) {return null;}
    var eq = desPoint - srcPoint;
    if(eq > 80) {
      goPrev.call(this, eq);
    }
    else if(eq < -80) {
      goNext.call(this, eq);
    }
    else {
      var width = this.slideItem.innerWidth();
      var arr = this.slideItem.children();
      for(var i = 0; i < arr.length; i++) {
        $(arr[i]).animate({
          'left': width * (i-1) + width / 2,
        }, 100);
      }
    }
  };

  var indexBullet = function(idx) {
    var listBullet = this.bulletWrapper.find('.slider-bullet').removeClass('current');
    var curBullet = listBullet.get(idx);
    $(curBullet).addClass('current');
  };

  function Plugin(element, options) {
    this.element = $(element);
    this.wrapper = null;
    this.slideItem = null;
    this.btnLeftCtrl = null;
    this.btnRightCtrl = null;
    this.bulletWrapper = null;

    this.options = $.extend({}, $.fn[pluginName].defaults, this.element.data(), options);
    this.init();
  }

  Plugin.prototype = {
    init: function() {
      var that = this;

      curIdx = 0;
      this.element.addClass(ClassNames.LIST).wrap(wrapper);
      this.wrapper = this.element.parent();
      this.btnLeftCtrl = $(btnLeftCtrl);
      this.btnRightCtrl = $(btnRightCtrl);
      this.bulletWrapper = $(bulletWrapper);
      this.slideItem = $(slideItem);

      this.wrapper.attr('tabindex', 0);
      createElements.call(this);
      resizeSlider.call(this);
      initLayers.call(this);
      indexBullet.call(this, 0);

      sliderInterval = setInterval(function() {
        if(!isSliding) {
          this.goTo('next');
        }
      }.bind(this), this.options.interval);

      $(window).resize(function() {
        resizeSlider.call(that);
      });
      this.element.find('img').load(function() {
        var ratio = that.options.height / that.options.width;
        resizeImg($(this), ratio);
      });
      this.btnLeftCtrl.on('click.slider', function() {
        if(!isSliding) {
          that.goTo('prev');
        }
      });
      this.btnRightCtrl.on('click.slider', function() {
        if(!isSliding) {
          that.goTo('next');
        }
      });
      this.bulletWrapper.find('.slider-bullet').on('click.slider', function() {
        if(!isSliding) {
          that.goTo($(this).index());
        }
      });
      this.wrapper.on('keydown.slider', function(e) {
        if(e.keyCode === KeyCodes.LEFT_ARROW && !isSliding) {
          that.goTo('prev');
        }
        else if(e.keyCode === KeyCodes.RIGHT_ARROW && !isSliding) {
          that.goTo('next');
        }
      });
      this.slideItem
      .on('touchstart.slider', function(e) {
        onTouchDownHandle.call(that, e, PointType.TOUCH);
      })
      .on('touchmove.slider', function(e) {
        onTouchMoveHandle.call(that, e, PointType.TOUCH);
      })
      .on('touchend.slider', function(e) {
        onTouchUpHandle.call(that);
      })
      .on('mousedown.slider', function(e) {
        onTouchDownHandle.call(that, e, PointType.MOUSE);
        e.preventDefault();
      })
      .on('mousemove.slider', function(e) {
        onTouchMoveHandle.call(that, e, PointType.MOUSE);
        e.preventDefault();
      })
      .on('mouseup.slider', function(e) {
        isMouseDown = false;
        onTouchUpHandle.call(that);
        e.preventDefault();
      });
    },

    goTo: function(params) {
      var that = this;
      var idx = null;

      if(listLength === 1 || isSliding) {
        return;
      }

      if(params === 'prev') {
        if(this.options.effect === Effects.SLIDE) {
          return goPrev.call(this);
        }
        idx = curIdx - 1 < 0 ? listLength - 1 : curIdx - 1;
      }
      else if(params === 'next') {
        if(this.options.effect === Effects.SLIDE) {
          return goNext.call(this);
        }
        idx = curIdx + 1 < listLength ? curIdx + 1 : 0;
      }
      else if(parseInt(params) >= 0 && parseInt(params) < listLength) {
        idx = parseInt(params);
        if(this.options.effect === Effects.SLIDE) {
          if(idx > curIdx) {
            this.slideItem.children().last().prependTo(that.element);
            this.element.find('[' + AttrNames.INDEX + '="' + idx + '"]').appendTo(that.slideItem);
            goNext.call(this);
          }
          else if(idx < curIdx) {
            this.slideItem.children().first().prependTo(that.element);
            this.element.find('[' + AttrNames.INDEX + '="' + idx + '"]').prependTo(that.slideItem);
            goPrev.call(this);
          }
          return;
        }
      }
      else {
        return;
      }

      this.element.find('[' + AttrNames.INDEX + '="' + idx + '"]').prependTo(this.slideItem);
      if(this.options.effect === Effects.FADE) {
        isSliding = true;
        this.slideItem.find('[' + AttrNames.INDEX + '="' + curIdx + '"]')
        .fadeOut(that.options.duration, function() {
          var img = $(this);
          img.prependTo(that.element);
          img.css('display', '');
          isSliding = false;
        });
      }
      else {
        this.slideItem.find('[' + AttrNames.INDEX + '="' + curIdx + '"]').prependTo(that.element);
      }
      curIdx = idx;
      indexBullet.call(this, idx);
    },
    changeEffect: function(params) {
      var that = this;
      var els = this.slideItem.find('[' + AttrNames.INDEX + '!="' + curIdx + '"]');
      if(params === this.options.effect || isSliding) {
        return;
      }
      if(params === Effects.FADE || params === Effects.NORMAL) {
        this.options.effect = params;
        this.slideItem.removeClass('slide');
        $(els).prependTo(that.element);
        this.element.find('.' + ClassNames.TEMP).remove();

        curIdx = parseInt(this.slideItem.find('img').first().attr(AttrNames.INDEX));
        listLength = this.element.find('img').length;
        if(this.slideItem.children().length === 0) {
          curIdx = 0;
          that.element.find('[' + AttrNames.INDEX + '="0"]').appendTo(that.slideItem);
        }
        return;
      }
      if(params === Effects.SLIDE) {
        this.options.effect = params;
        setSlideLayers.call(this);
      }

    },
    destroy: function() {
      clearInterval(sliderInterval);
      this.slideItem.children().prependTo(this.element);
      this.slideItem.remove();
      this.element.find('.' + ClassNames.TEMP).remove().end()
      .parent().find('.' + ClassNames.BUTTON).remove().end()
      .find('.' + ClassNames.BULLET_WR).remove();
      this.element.unwrap();

      $.removeData(this.element[0], pluginName);
    }
  };

  $.fn[pluginName] = function(options, params) {
    return this.each(function() {
      var instance = $.data(this, pluginName);
      if (!instance) {
        $.data(this, pluginName, new Plugin(this, options));
      } else if (instance[options]) {
        instance[options](params);
      } else {
        window.console && console.log(options ? options + ' method is not exists in ' + pluginName : pluginName + ' plugin has been initialized');
      }
    });
  };

  $.fn[pluginName].defaults = {
    width: 800,
    height: 300,
    effect: 'slide',
    duration: 500,
    autoplay: true,
    interval: 5000
  };

  $(function() {
    $('[data-' + pluginName + ']')[pluginName]();
  });
}(jQuery, window));
