/**
 *  @name custom-slider
 *  @description create a slideshow
 *  @version 1.0
 *  @options
 *    autoplay: slideshow plays automatic when loaded, default is false
 *    effect: effect to transition slides, default is 'slide'
 *    duration: timelong when transition between two slides, default is 500
 *    interval: timelong to play automatic, default is 5000
 *  @events
 *    onSlideChanges: fires when slideshow starts changing
 *    onSlideChanged: fires when slideshow finishs changing
 *    onEffectChanged: fires when slideshow changed current effect
 *  @methods
 *    init
 *    goto(index): jump to a special slide, index('next'/ 'previous'/ a number between 0 and total slides)
 *    changeEffect(effect): change current effect
 *    destroy
 */
;(function($, window, undefined) {
  var pluginName = 'custom-slider',
    doc = $(document);

  var SWIPE = 50;
  var SEEK_NEXT = 1;
  var SEEK_PREVIOUS = -1;
  var EASING_GRAPH = 'easeOutCubic';

  var Selectors = {
    SLIDE: 'slider',
    NAV: 'navigator',
    NAV_CONTROL: 'nav-control',
    NAV_NEXT: 'nav-next',
    NAV_PREVIOUS: 'nav-previous',
    NAV_PAGE: 'nav-paginator',
    BULLET: 'page-bullet',
    DOWN_STT: 'down',
    ACTIVED_STT: 'actived'
  };
  var Effects = {
    SLIDE: 'slide',
    SLIDE_VERTICAL: 'slideVertical',
    FADE: 'fade',
    SHOW: 'show'
  };
  var Keys = {
    LEFT: 37,
    UP: 38,
    RIGHT: 39,
    DOWN: 40
  };
  var Navigations = {
    NEXT: 'next',
    PREVIOUS: 'previous'
  };

  var boundary = function(val, min, max) {
    if (val <= min) { return min; }
    if (val >= max) { return max; }
    return val;
  };
  var createNavPage = function() {
    var slides = this.element.find('img');
    var content = '';
    slides.each(function(index) {
      content += '<div class="' + Selectors.BULLET + '" title="Slide ' + (index + 1) + '"></div>';
    });
    return content;
  };
  var createFakeEl = function() {
    var that = this;
    var mTag = this.element[0];
    var navPage = createNavPage.call(that);
    var content =
      '<div class="' + Selectors.NAV_CONTROL + '" id="' + Selectors.NAV_NEXT + '" title="' + L10n.Titles.NEXT + '"></div>' +
      '<div class="' + Selectors.NAV_CONTROL + '" id="' + Selectors.NAV_PREVIOUS + '" title="' + L10n.Titles.PREVIOUS + '"></div>' +
      '<div class="' + Selectors.NAV_CONTROL + '" id="' + Selectors.NAV_PAGE + '">' +
        navPage +
      '</div>';
    that.element.append(content).find('img').each(function() {
      $(this).wrap('<div class="' + Selectors.SLIDE + '">');
    });
    mTag.tabIndex = mTag.tabIndex ? mTag.tabIndex : -1;
  };
  var resizeHandler = function() {
    var wParent = this.element.parent().width(),
      bullets = this.vars.bullets,
      nextBtn = this.vars.next,
      prevBtn = this.vars.previous,
      width = 0, height = 0,
      slideShow = this.vars.slideShow;

    var wPaginator = bullets.length * bullets.eq(0).outerWidth(true);
    var hPaginator = bullets.outerWidth(true);

    slideShow.ratio = boundary(wParent / slideShow.width, 0, 1);
    width = slideShow.ratio * slideShow.width;
    height = slideShow.ratio * slideShow.height;

    this.element.css({
      'width': width,
      'height': height
    });
    nextBtn.css({
      'top': 0.5 * (height - nextBtn.height())
    });
    prevBtn.css({
      'top': 0.5 * (height - prevBtn.height())
    });
    bullets.parent().css({
      'width': wPaginator,
      'height': hPaginator,
      'left': 0.5 * (width - wPaginator),
    });
  };
  var showNavigator = function() {
    this.vars.next.animate({ 'right': 0 });
    this.vars.previous.animate({ 'left': 0 });
    this.vars.paginator.animate({ 'bottom': 16 });
  };
  var getNextSlide = function(seek) {
    var iPicture = this.vars.idxSlide,
      totals = this.vars.totalSlides;
    if (iPicture >= totals){
      iPicture = 0;
    }
    if (iPicture < 0){
      iPicture = totals - 1;
    }
    return (iPicture + seek + totals) % totals;
  };
  var seekSlide = function(seek) {
    var nextIdx = getNextSlide.call(this, seek);
    if ($.isFunction(this.options.onSlideChanges)) {
      this.options.onSlideChanges(this.vars.idxSlide, nextIdx);
    }
    executeTransition.call(this, nextIdx, seek < 0);
  };
  var executeTransition = function(nextIndex, isReversed) {
    var that = this,
      idxSlide = this.vars.idxSlide,
      bullets = this.vars.bullets,
      slideShow = this.vars.slideShow;

    if (this.vars.idxSlide === nextIndex || this.vars.isAnimating){
      if ($.isFunction(this.options.onSlideChanged)) {
        this.options.onSlideChanged();
      }
      return;
    }
    this.vars.isAnimating = true;
    switch(this.options.effect) {
      case Effects.FADE:
        fadeImg.call(this, nextIndex);
        break;
      case Effects.SLIDE:
        if (isReversed) {
          slideImgHorizontal.call(this, idxSlide, nextIndex, -slideShow.width);
        }
        else {
          slideImgHorizontal.call(this, idxSlide, nextIndex, slideShow.width);
        }
        break;
      case Effects.SLIDE_VERTICAL:
        if (isReversed) {
          slideImgVertical.call(this, idxSlide, nextIndex, -slideShow.height);
        }
        else {
          slideImgVertical.call(this, idxSlide, nextIndex, slideShow.height);
        }
        break;
      default:
        showImg.call(this, nextIndex);
    }
    this.vars.slides.promise().done(function() {
      that.vars.isAnimating = false;
      if ($.isFunction(that.options.onSlideChanged)) {
        that.options.onSlideChanged();
      }
    });
    bullets.removeClass(Selectors.ACTIVED_STT).eq(nextIndex).addClass(Selectors.ACTIVED_STT);
    this.vars.idxSlide = nextIndex;
  };
  var fadeImg = function(index) {
    var duration = this.options.duration,
      slides = this.vars.slides;
    slides.promise().done(function() {
      slides.css({ 'left': 0, 'top': 0 });
    });
    slides.eq(this.vars.idxSlide).fadeOut(duration);
    slides.eq(index).fadeIn(duration);
  };
  var slideImgVertical = function(curIndex, nextIndex, seek) {
    var duration = this.options.duration,
      ratio = this.vars.slideShow.ratio,
      slides = this.vars.slides;
    var ratioSeek = ratio * seek;

    slides.not(':nth-child(' + (nextIndex + 1) + '), :nth-child(' + (curIndex + 1) + ')').hide();

    slides.eq(nextIndex).css({
      'top': slides.eq(curIndex).position().top + ratioSeek,
      'left': 0
    })
    .animate({
      'top': 0
    }, ratio * duration, EASING_GRAPH).show();

    slides.eq(curIndex).animate({
      'top': -ratioSeek
    }, ratio * duration, EASING_GRAPH, function() {
      $(this).hide();
    });
  };
  var slideImgHorizontal = function(curIndex, nextIndex, seek) {
    var duration = this.options.duration,
      ratio = this.vars.slideShow.ratio,
      slides = this.vars.slides;
    var ratioSeek = ratio * seek;

    slides.not(':nth-child(' + (nextIndex + 1) + '), :nth-child(' + (curIndex + 1) + ')').hide();

    slides.eq(nextIndex).css({
      'top': 0,
      'left': slides.eq(curIndex).position().left + ratioSeek
    })
    .animate({
      'left': 0
    }, ratio * duration, EASING_GRAPH).show();

    slides.eq(curIndex).animate({
      'left': -ratioSeek
    }, ratio * duration, EASING_GRAPH, function() {
      $(this).hide();
    });
  };
  var showImg = function(index) {
    var duration = this.options.duration,
      slides = this.vars.slides;
    slides.promise().done(function() {
      slides.css({ 'left': 0, 'top': 0 });
    });
    slides.eq(this.vars.idxSlide).hide();
    slides.eq(index).show();
  };
  var moveImg = function(index, vertical, seek) {
    var slide = this.vars.slides.eq(index),
      slideShow = this.vars.slideShow;
    if (vertical) {
      slide.css({
        'left': 0,
        'top': boundary(seek, -slideShow.height, slideShow.height)
      }).show();
    }
    else {
      slide.css({
        'top': 0,
        'left': boundary(seek, -slideShow.width, slideShow.width)
      }).show();
    }
  };
  var swipeImg = function(vertical, seek) {
    var nextIdx = getNextSlide.call(this, SEEK_NEXT),
      prevIdx = getNextSlide.call(this, SEEK_PREVIOUS),
      slideShow = this.vars.slideShow;
    var oldPos = vertical ? slideShow.height : slideShow.width;
    moveImg.call(this, this.vars.idxSlide, vertical, seek);
    moveImg.call(this, nextIdx, vertical, slideShow.ratio * oldPos + seek);
    moveImg.call(this, prevIdx, vertical, slideShow.ratio * -oldPos + seek);
  };
  var swipeComplete = function() {
    var startPos = this.vars.startPos;
    var endPos = this.vars.endPos;
    var isVertical = this.vars.isVertical;
    var seek = isVertical ? endPos.yPos - startPos.yPos : endPos.xPos - startPos.xPos;
    if (!!!seek) {
      return;
    }

    var eff = this.options.effect;
    this.options.effect = isVertical ? Effects.SLIDE_VERTICAL : Effects.SLIDE;
    if (seek >= SWIPE) {
      seekSlide.call(this, SEEK_PREVIOUS);
    }
    else if (seek > 0) {
      this.vars.idxSlide = getNextSlide.call(this, SEEK_PREVIOUS);
      seekSlide.call(this, SEEK_NEXT);
    }
    else if (seek <= -SWIPE){
      seekSlide.call(this, SEEK_NEXT);
    }
    else {
      this.vars.idxSlide = getNextSlide.call(this, SEEK_NEXT);
      seekSlide.call(this, SEEK_PREVIOUS);
    }
    this.options.effect = eff;
  };
  var setTimer = function() {
    var that = this;
    if (!this.vars.intervalId) {
      this.vars.intervalId = setInterval(function() {
        seekSlide.call(that, SEEK_NEXT);
      }, that.options.interval);
    }
  };
  var clearTimer = function() {
    clearInterval(this.vars.intervalId);
    this.vars.intervalId = null;
  };
  var checkAutoplay = function() {
    if (this.options.autoplay) {
      setTimer.call(this);
    }
  };
  var changeSlideHandler = function(seek) {
    seekSlide.call(this, seek);
  };
  var keydownHandler = function(e) {
    var key = e.keyCode;
    var eff = this.options.effect;
    clearTimer.call(this);
    switch(key) {
      case Keys.LEFT:
        seekSlide.call(this, SEEK_PREVIOUS);
        break;
      case Keys.RIGHT:
        seekSlide.call(this, SEEK_NEXT);
        break;
      case Keys.UP:
        e.preventDefault();
        if (eff === Effects.SLIDE) {
          this.options.effect = Effects.SLIDE_VERTICAL;
        }
        seekSlide.call(this, SEEK_PREVIOUS);
        this.options.effect = eff;
        break;
      case Keys.DOWN:
        e.preventDefault();
        if (eff === Effects.SLIDE) {
          this.options.effect = Effects.SLIDE_VERTICAL;
        }
        seekSlide.call(this, SEEK_NEXT);
        this.options.effect = eff;
        break;
      default:
    }
    setTimer.call(this);
  };
  var mousedownSlideHandler = function(e) {
    if (!this.vars.isAnimating) {
      this.vars.startPos.xPos = this.vars.endPos.xPos = e.pageX;
      this.vars.startPos.yPos = this.vars.endPos.yPos = e.pageY;
      this.vars.isDragged = true;
      clearTimer.call(this);
    }
  };
  var mousemoveSlideHandler = function(e) {
    if (this.vars.isDragged) {
      this.vars.endPos.xPos = e.pageX;
      this.vars.endPos.yPos = e.pageY;

      var startPos = this.vars.startPos;
      var endPos = this.vars.endPos;
      if (endPos.xPos !== startPos.xPos && !this.vars.isVertical) {
        swipeImg.call(this, false, endPos.xPos - startPos.xPos);
      }
      else if (endPos.yPos !== startPos.yPos) {
        this.vars.isVertical = true;
        swipeImg.call(this, true, endPos.yPos - startPos.yPos);
      }
    }
  };
  var mousedownNavHandler = function(e, obj) {
    obj.addClass(Selectors.DOWN_STT);
    this.vars.isDowned = true;
    clearTimer.call(this);
  };
  var mouseupNavHandler = function(e) {
    if (this.vars.isDowned) {
      this.vars.previous.removeClass(Selectors.DOWN_STT);
      this.vars.next.removeClass(Selectors.DOWN_STT);
      this.vars.bullets.removeClass(Selectors.DOWN_STT);
      checkAutoplay.call(this);
    }
    this.vars.isDowned = true;
  };
  var mouseupSlideHandler = function(e) {
    if (this.vars.isDragged) {
      swipeComplete.call(this);
      checkAutoplay.call(this);
    }
    this.vars.isDragged = this.vars.isVertical = false;
  };

  function Plugin(element, options) {
    this.element = $(element);
    this.options = $.extend({}, $.fn[pluginName].defaults, this.element.data(), options);
    this.init();
  }

  Plugin.prototype = {
    init: function() {
      createFakeEl.call(this);
      var that = this;
      var slides = this.element.find('.' + Selectors.SLIDE);

      this.vars = {
        intervalId: null,
        isAnimating: false,
        isDowned: false,
        isDragged: false,
        isVertical: false,
        slideShow: { width: 0, height: 0, ratio: 1.0 },
        startPos: { xPos: 0, yPos: 0 },
        endPos: { xPos: 0, yPos: 0 },
        idxSlide: 0,
        totalSlides: slides.length,
        slides: slides,
        previous: this.element.find('#' + Selectors.NAV_PREVIOUS),
        next: this.element.find('#' + Selectors.NAV_NEXT),
        paginator: this.element.find('#' + Selectors.NAV_PAGE),
        bullets: this.element.find('.' + Selectors.BULLET)
      };
      this.vars.bullets.eq(0).addClass(Selectors.ACTIVED_STT);

      this.vars.slideShow.width = this.element.outerWidth(true);
      this.vars.slideShow.height = this.element.outerHeight(true);

      resizeHandler.call(this);
      showNavigator.call(this);
      checkAutoplay.call(this);

      doc.on('resize.' + pluginName, function() {
        resizeHandler.call(that);
      })
      .on('mouseup.' + pluginName + ' vmouseup.' + pluginName, function(e) {
        mouseupNavHandler.call(that, e);
        mouseupSlideHandler.call(that, e);
      });

      this.element.on('mousedown.' + pluginName + ' vmousedown.' + pluginName,
        ['#' + Selectors.NAV_PREVIOUS, '#' + Selectors.NAV_NEXT, '.' + Selectors.BULLET].join(','),
        function(e) {
          e.stopPropagation();
          mousedownNavHandler.call(that, e, $(this));
      })
      .on('click.' + pluginName, '#' + Selectors.NAV_PREVIOUS, function() {
        changeSlideHandler.call(that, SEEK_PREVIOUS);
      })
      .on('click.' + pluginName, '#' + Selectors.NAV_NEXT, function() {
        changeSlideHandler.call(that, SEEK_NEXT);
      })
      .on('click.' + pluginName, '.' + Selectors.BULLET, function() {
        changeSlideHandler.call(that, $(this).index() - that.vars.idxSlide);
      })
      .on('keydown.' + pluginName, function(e) {
        keydownHandler.call(that, e);
      })
      .on('dragstart.' + pluginName, '.' + Selectors.SLIDE, function(e) {
        e.preventDefault();
      })
      .on('mousedown.' + pluginName + ' vmousedown.' + pluginName, '.' + Selectors.SLIDE, function(e) {
        mousedownSlideHandler.call(that, e);
      })
      .on('mousemove.' + pluginName + ' vmousemove.' + pluginName, '.' + Selectors.SLIDE, function(e) {
        mousemoveSlideHandler.call(that, e);
      });
    },
    goto: function(index) {
      clearTimer.call(this);
      var type = typeof(index);
      if (type === L10n.Types.STRING && index.toLowerCase() === Navigations.PREVIOUS) {
        seekSlide.call(this, SEEK_PREVIOUS);
      }
      if (type === L10n.Types.STRING && index.toLowerCase() === Navigations.NEXT) {
        seekSlide.call(this, SEEK_NEXT);
      }
      if (type === L10n.Types.NUMBER && index >= 0 && index < this.vars.totalSlides) {
        seekSlide.call(this, index - this.vars.idxSlide);
      }
      checkAutoplay.call(this);
    },
    changeEffect: function(effect) {
      var eff = effect.trim().toLowerCase();
      var isValuedEff = false;
      var slides = this.vars.slides;
      var idxSlide = this.vars.idxSlide;
      for(var key in Effects) {
        if (Effects[key] === eff) {
          isValuedEff = true;
          break;
        }
      }
      if (isValuedEff) {
        this.options.effect = eff;
        if ($.isFunction(this.options.onEffectChanged)) {
          this.options.onEffectChanged(eff);
        }
      }
    },
    destroy: function() {
      this.element.off('.' + pluginName);
      doc.off('.' + pluginName);
      this.element.children().not('.' + Selectors.SLIDE).remove();
      this.element.children('.' + Selectors.SLIDE).find('img').each(function() {
        $(this).unwrap();
      });
      clearTimer.call(this);
      this.vars = null;
      $.removeData(this.element[0], pluginName);
    }
  };

  $.fn[pluginName] = function(options, params) {
    return this.each(function() {
      var instance = $.data(this, pluginName);
      if (!instance) {
        $.data(this, pluginName, new Plugin(this, options));
      } else if (instance[options]) {
        instance[options](params);
      } else {
        window.console && console.log(options ? options + ' method is not exists in ' + pluginName : pluginName + ' plugin has been initialized');
      }
    });
  };

  $.fn[pluginName].defaults = {
    autoplay: true,
    effect: 'slide',
    duration: 500,
    interval: 5000,
    onSlideChanges: $.noop,
    onSlideChanged: $.noop,
    onEffectChanged: $.noop
  };

  $(function() {
    $('[data-' + pluginName + ']')[pluginName]({
    });
  });

}(jQuery, window));
