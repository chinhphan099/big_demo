/**
 * @name Site
 * @description Define global variables and functions
 * @version 1.0
 */
var Site = (function($, window, undefined) {
  var privateVar = 1;

  $('#effect_btn').click(function(){
    $('[data-slide]').slide('changeEffect', $('#effect_txt').val());
  });
   $('#goto_btn').click(function(){
    $('[data-slide]').slide('goTo', $('#goto_txt').val());
  });
  return {
    publicVar: 1,
    publicObj: {
      var1: 1,
      var2: 2
    },

  };

})(jQuery, window);

jQuery(function() {

});

/**
 *  @name slide
 *  @description slider
 *  @version 1.0
 *  @options
      autoplay
      effect
      duration
      interval
      selectorClass
 *  @events
 *    event
 *  @methods
 *    init
 *    changeEffect
 *    goTo
 *    destroy
 */
;(function($, window, undefined) {
  var pluginName = 'slide',
    doc = $(document);
    prevDragSize = 40,
    nextDragSize = -40,
    slidesMargin = -600,
    firstPos = 0,
    distanceMove = 0,
    marginCurrent = 0,
    isMove = false,
    isAnimate = false,
    Effects = {
      SLIDE: 'slide',
      FADE: 'fade',
      SHOW: 'show'
    },
    Directions = {
      NEXT: 'next',
      PREV: 'prev'
    },
    Keys = {
      LEFT_ARROW : 37,
      RIGHT_ARROW : 39
    };

  function Plugin(element, options) {
    this.element = $(element);
    this.options = $.extend({}, $.fn[pluginName].defaults, this.element.data(), options);
    this.init();
  }
  function createView(that){
    var wrapper = that.element;
    wrapper
      .attr('tabindex', 0)
      .children()
      .addClass(that.options.selectorClass.containerSlides)
      .children()
      .addClass(that.options.selectorClass.slides);
    var imgs = wrapper.find('img');
    var bullets = '';
    imgs.each(function(){
      bullets += '<a></a>';
    });
    var controlEl = '<span class="' + that.options.selectorClass.btnPrev + '"></span>' +
                    '<span class="' + that.options.selectorClass.btnNext + '"></span>' +
                    '<div class="' + that.options.selectorClass.controlNav +'">' +
                      bullets +
                    '<div>';
    imgs.first().addClass('current');
    wrapper
      .append(controlEl)
      .find('.' + that.options.selectorClass.controlNav + '> a:first')
      .addClass(that.options.selectorClass.bulletActive);
  }
  function getCurrentImgIndex(that){
    return that.vars.imgs.filter('.current').index();
  }
  function setViewFade(that){
    if(that.vars.imgs.first().data('clone')){
      that.vars.slides
        .children()
        .first()
        .remove();
      that.vars.slides
        .children()
        .last()
        .remove();
      that.vars.imgs = that.vars.slides
                        .find('img');
      that.vars.imgCount = that.vars.imgs.length - 1;
    }
    var currentIndex = getCurrentImgIndex(that);
    that.vars.imgs
      .hide()
      .css({position: 'absolute'})
      .eq(currentIndex)
      .show();
    that.vars.slides
      .css({'margin-left':''});
  }
  function setViewSlide(that){
    if(!that.vars.imgs.first().data('clone')){
      var firstClone = that.vars.imgs
                        .first()
                        .clone()
                        .attr('data-clone', 'last')
                        .removeClass('current');
      var lastClone = that.vars.imgs
                        .last()
                        .clone()
                        .attr('data-clone', 'first')
                        .removeClass('current');
      that.vars.slides
        .prepend(lastClone)
        .append(firstClone);
      that.vars.imgs = that.vars.slides
                        .find('img');
      that.vars.imgCount = that.vars.imgs.length - 1;
    }
    that.vars.imgs
      .css({position: 'static', display: 'inline'})
      .show();
    that.vars.slides
      .css({'margin-left': slidesMargin * getCurrentImgIndex(that)});
  }
  function nextImg(that){
    var index = getCurrentImgIndex(that);
    if (index < that.vars.imgCount) {
      goImg(that, index + 1);
    }
    else {
      goImg(that, 0);
    }
  }
  function prevImg(that){
    var index = getCurrentImgIndex(that);
    if (index > 0) {
      goImg(that, index - 1);
    } else {
      goImg(that, that.vars.imgCount);
    }
  }
  function setCurrentImg(that, index){
    that.vars.imgs
      .removeClass('current')
      .eq(index)
      .addClass('current');
  }
  function setCurrentBullet(that, index){
    that.vars.bullets
      .removeClass(that.options.selectorClass.bulletActive)
      .eq(index)
      .addClass(that.options.selectorClass.bulletActive);
  }
  function fadeImg(that, index){
    setCurrentImg(that, index);
    that.vars.imgs
      .fadeOut(that.options.duration)
      .eq(index)
      .fadeIn(that.options.duration, function (){
                                          isAnimate = false;
                                        });
    setCurrentBullet(that, index);
  }
  function showImg(that, index){
    setCurrentImg(that, index);
    that.vars.imgs
      .hide()
      .eq(index)
      .show();
    setCurrentBullet(that, index);
    isAnimate = false;
  }
  function slideImg(that, index){
    if(that.vars.imgs.eq(index).data('clone') === 'last'){
      setCurrentImg(that, 1);
      setCurrentBullet(that, 0);
      that.vars.slides
        .animate({
          'margin-left' : slidesMargin * index }, {
          duration : that.options.duration,
          easing : 'swing',
          complete : function(){
            isAnimate = false;
            that.vars.slides
              .css({'margin-left': slidesMargin});
          }
      });
    }else{
      if(that.vars.imgs.eq(index).data('clone') === 'first'){
        setCurrentImg(that, that.vars.imgCount - 1);
        setCurrentBullet(that, that.vars.bullets.length - 1);
        that.vars.slides
          .animate({
            'margin-left' : slidesMargin * index }, {
            duration : that.options.duration,
            easing : 'swing',
            complete : function(){
              isAnimate = false;
              that.vars.slides
                .css({'margin-left': slidesMargin * (that.vars.imgCount - 1)});
            }
        });
      }else{
        setCurrentImg(that, index);
        setCurrentBullet(that, index - 1);
        that.vars.slides
          .animate({'margin-left' : slidesMargin * index}, {
            duration : that.options.duration,
            easing : 'swing',
            complete : function(){
              isAnimate = false;
            }
        });
      }
    }
  }
  function goImg(that, index){
    isAnimate = true;
    switch(that.options.effect){
      case Effects.FADE:
        fadeImg(that, index);
        break;
      case Effects.SLIDE:
        slideImg(that, index);
        break;
      case Effects.SHOW:
        showImg(that, index);
        break;
    }
  }
  function autoPlay(that){
    var interval = null;
    return interval = setInterval(function(){
                        nextImg(that);
                      }, that.options.interval);
  }

  function dragSlide(that, currentPos){
    if(isMove){
      distanceMove = currentPos - firstPos;
      that.vars.slides
        .css({'margin-left' : marginCurrent + distanceMove });
    }
  }
  function updateSlide(that){
    isMove = false;
    if(distanceMove > prevDragSize){
        prevImg(that);
    }else{
      if(distanceMove < nextDragSize){
        nextImg(that);
      }else{
        that.vars.slides
          .animate({'margin-left' : marginCurrent}, that.options.duration);
      }
    }
    distanceMove = 0;
  }
  function getMarginCurrent(that){
    return slidesMargin * getCurrentImgIndex(that);
  }
  Plugin.prototype = {
    init: function() {
      var that = this,
        wrapper = that.element;
      createView(this);
      this.vars = {
        containerSlides : wrapper.find('.' +  that.options.selectorClass.containerSlides),
        slides : wrapper.find('.' + that.options.selectorClass.slides),
        btnNext : wrapper.find('.' + that.options.selectorClass.btnNext),
        btnPrev : wrapper.find('.' + that.options.selectorClass.btnPrev),
        controlNav : wrapper.find('.' + that.options.selectorClass.controlNav),
        imgs : wrapper.find('img'),
        imgCount : wrapper.find('img').length - 1,
        bullets : wrapper.find('a')
      };

      this.changeEffect(this.options.effect);

      this.vars.btnNext.on('click.' + pluginName, function(){
        if(!isAnimate){
          nextImg(that);
        }
      });
      this.vars.btnPrev.on('click.' + pluginName, function(){
        if(!isAnimate){
          prevImg(that);
        }
      });
      this.vars.bullets.on('click.' + pluginName, function(){
        if(!isAnimate){
          if(that.options.effect === Effects.SLIDE){
            goImg(that, $(this).index() + 1);
          }
          else{
            goImg(that, $(this).index());
          }
        }
      });
      wrapper.on('keyup.' + pluginName, function(e){
        if(!isAnimate){
          switch(e.which){
            case Keys.LEFT_ARROW:
              prevImg(that);
              break;
            case Keys.RIGHT_ARROW:
              nextImg(that);
              break;
          }
        }
      });


      this.vars.containerSlides.on('mousedown.' + pluginName, function(evt){
        if(!isAnimate){
          evt.preventDefault();
          isMove = true;
          firstPos = evt.pageX;
          marginCurrent = getMarginCurrent(that);
          doc.on('mousemove.' + pluginName, function(e){
            dragSlide(that, e.pageX);
          });
          doc.one('mouseup.' + pluginName, function(){
            updateSlide(that);
          });
        }
      });

      if(this.options.autoplay){
        interval = autoPlay(that);
        wrapper.on('mouseenter.' + pluginName,function(){
          clearInterval(interval);
        });
        wrapper.on('mouseleave.' + pluginName,function(){
          interval = autoPlay(that);
        });
      }
    },
    goTo: function(param){
      if(param > 0 && param <= this.vars.imgCount + 1){
        if(this.options.effect === Effects.SLIDE){
          if(param < this.vars.imgCount){
            goImg(this, param);
          }
        }else{
          goImg(this, param - 1);
        }
      }else{
        if(param === Directions.NEXT){
          nextImg(this);
        }else{
          if(param === Directions.PREV){
            prevImg(this);
          }
        }
      }
    },
    changeEffect: function(effect){
      if(effect === Effects.SLIDE){
        this.options.effect = effect;
        setViewSlide(this);
      }else{
        if(effect === Effects.FADE ||
            effect === Effects.SHOW){
          this.options.effect = effect;
          setViewFade(this);
        }
      }
    },
    destroy: function() {
      if(this.vars.imgs.first().data('clone')){
        this.vars.slides
          .children()
          .first()
          .remove();
        this.vars.slides
          .children()
          .last()
          .remove();
      }
      this.vars.btnNext.remove();
      this.vars.btnPrev.remove();
      this.vars.controlNav.remove();
      this.vars.containerSlides.off('.' + pluginName);
      doc.off('.' + pluginName);
      this.element.off('.' + pluginName);
      clearInterval(interval);
      this.vars = null;
      $.removeData(this.element[0], pluginName);
    }
  };

  $.fn[pluginName] = function(options, params) {
    return this.each(function() {
      var instance = $.data(this, pluginName);
      if (!instance) {
        $.data(this, pluginName, new Plugin(this, options));
      } else if (instance[options]) {
        instance[options](params);
      } else {
        window.console && console.log(options ? options + ' method is not exists in ' + pluginName : pluginName + ' plugin has been initialized');
      }
    });
  };

  $.fn[pluginName].defaults = {
    effect: 'slide',
    duration: 500,
    autoplay: true,
    interval: 5000,
    selectorClass: {
      containerSlides: 'container-slides',
      slides: 'slides',
      btnNext: 'next',
      btnPrev: 'prev',
      controlNav: 'controlNav',
      bulletActive: 'active'
    }
  };

  $(function() {
    $('[data-' + pluginName + ']')[pluginName]({
    });
    $('[data-' + pluginName + ']').on('customEvent', function(){
    });
  });

}(jQuery, window));
