/**
 * @name Site
 * @description Define global variables and functions
 * @version 1.0
 */
var Site = (function($, window, undefined) {
  var privateVar = 1;

  function privateMethod1() {
    // todo
  }

  return {
    publicVar: 1,
    publicObj: {
      var1: 1,
      var2: 2
    },
    publicMethod1: privateMethod1
  };

})(jQuery, window);

jQuery(function() {
  //Site.publicMethod1();



    $('[data-' + 'gallery' + ']')['gallery']({
      clsiconnext :'.btnNext',
      clsiconprev : '.btnPre',
      clsContainer: '.imgBig',
      clsThumb : '.imgThumb',
      duration: 600,
      clsActive: 'active',
      setInterval: false,
      animateType: 'show',
      timerInterval: 3000,
      goTo: function(index) {

      },
      changeEffect: function(effect) {

      }
    });


  $('#btnChangeEffect').click(function() {
     $('[data-' + 'gallery' + ']')['gallery']('changeEffect', $('#cboEffect option:selected').val());
  });

  $('#btnGoTo').click(function() {
     $('[data-' + 'gallery' + ']')['gallery']('goTo', $('#cboIndex').val());
  });
});

/**
 * @name smGallery
 * @options
 *     clsIconNext
 *     clsIconPrev
 *     clsContainer
 *     clsThumb
 *     duration
 *     clsActive
 *     setInterval
 *     animateType
 *     timerInterval
 *     goTo: $.noop
 *    changeEffect: $.noop
 * @methods
 *    init
 *    goTo
 *    changeEffect
 *    destroy
 */
;(function($, window, undefined) {
  var pluginName = 'gallery';

  var Effects = {
      FADE: 'fade',
      SHOW: 'show',
      SLIDE: 'slide'
    },
    SwipeLength = {
      MIN: 0,
      MAX_RIGHT: 50,
      MAX_LEFT: -50
    },
    Keys = {
      KEY_RIGHT: 39,
      KEY_LEFT: 37
    },
    Idxs = {
      NEXT: 'next',
      PREV: 'prev'
    },
    MIN_DURATION = 0,
    MIN_MARGIN = 0,
    SLIDE_CICLE = -1,
    SLIDE_ANI_DEFAULT = {
      'margin-left': -10000
    },
    SLIDE_ANI_END = {
      'margin-left': 0
    },
    FADE_ANI_START = {
      'z-index': 0,
      'opacity': 0
    },
    FADE_ANI_END = {
      'z-index': 1,
      'opacity': 1
    },
    SHOW_ANI_START = {
      'z-index': 0
    },
    SHOW_ANI_END = {
      'z-index': 1
    },
    clsMain = 'contGallery',
    clsBtn = 'uiIcon';

  var renderCtrl = function(that) {
    var el = that.element,
      opts = that.options;

    el
      .addClass(clsMain)
      .find('ul:first')
      .addClass('lst-img')
      .wrap('<div class="imgBig">')
      .find('li:first')
      .addClass('active')
      .closest(clsMain);

    var thumbItem = '';
    el.find('img').each(function(idx) {
      thumbItem += '<li><a href="#" title="#">' + (idx + 1) + '</a></li>';
    });

    el.append('<div class="imgThumb">' +
      '<ul class="pagging">' + thumbItem + '</ul></div>' +
      '<a class="btnPre" title="Previous" href="#">' +
      '<img class="' + clsBtn + '" src="images/transparent.png" alt="Previous" />' +
      '</a>' +
      '<a class="btnNext" title="Next" href="#">' +
      '<img class="' + clsBtn + '" src="images/transparent.png" alt="Next"></a>');

    that.vars.divImg = el.find(opts.clsContainer);
    that.vars.ulImg = that.vars.divImg.find('ul:first');
    that.vars.liImgs = that.vars.ulImg.children(),
    that.vars.divThumb = el.find(opts.clsThumb);
    that.vars.ulThumb = that.vars.divThumb.find('ul:first');
    that.vars.liThumbs = that.vars.ulThumb.children();
    that.vars.btnPrev = el.find(that.options.clsIconPrev);
    that.vars.btnNext = el.find(that.options.clsIconNext);
    that.vars.lengthContainer = that.vars.liImgs.length - 1;
    that.vars.containerWidth = that.vars.liImgs.eq(0).outerWidth(true);
    that.vars.durationAnimate = that.options.duration;
    that.vars.isAniCompleted = true;
  };

  var startInterval = function(that) {
    if (that.options.setInterval) {
      that.vars.timerId = setInterval(function() {
        go(that, true);
      }, that.options.timerInterval);
    }
  };

  var endInterval = function(that) {
    if (that.options.setInterval) {
      clearInterval(that.vars.timerId);
    }
  };

  var activeThumb = function(that) {
    var liThbs = that.vars.liThumbs;
    liThbs.removeClass(that.options.clsActive);
    liThbs.eq(that.vars.currenIdx).addClass(that.options.clsActive);
  };

  var clickLiThumbAnimation = function(that, idx, aniStart, aniEnd) {
    if (!that.vars.isAniCompleted) {
      return false;
    }

    var opts = that.options,
      aniType = opts.animateType,
      liTags = that.vars.liImgs;

    var dura = 0;
    if (aniType !== Effects.SHOW) {
      dura = that.vars.durationAnimate;
    }

    endInterval(that);
    play(that);

    liTags.eq(that.vars.currenIdx).stop(true).animate(aniStart, dura);

    that.vars.isAniCompleted = false;
    if (aniType === Effects.SLIDE) {
      aniStart['margin-left'] = aniStart['margin-left'] * SLIDE_CICLE;
    }

    that.vars.currenIdx = idx;

    liTags.eq(idx).stop(true).css(aniStart).animate(aniEnd, dura, function() {
      if (opts.setInterval) {
        startInterval(that);
      }
      that.vars.isAniCompleted = true;
    });
  };

  var play = function(that) {
    if (that.vars.liImgs.length < 2) {
      that.vars.btnNext.css('visibility', 'hidden');
      that.vars.btnPrev.css('visibility', 'hidden');
      that.vars.liThumbs.css('visibility', 'hidden');
      return false;
    }

    var opts = that.options,
      aniType = opts.animateType,
      liTags = that.vars.liImgs;

    endInterval(that);

    if (aniType === Effects.FADE) {
      liTags.removeAttr('style');
      liTags
        .css(FADE_ANI_START)
        .eq(that.vars.currenIdx)
        .css(FADE_ANI_END);
    } else if (aniType === Effects.SHOW) {
      liTags.removeAttr('style');
      liTags
        .css(SHOW_ANI_START)
        .eq(that.vars.currenIdx)
        .css(SHOW_ANI_END);
    } else {
      liTags.removeAttr('style');
      liTags
        .css(SLIDE_ANI_DEFAULT)
        .eq(that.vars.currenIdx)
        .css(SLIDE_ANI_END);
    }

    if (opts.setInterval) {
      startInterval(that);
    }
    activeThumb(that);
  };

  var goAnimation = function(that, aniStart, aniEnd, isNext) {
    if (!that.vars.isAniCompleted) {
      return false;
    }

    var opts = that.options,
      aniType = opts.animateType,
      liTags = that.vars.liImgs,
      curIdx = that.vars.currenIdx,
      imgNumber = that.vars.lengthContainer;
    endInterval(that);

    var dura = 0;
    if (that.options.animateType !== Effects.SHOW) {
      dura = that.vars.durationAnimate;
    }

    liTags.eq(that.vars.currenIdx).stop(true).animate(aniStart, dura);

    if (isNext) {
      that.vars.currenIdx++;
      if (that.vars.currenIdx > imgNumber) {
        that.vars.currenIdx = 0;
      }
    } else {
      that.vars.currenIdx--;
      if (that.vars.currenIdx < 0) {
        that.vars.currenIdx = imgNumber;
      }
    }

    if (aniType === Effects.SLIDE) {
      aniStart['margin-left'] = aniStart['margin-left'] * SLIDE_CICLE;
    }

    that.vars.isAniCompleted = false;
    liTags.eq(that.vars.currenIdx).stop(true)
      .css(aniStart)
      .animate(aniEnd, dura, function() {
        if (opts.setInterval) {
          startInterval(that);
        }
        that.vars.isAniCompleted = true;
      });
  };

  var go = function(that, isNext) {
    if (that.vars.liImgs.length < 2) {
      return false;
    }
    play(that);
    endInterval(that);

    var opts = that.options,
      aniType = that.options.animateType,
      width = that.vars.containerWidth;

    if (aniType === Effects.FADE) {
      goAnimation(that, FADE_ANI_START, FADE_ANI_END, isNext);
    } else if (aniType === Effects.SHOW) {
      goAnimation(that, SHOW_ANI_START, SHOW_ANI_END, isNext);
    } else {
      var aniStart = null;

      if (isNext) {
        aniStart = {
          'margin-left': -width
        };
      } else {
        aniStart = {
          'margin-left': width
        };
      }

      goAnimation(that, aniStart, SLIDE_ANI_END, isNext);
    }

    activeThumb(that);
  };

  var clickLiThumbHandler = function(that, idx) {
    var opts = that.options,
      currIdx = that.vars.currenIdx,
      aniType = opts.animateType,
      width = that.vars.containerWidth;

    if (opts.setInterval) {
      endInterval(that);
    }

    if (currIdx === idx) {
      if (opts.setInterval) {
        startInterval(that);
      }
      return false;
    }

    if (aniType === Effects.FADE) {
      clickLiThumbAnimation(that, idx, FADE_ANI_START, FADE_ANI_END);
    } else if (aniType === Effects.SHOW) {
      clickLiThumbAnimation(that, idx, SHOW_ANI_START, SHOW_ANI_END);
    } else {
      var aniStart = null;

      if (currIdx > idx) {
        aniStart = {
          'margin-left': width
        };
      } else {
        aniStart = {
          'margin-left': -width
        };
      }

      clickLiThumbAnimation(that, idx, aniStart, SLIDE_ANI_END);
    }

    activeThumb(that);
  };

  var keyDownHandler = function(e, that) {
    e.preventDefault();
    if (e.keyCode === Keys.KEY_RIGHT) {
      go(that, true);
    }
    if (e.keyCode === Keys.KEY_LEFT) {
      go(that, false);
    }
  };

  var btnNextClickHandler = function(that) {
    go(that, true);
  };

  var btnPrevClickHandler = function(that) {
    go(that, false);
  };

  var liImgMouseDownHandler = function(e, that) {
    e.preventDefault();

    if (!that.vars.isAniCompleted || that.options.animateType !== Effects.SLIDE) {
      return false;
    }

    that.vars.isPressed = true;

    var liTags = that.vars.liImgs,
      currIdx = that.vars.currenIdx,
      nextIdx = currIdx + 1,
      prevIdx = currIdx - 1,
      imgNumber = that.vars.lengthContainer,
      width = that.vars.containerWidth;

    if (nextIdx > imgNumber) {
      nextIdx = 0;
    }

    if (prevIdx < 0) {
      prevIdx = imgNumber;
    }

    liTags
      .eq(prevIdx)
      .stop(true)
      .css('margin-left', -width);
    liTags
      .eq(nextIdx)
      .stop(true)
      .css('margin-left', width);

    that.vars.clientXDown = e.clientX;
  };

  var liImgMouseLeaveHandler = function(e, that) {
    e.preventDefault();

    if (!that.vars.isAniCompleted || that.options.animateType !== Effects.SLIDE) {
      return false;
    }

    if (that.vars.isPressed) {
      var liTags = that.vars.liImgs,
        currIdx = that.vars.currenIdx,
        nextIdx = currIdx + 1,
        prevIdx = currIdx - 1,
        imgNumber = that.vars.lengthContainer,
        dura = that.vars.durationAnimate;

      if (nextIdx > imgNumber) {
        nextIdx = 0;
      }
      if (prevIdx < 0) {
        prevIdx = imgNumber;
      }

      that.vars.clientXUp = e.clientX;
      that.vars.isPressed = false;

      var cliX = that.vars.clientXUp - that.vars.clientXDown;

      that.vars.isAniCompleted = false;

      if (cliX <= SwipeLength.MAX_LEFT) {
        liTags.eq(currIdx).stop(true).animate({
          'margin-left': -width
        }, dura);

        that.vars.currenIdx = nextIdx;

        liTags.eq(nextIdx).stop(true).animate({
          'margin-left': MIN_MARGIN
        }, dura, function() {
          activeThumb(that);
          that.vars.isAniCompleted = true;
        });
      } else if (cliX >= SwipeLength.MAX_RIGHT) {
        liTags.eq(currIdx).stop(true).animate({
          'margin-left': that.vars.containerWidth
        }, that.vars.durationAnimate);

        that.vars.currenIdx = prevIdx;

        liTags.eq(prevIdx).stop(true).animate({
          'margin-left': MIN_MARGIN
        }, dura, function() {
          activeThumb(that);
          that.vars.isAniCompleted = true;
        });
      } else {
        liTags.eq(currIdx).animate({
          'margin-left': MIN_MARGIN
        }, MIN_DURATION);

        if (cliX > SwipeLength.MAX_LEFT && cliX < SwipeLength.MIN) {
          liTags.eq(nextIdx).animate({
            'margin-left': width
          }, MIN_DURATION, function() {
            that.vars.isAniCompleted = true;
          });
        } else if (cliX > SwipeLength.MIN && cliX < SwipeLength.MAX_LEFT) {
          liTags.eq(prevIdx).animate({
            'margin-left': -width
          }, MIN_DURATION, function() {
            that.vars.isAniCompleted = true;
          });
        }
      }
    }
  };

  var liImgMouseUpHandler = function(e, that) {
    if (!that.vars.isAniCompleted || that.options.animateType !== Effects.SLIDE) {
      return false;
    }
    var liTags = that.vars.liImgs,
      currIdx = that.vars.currenIdx,
      nextIdx = currIdx + 1,
      prevIdx = currIdx - 1,
      imgNumber = that.vars.lengthContainer,
      width = that.vars.containerWidth,
      dura = that.vars.durationAnimate;

    if (nextIdx > imgNumber) {
      nextIdx = 0;
    }

    if (prevIdx < 0) {
      prevIdx = imgNumber;
    }

    that.vars.clientXUp = e.clientX;
    that.vars.isPressed = false;

    var cliX = that.vars.clientXUp - that.vars.clientXDown;

    that.vars.isAniCompleted = false;

    if (cliX <= SwipeLength.MAX_LEFT) {
      liTags.eq(currIdx).stop(true).animate({
        'margin-left': -width
      }, dura);

      that.vars.currenIdx = nextIdx;

      liTags.eq(nextIdx).stop(true).animate({
        'margin-left': MIN_MARGIN
      }, dura, function() {
        activeThumb(that);
        that.vars.isAniCompleted = true;
      });
    } else if (cliX >= SwipeLength.MAX_RIGHT) {
      liTags.eq(currIdx).stop(true).animate({
        'margin-left': width
      }, dura);

      that.vars.currenIdx = prevIdx;

      liTags.eq(prevIdx).stop(true).animate({
        'margin-left': MIN_MARGIN
      }, dura, function() {
        activeThumb(that);
        that.vars.isAniCompleted = true;
      });
    } else {
      liTags.eq(currIdx).stop(true).animate({
        'margin-left': MIN_MARGIN
      }, dura);

      if (cliX > SwipeLength.MAX_LEFT && cliX < SwipeLength.MIN) {
        liTags.eq(nextIdx).stop(true).animate({
          'margin-left': width
        }, dura, function() {
          that.vars.isAniCompleted = true;
        });
      } else if (cliX > SwipeLength.MIN && cliX < SwipeLength.MAX_RIGHT) {
        liTags.eq(prevIdx).stop(true).animate({
          'margin-left': -width
        }, dura, function() {
          that.vars.isAniCompleted = true;
        });
      } else {
        liTags.eq(prevIdx).stop(true).animate({
          'margin-left': -width
        }, MIN_DURATION);

        liTags.eq(nextIdx).stop(true).animate({
          'margin-left': width
        }, MIN_DURATION, function() {
          that.vars.isAniCompleted = true;
        });
      }
    }
  };

  var liImgMouseMoveHandler = function(e, that) {
    e.preventDefault();

    if (!that.vars.isAniCompleted || that.options.animateType !== Effects.SLIDE) {
      return false;
    }

    var liTags = that.vars.liImgs,
      currIdx = that.vars.currenIdx,
      nextIdx = currIdx + 1,
      prevIdx = currIdx - 1,
      imgNumber = that.vars.lengthContainer,
      width = that.vars.containerWidth;

    if (nextIdx > imgNumber) {
      nextIdx = 0;
    }

    if (prevIdx < 0) {
      prevIdx = imgNumber;
    }

    if (that.vars.isPressed) {
      var cliX = e.clientX - that.vars.clientXDown;

      that.vars.isAniCompleted = false;

      liTags.eq(currIdx).stop(true).animate({
        'margin-left': cliX
      }, MIN_DURATION);

      if (cliX > SwipeLength.MIN) {
        liTags.eq(prevIdx).stop(true).animate({
          'margin-left': -width + cliX
        }, MIN_DURATION, function() {
          that.vars.isAniCompleted = true;
        });
      } else if (cliX < SwipeLength.MIN) {
        liTags.eq(nextIdx).stop(true).animate({
          'margin-left': width + cliX
        }, MIN_DURATION, function() {
          that.vars.isAniCompleted = true;
        });
      } else if (cliX === SwipeLength.MIN) {
        liTags.eq(prevIdx).stop(true).animate({
          'margin-left': -width
        }, MIN_DURATION);

        liTags.eq(nextIdx).stop(true).animate({
          'margin-left': width
        }, MIN_DURATION, function() {
          that.vars.isAniCompleted = true;
        });
      }
    }
  };

  function Plugin(element, options) {
    this.element = $(element);
    this.options = $.extend({}, $.fn[pluginName].defaults, options);
    this.init();
  }

  Plugin.prototype = {
    init: function() {
      var that = this,
        options = this.options,
        doc = $(document);
      this.vars = {
        isPressed: false,
        clientXDown: 0,
        clientXUp: 0,
        divImg: null,
        ulImg: null,
        liImgs: null,
        divThumb: null,
        ulThumb: null,
        liThumbs: null,
        btnNext: null,
        btnPrev: null,
        timerId: null,
        currenIdx: 0,
        lengthContainer: null,
        containerWidth: null,
        durationAnimate: null,
        isAniCompleted: false
      };

      renderCtrl(that);
      play(that);

      doc.bind('keydown.' + pluginName, function(e) {
        keyDownHandler(e, that);
      });

      that.vars.liThumbs.each(function(idx) {
        var liThumb = $(this);
        liThumb.bind('click.' + pluginName, function() {
          clickLiThumbHandler(that, idx);
        });
      });

      that.vars.btnNext.bind('click.' + pluginName, function() {
        btnNextClickHandler(that);
      });

      that.vars.btnPrev.bind('click.' + pluginName, function() {
        btnPrevClickHandler(that);
      });

      this.element.bind('mouseenter.' + pluginName, function() {
        endInterval(that);
      });

      this.element.bind('mouseleave.' + pluginName, function() {
        startInterval(that);
      });

      this.vars.liImgs.bind('mouseenter.' + pluginName, function() {
        endInterval(that);
      });

      this.vars.liImgs.bind('mousedown.' + pluginName, function(e) {
        liImgMouseDownHandler(e, that);
      });

      this.vars.liImgs.bind('mouseleave.' + pluginName, function(e) {
        liImgMouseLeaveHandler(e, that);
      });

      this.vars.liImgs.bind('mouseup.' + pluginName, function(e) {
        liImgMouseUpHandler(e, that);
      });

      this.vars.liImgs.bind('mousemove.' + pluginName, function(e) {
        liImgMouseMoveHandler(e, that);
      });
    },

    goTo: function(index) {
      var that = this;
      endInterval(that);


      if (index === Idxs.NEXT) {
        go(that, true);
      } else if (index === Idxs.PREV) {
        go(that, false);
      } else {
        index = Number(index);
        if (!isNaN(index) && (index > 0 && index <= this.vars.lengthContainer + 1)) {
          if (index === 1) {
            this.vars.currenIdx = this.vars.lengthContainer;
          } else if (index === this.vars.lengthContainer + 1) {
            this.vars.currenIdx = this.vars.lengthContainer - 1;
          } else {
            this.vars.currenIdx = index - 2;
          }

          play(that);
          go(that, true);
        }
      }
      this.options.goTo(index);
    },

    changeEffect: function(effect) {
      endInterval(this);
      this.options.animateType = effect;
      play(this);
      this.options.changeEffect(effect);
    },

    destroy: function() {
      var el = this.element,
        opts = this.options;

      $(document).unbind('.' + pluginName);
      el.find('ul:first').find('li').unbind('.' + pluginName);
      el.unbind('.' + pluginName);
      el.find(opts.clsThumb).remove();
      el.find(opts.clsIconNext).remove();
      el.find(opts.clsIconPrev).remove();
      el.find('ul:first').find('li').removeAttr('style');
      el.find('ul:first').unwrap(opts.clsContainer);
      el.removeClass(clsMain);
    }
  };

  $.fn[pluginName] = function(options, params) {
    return this.each(function() {
      var instance = $.data(this, pluginName);
      if (!instance) {
        $.data(this, pluginName, new Plugin(this, options));
      } else if (instance[options]) {
        instance[options](params);
      } else {
        console.warn(options ? options + ' method is not exists in ' + pluginName : pluginName + ' plugin has been initialized');
      }
    });
  };

  $.fn[pluginName].defaults = {
    clsIconNext: '.btnNext',
    clsIconPrev: '.btnPre',
    clsContainer: '.imgBig',
    clsThumb: '.imgThumb',
    duration: 500,
    clsActive: 'active',
    setInterval: false,
    animateType: 'show',
    timerInterval: 3000,
    goTo: $.noop,
    changeEffect: $.noop
  };

}(jQuery, window));
