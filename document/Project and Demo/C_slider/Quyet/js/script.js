/**
 * @name Site
 * @description Define global variables and functions
 * @version 1.0
 */
var Site = (function($, window, undefined) {
  $('#goto').click(function(){
    var val = $('#slGoTo').val();
    $('[data-carousel').carousel('goTo', val);
  });
  $('#changeEff').click(function(){
    var val = $('#slEffect').val();
    $('[data-carousel').carousel('changeEffect', val);
  });
  $('#init').click(function(){
    $('[data-carousel').carousel('init', '');
  });
  $('#destroy').click(function(){
    $('[data-carousel').carousel('destroy', '');
  });
  $('#auto').click(function(){
    $('[data-carousel').carousel('toggleAutoplay', '');
  });
  function privateMethod1() {
    // todo
  }

  return {
    publicVar: 1,
    publicObj: {
      var1: 1,
      var2: 2
    },
    publicMethod1: privateMethod1
  };

})(jQuery, window);

jQuery(function() {
  Site.publicMethod1();
});

/**
 *  @name carousel
 *  @description a images slider
 *  @version 1.0
 *  @options
 *    effect: type of animation effect for carousel
 *    duration: timer for one effect completed
 *    autoplay: boolean value to set auto animation for carousel
 *    interval: timer for each time carousel will autoplay
 *  @events
 *  @methods
 *    init:
 *    goTo:
 *    changeEffect:
 *    toggleAutoplay:
 *    destroy:
 */
;(function($, window, undefined) {
  var pluginName = 'carousel';
  var win = $(window);
  var doc = $(document);
  var canDrag = false;
  var interval;

  var Events = {
    LOAD: 'load.' + pluginName,
    CLICK: 'click.' + pluginName,
    RESIZE: 'resize.' + pluginName,
    KEYDOWN: 'keydown.' + pluginName,
    MOUSEDOWN: 'mousedown.' + pluginName,
    MOUSEMOVE: 'mousemove.' + pluginName,
    MOUSEUP: 'mouseup.' + pluginName,
    DRAGSTART: 'dragstart.' + pluginName
  };

  var Keys = {
    LEFT: 37,
    RIGHT: 39
  };

  var Effects = ['slide', 'fade', 'show'];

  var Directions = {
    NEXT: 'next',
    PREV: 'prev'
  };

  var initTemplate = function(that) {
    var el = that.element;
    var crs = that.vars;
    el.addClass('carousel-outer');
    var crsInner = '<div class="carousel-inner"></div>',
      prevBtn = '<div class="dir prev"><i class="fa fa-angle-left"></i></div>',
      nextBtn = '<div class="dir next"><i class="fa fa-angle-right"></i></div>',
      bullet = '<div></div>';
    var wrapBullets = $('<div class="carousel-bullets"></div>');
    $(prevBtn).appendTo(el);
    $(nextBtn).appendTo(el);
    wrapBullets.appendTo(el);
    crs.images.each(function() {
      var img = $(this);
      img.wrap(crsInner);
      wrapBullets.append(bullet);
    });
    crs.nextBtn = el.find('.next');
    crs.prevBtn = el.find('.prev');
    crs.crsInners = el.find('.carousel-inner');
    crs.wrapBullets = el.find('.carousel-bullets');
    crs.bullets = crs.wrapBullets.find('div');
    bringImageToTop(that, $(crs.images[0]).parent());
    crs.bullets.eq(0).addClass('bullet-active');
  };

  var autoAnimate = function(that) {
    if (that.options.autoplay) {
      interval = setInterval(function() {
        that.vars.nextBtn.trigger(Events.CLICK);
      }, that.options.interval);
    }
  };

  var responsive = function(that) {
    var el = that.element;
    var crs = that.vars;
    var w = crs.nextBtn.width();
    var indent = crs.wrapBullets.find('div').width() * crs.images.length;
    el.find('.dir').css({
      'font-size': w
    });
    el.find('.dir i').css({
      'left': (w - el.find('.dir i').width()) / 2
    });
    crs.wrapBullets.css({
      'left': (el.width() - indent) / 2
    });
  };

  var canAnimate = function(that) {
    var isAnimating;
    that.vars.images.each(function() {
      isAnimating = $(this).parent().is(':animated');
      if (isAnimating) {
        return !isAnimating;
      }
    });
    return !isAnimating;
  };

  var animateImg = function(that, direction, steps) {
    if (canAnimate(that) && steps > 0) {
      var crs = that.vars;
      var idx = getCurrentIdx(that);
      var crsInners = crs.crsInners;
      var maxIdx = crsInners.length - 1;
      var current = $(crsInners[idx]);
      var target, newIdx;
      if (direction === Directions.NEXT) {
        newIdx = (idx + steps > maxIdx) ? (idx + steps - maxIdx - 1) : (idx + steps);
        target = $(crsInners[newIdx]);
        crs.w = current.width();
      } else {
        newIdx = (idx - steps < 0) ? maxIdx : (idx - steps);
        target = $(crsInners[newIdx]);
        crs.w = -current.width();
      }
      crs.images.each(function() {
        $(this).parent().css('opacity', 0);
      });
      current.css('opacity', 1);
      target.css('opacity', 1);
      switch (that.options.effect) {
        case Effects[0]: //SLIDE
          slide(that, current, target, newIdx);
          break;
        case Effects[1]: //FADE
          fade(that, current, target, newIdx);
          break;
        case Effects[2]: //SHOW
          show(that, target, newIdx);
          break;
        default:
      }
    }
  };

  var slide = function(that, current, target, newIdx) {
    bringImageToTop(that, target);
    if (target.position().left === 0) {
      target.css({
        'left': that.vars.w
      });
    }
    current.animate({
      'left': -that.vars.w
    }, that.options.duration);
    target.animate({
      'left': 0
    }, that.options.duration, function() {
      resetCrs(that);
      bringImageToTop(that, target);
    });
    updateBullet(that, newIdx);
  };

  var fade = function(that, current, target, newIdx) {
    current.animate({
      'opacity': 0
    }, {
      'duration': that.options.duration,
      'done': function() {
        resetCrs(that);
        bringImageToTop(that, target);
      }
    });
    updateBullet(that, newIdx);
  };

  var show = function(that, target, newIdx) {
    bringImageToTop(that, target);
    updateBullet(that, newIdx);
  };

  var resetCrs = function(that){
    that.vars.crsInners.each(function() {
      $(this).css({
        'left': 0,
        'z-index': 0,
        'opacity': 1
      });
    });
  };

  var bringImageToTop = function(that, img) {
    that.vars.images.each(function() {
      $(this).parent().css('z-index', 0);
    });
    img.css({
      'z-index': 1
    });
  };

  var getCurrentIdx = function(that) {
    return that.element.find('.bullet-active').index();
  };

  var updateBullet = function(that, idx) {
    that.vars.bullets.each(function() {
      $(this).removeClass('bullet-active');
    });
    that.vars.bullets.eq(idx).addClass('bullet-active');
  };

  var handleKeyEvent = function(e, that) {
    switch (e.which) {
      case Keys.LEFT:
        animateImg(that, Directions.PREV, 1);
        break;
      case Keys.RIGHT:
        animateImg(that, Directions.NEXT, 1);
        break;
      default:
    }
  };

  var prepareDD = function(e, that) {
    clearInterval(interval);
    var crs = that.vars;
    var crsInners = crs.crsInners;
    var idx = getCurrentIdx(that),
      maxIdx = crsInners.length - 1,
      nextIdx = (idx === maxIdx) ? 0 : idx + 1,
      prevIdx = (idx === 0) ? maxIdx : idx - 1,
      w = crsInners.width();
    crs.nextTarget = $(crs.crsInners[nextIdx]);
    crs.prevTarget = $(crs.crsInners[prevIdx]);
    crs.nextTarget.css({
      'left': w,
      'z-index': 1
    });
    crs.prevTarget.css({
      'left': -w,
      'z-index': 1
    });
    crs.xPos = e.pageX;
    canDrag = true;
  };

  var dragging = function(e, that) {
    var idx = getCurrentIdx(that);
    var crs = that.vars;
    crs.currentTarget = $(crs.crsInners[idx]);
    var w = crs.currentTarget.width();
    crs.distance = e.pageX - crs.xPos;
    crs.currentTarget.css({
      'left': crs.distance
    });
    crs.prevTarget.css({
      'left': crs.distance - w
    });
    crs.nextTarget.css({
      'left': crs.distance + w
    });
  };

  var droped = function(that) {
    if (canDrag) {
      var crs = that.vars;
      canDrag = false;
      var tmp = that.options.effect;
      var swipe = crs.crsInners.width() / 5;
      that.options.effect = Effects[0]; // SLIDE
      if (crs.distance > swipe) {
        animateImg(that, Directions.PREV, 1);
      } else if (crs.distance < -swipe) {
        animateImg(that, Directions.NEXT, 1);
      } else {
        resetCrs(that);
        bringImageToTop(that, crs.currentTarget);
        crs.currentTarget.animate({
          'left': 0
        }, that.options.duration / 3);
      }
      that.options.effect = tmp;
      crs.distance = 0;
      autoAnimate(that);
    }
  };

  var goToSlide = function(that, target){
    var len = that.vars.crsInners.length;
    if (target <= len) {
      var current = that.element.find('.bullet-active').index() + 1;
      var steps = (target > current) ? (target - current) : (current - target);
      var direction = (target > current) ? Directions.NEXT : Directions.PREV;
      animateImg(that, direction, steps);
    }
  };

  function Plugin(element, options) {
    this.element = $(element);
    this.options = $.extend({}, $.fn[pluginName].defaults, this.element.data(), options);
    this.init();
  }

  Plugin.prototype = {
    init: function() {
      var that = this;
      var el = this.element;
      var opts = this.options;
      that.vars = {
        images: el.find('img')
      };
      initTemplate(that);
      autoAnimate(that);
      responsive(that);
      var crs = that.vars;
      crs.bullets.each(function(idx) {
        var bullet = $(this);
        bullet.on(Events.CLICK, function(e) {
          var currentIdx = that.element.find('.bullet-active').index();
          var steps = (idx > currentIdx) ? (idx - currentIdx) : (currentIdx - idx);
          var direction = (idx > currentIdx) ? Directions.NEXT : Directions.PREV;
          animateImg(that, direction, steps);
        });
      });

      crs.images.on(Events.LOAD, function() {
        responsive(that);
      })
      .each(function(){
        $(this).on(Events.DRAGSTART, function(e){
          e.preventDefault();
        });
      });

      crs.nextBtn.on(Events.CLICK, function() {
        animateImg(that, Directions.NEXT, 1);
      });

      crs.prevBtn.on(Events.CLICK, function() {
        animateImg(that, Directions.PREV, 1);
      });

      crs.crsInners.each(function() {
        var img = $(this);
        img.on(Events.MOUSEDOWN, function(e) {
          if (canAnimate(that)) {
            prepareDD(e, that);
            e.preventDefault();
          }
        });
      });

      doc.on(Events.KEYDOWN, function(e) {
        handleKeyEvent(e, that);
      })
        .on(Events.MOUSEMOVE, function(e) {
          if (!canDrag) {
            return;
          } else {
            dragging(e, that);
          }
        })
        .on(Events.MOUSEUP, function(e) {
          droped(that);
        });

      win.on(Events.RESIZE, function() {
        responsive(that);
      });
    },

    goTo: function(params) {
      var that = this;
      switch ($.type(params)) {
        case L10n.Types.STRING:
          if (params === Directions.NEXT) {
            animateImg(that, Directions.NEXT, 1);
          } else if (params === Directions.PREV) {
            animateImg(that, Directions.PREV, 1);
          }else if(!isNaN(parseInt(params))){
            goToSlide(that, parseInt(params));
          }
          break;
        case L10n.Types.NUMBER:
          goToSlide(that, params);
          break;
        default:
      }
    },

    changeEffect: function(params) {
      var that = this;
      if ($.inArray(params, Effects) >= 0) {
        that.element.find('*').promise().done(function() {
          that.options.effect = params;
        });
      }
    },

    toggleAutoplay: function(){
      var that = this;
      that.options.autoplay = !this.options.autoplay;
      if (!this.options.autoplay) {
        clearInterval(interval);
      }else{
        autoAnimate(that);
      }
    },

    destroy: function() {
      var that = this;
      var crs = that.vars;
      doc.off();
      win.off();
      crs.images.each(function() {
        $(this).unwrap();
      });
      that.element.removeClass('carousel-outer');
      crs.crsInners.remove();
      crs.nextBtn.remove();
      crs.prevBtn.remove();
      crs.wrapBullets.remove();
      this.vars = '';

      $.removeData(this.element[0], pluginName);
    }
  };

  $.fn[pluginName] = function(options, params) {
    return this.each(function() {
      var instance = $.data(this, pluginName);
      if (!instance) {
        $.data(this, pluginName, new Plugin(this, options));
      } else if (instance[options]) {
        instance[options](params);
      } else {
        window.console && console.log(options ? options + ' method is not exists in ' + pluginName : pluginName + ' plugin has been initialized');
      }
    });
  };

  $.fn[pluginName].defaults = {
    effect: 'slide',
    duration: 1000,
    autoplay: true,
    interval: 3000
  };

  $(function() {
    $('[data-' + pluginName + ']')[pluginName]({});
  });

}(jQuery, window));
