/**
 *  @name slider
 *  @description
 *  @version 1.0
 *  @options
 *    effect
 *    duration
 *    autoplay
 *    interval
 *  @events
 *    event
 *  @methods
 *    init
 *    goTo
 *    changeEft
 */
;(function($, window, undefined) {
  var pluginName = 'slider',
  Classes = {
    SLIDER: 'slider',
    SLIDE: 'slide',
    NAV: 'navigator',
    BL: 'bullet',
    L_BTN: 'arrow-left',
    R_BTN: 'arrow-right',
    ITEM: 'item',
    HIDDEN: 'hidden',
    FADE: 'fade-out',
    ACT: 'active'
  },
  effect = {
    SLIDE: 'slide',
    FADE: 'fade'
  },
  direction = {
    L: 'left',
    R: 'right'
  },
  Keys = {
    LEFT: 37,
    RIGHT: 39
  },
  Evts = {
    CLICK: 'click.' + pluginName,
    M_ENTER: 'mouseenter.' + pluginName,
    M_LEAVE: 'mouseleave.' + pluginName,
    K_DOWN: 'keydown.' + pluginName
  },
  doc = $(document),
  loop,
  index,
  effClass,
  isAnimating;

  var addEl = function(that) {
    var bullet = '<a class="' + Classes.BL + '" href="#" title=""></a>',
        listBL = '',
        nav = '',
        arrBtn = '';

    for(var i = 0, len = that.element.find('.' + Classes.ITEM).length; i < len; i++) {
      listBL += bullet;
    }
    nav = '<div class="' + Classes.NAV + '">' + listBL + '</div>';
    arrBtn = '<a class="' + Classes.L_BTN + '"></a><a class="' + Classes.R_BTN + '"></a>';
    that.element.append(nav + arrBtn);
  };
  var setH = function(that) {
    var MAX = 0;
    that.element.vars.items.each(function() {
      MAX = Math.max($(this).height(), MAX);
    });
    that.element.css('height', MAX);
  };
  var getEffect = function(that, params) {
    switch(params || that.options.effect) {
      case effect.FADE:
        effClass = Classes.FADE;
        break;
      case effect.SLIDE:
        $('.' + Classes.SLIDER).width($('.' + Classes.ITEM).width() * $('.' + Classes.ITEM).length).addClass(Classes.SLIDE);
        effClass = Classes.SLIDE;
        return params || that.options.effect;
      default:
        effClass = Classes.HIDDEN;
    }
  };
  var setDuration = function(that) {
    that.element.vars.slider.css('transition-duration', that.options.duration + 'ms');
  };
  var getIdx = function(that) {
    that.element.vars.items.each(function() {
      if(!$(this).hasClass(effClass)) {
        index = $(this).index();
        return false;
      }
    });
  };
  var makeCycle = function(that) {
    if(index < 0) {
      return index = that.element.vars.items.length - 1;
    }
    if(index >= that.element.vars.items.length) {
      return index = 0;
    }
  };
  var toggleClass = function(that) {
    var vars = that.element.vars,
        items = vars.items;

    isAnimating = true;
    if(vars.eff) {
      vars.slider.addClass(effClass).css('margin-left', -vars.W * index).one('transitionend', function() {
        isAnimating = false;
      });
    }
    items.eq(index).removeClass(effClass).siblings().addClass(effClass).one('transitionend', function() {
        isAnimating = false;
      });

    vars.bullet.eq(index).addClass(Classes.ACT).siblings().removeClass(Classes.ACT);
  };

  var play = function(that) {
    loop = setTimeout(function() {
      toggleClass(that);
      index++;
      makeCycle(that);
      play(that);
    }, that.options.interval);
  };

  var stop = function() {
    clearTimeout(loop);
  };

  var keydownHandler = function(that, keycode) {
    switch(keycode) {
      case Keys.LEFT:
        that.element.vars.leftBtn.trigger(Evts.CLICK);
        break;
      case Keys.RIGHT:
        that.element.vars.rightBtn.trigger(Evts.CLICK);
        break;
    }
  };

  function Plugin(element, options) {
    this.element = $(element);
    this.options = $.extend({}, $.fn[pluginName].defaults, this.element.data(), options);
    this.init();
  }

  Plugin.prototype = {
    init: function() {
      var that = this,
          el = this.element;

      addEl(this);

      el.vars = {
        items: el.find('.' + Classes.ITEM),
        nav: el.find('.' + Classes.NAV),
        bullet: el.find('.' + Classes.BL),
        leftBtn: el.find('.' + Classes.L_BTN),
        rightBtn: el.find('.' + Classes.R_BTN),
        slider: el.find('.' + Classes.SLIDER),
        eff: getEffect(this),
        W: $('.' + Classes.ITEM).width()
      };

      setH(this);
      setDuration(this);
      getIdx(this);
      toggleClass(this, effClass);
      stop();

      if(this.options.autoplay) {
        play(this);
      }

      el.vars.leftBtn.off().on(Evts.CLICK, function(e) {
        e.preventDefault();
        that.goTo('left');
      });

      el.vars.rightBtn.off().on(Evts.CLICK, function(e) {
        e.preventDefault();
        that.goTo('right');
      });

      el.vars.bullet.off().on(Evts.CLICK, function(e) {
        e.preventDefault();
        that.goTo($(this).index());
      });

      el.vars.items.off(Evts.M_ENTER).on(Evts.M_ENTER, function() {
        stop();
      })
      .off(Evts.M_LEAVE).on(Evts.M_LEAVE, function() {
        play(that);
      })
      .off(Evts.CLICK).on(Evts.CLICK, function(e) {
        if(isAnimating) {
          return;
        }
        console.log(index + ' ' + isAnimating);
      });

      el.off(Evts.K_DOWN).on(Evts.K_DOWN, function(e) {
        e.preventDefault();
        keydownHandler(that, e.which);
      });
    },
    goTo: function(params) {
      stop();
      getIdx(this);
      switch(params) {
        case direction.L:
          index--;
          break;
        case direction.R:
          index++;
          break;
        default:
          if(!isNaN(params)) {
            index = params;
          }
      }
      makeCycle(this);
      toggleClass(this);
      play(this);
    },
    changeEffect: function(params) {
      var vars = this.element.vars;
      stop(this);
      getIdx(this);
      vars.items.removeClass(effClass);
      vars.slider.removeClass(effClass).css('margin-left', '').width(vars.W);
      vars.eff = getEffect(this, params);
      toggleClass(this);
      play(this);
    },
    destroy: function() {
      var vars = this.element.vars;
      $.removeData(this.element[0], pluginName);
      stop();
      vars.leftBtn.remove();
      vars.rightBtn.remove();
      this.element.find('.' + Classes.NAV).remove();
      vars.items.off(Evts.M_ENTER).off(Evts.M_LEAVE).off(Evts.CLICK);
      this.element.off(Evts.K_DOWN);
    }
  };

  $.fn[pluginName] = function(options, params) {
    return this.each(function() {
      var instance = $.data(this, pluginName);
      if (!instance) {
        $.data(this, pluginName, new Plugin(this, options));
      } else if (instance[options]) {
        instance[options](params);
      } else {
        window.console && console.log(options ? options + ' method is not exists in ' + pluginName : pluginName + ' plugin has been initialized');
      }
    });
  };

  $.fn[pluginName].defaults = {
    effect: 'show',
    duration: 1000,
    autoplay: true,
    interval: 2000
  };

  $(function() {
    $('[data-' + pluginName + ']')[pluginName]({
      key: 'custom'
    });
    $('[data-' + pluginName + ']').on('customEvent', function(){});
  });

}(jQuery, window));
