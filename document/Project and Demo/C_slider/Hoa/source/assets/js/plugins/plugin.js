/**
 *  @name plugin
 *  @description description
 *  @version 1.0
 *  @options
 *    option
 *  @events
 *    event
 *  @methods
 *    init
 *    publicMethod
 *    destroy
 */
if (!Function.prototype.bind) {
  Function.prototype.bind = function(oThis) {
    if (typeof this !== "function") {
      throw new TypeError("Function.prototype.bind - what is trying to be bound is not callable");
    }
    var aArgs = Array.prototype.slice.call(arguments, 1),
      fToBind = this,
      fNOP = function() {},
      fBound = function() {
        return fToBind.apply(this instanceof fNOP && oThis ? this : oThis,
          aArgs.concat(Array.prototype.slice.call(arguments)));
      };
    FNOP.prototype = this.prototype;
    fBound.prototype = new FNOP();
    return fBound;
  };
}

(function($, window, undefined) {
  var pluginName = 'slide';
  var EffectsArr = ['slide', 'fade', 'show'];
  var doc = $(document);

  var createBtn = function(label, handler) {
    return btn = $('<button>' + label + '</button>').on('click', handler);
  };

  createAdditionalBtn = function() {
    var el = '';
    el = '<label> Effect: ' +
      '<select id="effect-select">' +
      '<option value="slide">slide</option>' +
      '<option value="fade">fade</option>' +
      '<option value="show">show</option>' +
      '<option value="random">random</option>' +
      '</select>' +
      '</label>' +
      '<label> Duration' +
      '<input type="text" id="duration-input"/>' +
      '<button id="duration-btn">Change</button>' +
      '</label>' +
      '<label> Interval' +
      '<input type="text" id="interval-input"/>' +
      '<button id="interval-btn">Change</button>' +
      '</label>';
    return $(el);
  };

  setValForAdditionalBtn = function(effect, duration, interval) {
    var Ids = this.options.Ids;
    $(Ids.DURATION_TXTBOX).val(duration);
    $(Ids.INTERVAL_TXTBOX).val(interval);
    $(Ids.EFFECT_SELECT).val(effect);
    $(Ids.EFFECT_SELECT).on('change', onEffectChangeHandler.bind(this));
    $(Ids.INTERVAL_BTN).on('click', onIntervalChangeHandler.bind(this));
    $(Ids.DURATION_BTN).on('click', onDurationChangeHandler.bind(this));
  };

  var toFakeEl = function() {
    var opts = this.options;
    var fakeEl = this.fakeEl;
    this.slideArr = [];
    this.selectedSlide = 0;
    var paging = '';
    var btnEl = '';
    btnEl = '<div class="btn-wrap">' +
      '<div class="bg"> </div>' +
      '<span class="btn next"></span>' +
      '<span class="label splash"></span>' +
      '<span class="btn prev"></span>' +
      '</div>';
    fakeEl.addClass(opts.Classes.CONTAINER);
    var imgTags = fakeEl.find('img');
    var len = imgTags.length;
    for (var i = 0; i < len; i++) {
      var imgTagAtI = imgTags.eq(i);
      imgTagAtI.wrap('<div class="' + opts.Classes.SLIDER + ' hide" data-idx="' + i + '"></div>');
      paging = paging + '<a href="#' + i + '">' + (i + 1) + '</a>';
      this.slideArr.push(i);
    }
    paging = '<div class="navigator">' + paging + '</div>';
    fakeEl.append(btnEl);
    fakeEl.append(paging);
    fakeEl.data('len', len);
  };

  var prepareNextSlide = function(idx) {
    var opts = this.options;
    this.fakeEl.find('[data-idx="' + idx + '"]').addClass(opts.Pos.LEFT).removeClass(opts.Classes.HIDE);
  };

  var preparePrevSlide = function(idx) {
    var opts = this.options;
    this.fakeEl.find('[data-idx="' + idx + '"]').addClass(opts.Pos.RIGHT).removeClass(opts.Classes.HIDE);
  };

  var prepareNextFade = function(idx) {
    var opts = this.options;
    this.fakeEl.find('[data-idx="' + idx + '"]').addClass(opts.Pos.BELOW).removeClass(opts.Classes.HIDE);
  };

  var setMiddle = function(idx) {
    var fakeEl = this.fakeEl;
    var opts = this.options;
    fakeEl.find('.' + opts.Pos.MIDDLE).removeClass(opts.Pos.MIDDLE).addClass(opts.Classes.HIDE);
    fakeEl.find('[data-idx="' + idx + '"]').addClass(opts.Pos.MIDDLE).removeClass(opts.Classes.HIDE);
  };

  var slideNextTo = function(idx) {
    this.isAnimating = true;
    finnishAnimate.call(this);
    var that = this;
    var fakeEl = this.fakeEl;
    var opts = this.options;
    var Pos = opts.Pos;
    prepareNextSlide.call(this, idx);
    fakeEl.find('.' + Pos.MIDDLE).animate({
      left: Pos.RIGHT_VAL
    }, {
      duration: this.duration,
      easing: 'swing',
      always: function() {
        $(this).removeClass(Pos.MIDDLE).addClass(opts.Classes.HIDE).css('left', '');
      }
    });
    fakeEl.find('.' + Pos.LEFT).animate({
      left: Pos.MIDDLE_VAL
    }, {
      duration: this.duration,
      easing: 'swing',
      always: function() {
        $(this).removeClass(Pos.LEFT).addClass(Pos.MIDDLE).css('left', '');
      }
    });
  };

  var slidePrevTo = function(idx) {
    this.isAnimating = true;
    finnishAnimate.call(this);
    var that = this;
    var fakeEl = this.fakeEl;
    var opts = this.options;
    var Pos = opts.Pos;
    preparePrevSlide.call(this, idx);
    fakeEl.find('.' + Pos.MIDDLE).animate({
      left: Pos.LEFT_VAL
    }, {
      duration: this.duration,
      easing: 'swing',
      always: function() {
        $(this).removeClass(Pos.MIDDLE).addClass(opts.Classes.HIDE).css('left', '');
      }
    });
    fakeEl.find('.' + Pos.RIGHT).animate({
      left: Pos.MIDDLE_VAL
    }, {
      duration: this.duration,
      easing: 'swing',
      always: function() {
        $(this).removeClass(Pos.RIGHT).addClass(Pos.MIDDLE).css('left', '');
      }
    });
  };

  var fadeTo = function(idx) {
    this.isAnimating = true;
    finnishAnimate.call(this);
    var that = this;
    var fakeEl = this.fakeEl;
    var opts = this.options;
    var Pos = opts.Pos;
    var selectedIdx = fakeEl.find('.' + Pos.MIDDLE).data('idx');
    if (idx === selectedIdx) {
      return;
    }
    prepareNextFade.call(this, idx);
    fakeEl.find('.' + Pos.MIDDLE).animate({
      opacity: 0
    }, {
      duration: this.duration,
      easing: 'swing',
      always: function() {
        $(this).removeClass(Pos.MIDDLE).addClass(opts.Classes.HIDE).css('opacity', '');
      }
    });
    fakeEl.find('.' + Pos.BELOW).animate({
      opacity: 1
    }, {
      duration: this.duration,
      easing: 'swing',
      always: function() {
        $(this).removeClass(Pos.BELOW).addClass(Pos.MIDDLE).css('opacity', '');
      }
    });
  };

  var showHideTo = function(idx) {
    var that = this;
    var fakeEl = this.fakeEl;
    var opts = this.options;
    var Pos = opts.Pos;
    var selectedIdx = fakeEl.find('.' + Pos.MIDDLE).data('idx');
    if (idx === selectedIdx) {
      return;
    }
    prepareNextFade.call(this, idx);
    fakeEl.find('.' + Pos.MIDDLE).animate({
      opacity: 0
    }, {
      duration: 0,
      easing: 'swing',
      always: function() {
        $(this).removeClass(opts.Pos.MIDDLE).addClass(opts.Classes.HIDE).css('opacity', '');
      }
    });
    fakeEl.find('.' + Pos.BELOW).animate({
      opacity: 1
    }, {
      duration: 0,
      easing: 'swing',
      always: function() {
        $(this).removeClass(opts.Pos.BELOW).addClass(opts.Pos.MIDDLE).css('opacity', '');
      }
    });
  };

  var slideTo = function(idx) {
    var selectedIdx = this.fakeEl.find('.' + this.options.Pos.MIDDLE).data('idx');
    if (idx === selectedIdx) {
      return;
    } else if (idx < selectedIdx) {
      slidePrevTo.call(this, idx);
    } else {
      slideNextTo.call(this, idx);
    }
  };

  var goTo = function(idx) {
    if(this.isTouching) {
      return;
    }
    if (this.isAnimating) {
      return;
    }
    var fakeEl = this.fakeEl;
    var opts = this.options;
    var selectedIdx = fakeEl.find('.' + opts.Pos.MIDDLE).data('idx');
    if (idx === 'next') {
      idx = ++selectedIdx;
    } else if (idx === 'prev') {
      idx = --selectedIdx;
    }
    if (idx < 0) {
      idx = fakeEl.data('len') - 1;
    }
    if (idx >= fakeEl.data('len')) {
      idx = 0;
    }
    this.fakeEl.find('a.selected').removeClass('selected');
    fakeEl.find('a[href="#' + idx + '"]').addClass('selected');
    var effect = this.effect;
    if (this.effect === 'random') {
      var rand = Math.floor(Math.random() * 3);
      effect = EffectsArr[rand];
    }
    var Effects = opts.Effects;
    switch (effect) {
      case Effects.SLIDE:
        slideTo.call(this, idx);
        break;
      case Effects.FADE:
        fadeTo.call(this, idx);
        break;
      case Effects.SHOW:
        showHideTo.call(this, idx);
        break;
      default:
        slideTo.call(this, idx);
    }
  };
  var autoPlay = function() {
    goTo.call(this, 'next');
    setTimeout(autoPlay.bind(this), this.interval);
  };

  var finnishAnimate = function() {
    setTimeout(function() {
      this.isAnimating = false;
    }.bind(this), this.duration);
  };

  var onNavigatorClickHandler = function(e) {
    var target = $(e.target);
    var clickedIdx = target.html() - 1;
    goTo.call(this, clickedIdx);
    e.preventDefault();
  };

  var onNextClickHandler = function(e) {
    var selectedIdx = this.fakeEl.find('.' + this.options.Pos.MIDDLE).data('idx');
    selectedIdx = ++selectedIdx;
    goTo.call(this, selectedIdx);
    e.preventDefault();
  };

  var onPrevClickHandler = function(e) {
    var selectedIdx = this.fakeEl.find('.' + this.options.Pos.MIDDLE).data('idx');
    selectedIdx = --selectedIdx;
    goTo.call(this, selectedIdx);
    e.preventDefault();
  };

  var onEffectChangeHandler = function(e) {
    this.effect = $(this.options.Ids.EFFECT_SELECT).val();
  };

  var onDurationChangeHandler = function(e) {
    this.duration = $(this.options.Ids.DURATION_TXTBOX).val();
  };

  var onIntervalChangeHandler = function(e) {
    this.interval = $(this.options.Ids.INTERVAL_TXTBOX).val();
  };

  var onTouchStartHandler = function(e) {
    if (this.isAnimating) {
      return;
    }
    var opts = this.options;
    this.fakeEl.find('.' + opts.Pos.LEFT).removeClass(opts.Pos.LEFT);
    this.fakeEl.find('.' + opts.Pos.RIGHT).removeClass(opts.Pos.RIGHT);
    this.isTouching = true;
    var len = this.fakeEl.data('len');
    this.touchedItem = this.fakeEl.find('.' + this.options.Pos.MIDDLE);
    var selectedIdx = this.touchedItem.data('idx');
    this.x = e.originalEvent.changedTouches[0].pageX;
    var nextIdx = selectedIdx + 1;
    var prevIdx = selectedIdx - 1;
    if (prevIdx < 0) {
      prevIdx = len - 1;
    }
    if (nextIdx >= len) {
      nextIdx = 0;
    }
    this.nextSlide = this.fakeEl.find('.' + opts.Classes.SLIDER + '[data-idx="' + nextIdx + '"]');
    this.prevSlide = this.fakeEl.find('.' + opts.Classes.SLIDER + '[data-idx="' + prevIdx + '"]');
    this.w = this.fakeEl.width();
    prepareNextSlide.call(this, nextIdx);
    preparePrevSlide.call(this, prevIdx);
  };

  var onTouchMoveHandler = function(e) {
    if (!this.isTouching) {
      return;
    }
    if (this.isAnimating) {
      return;
    }
    var distance = e.originalEvent.changedTouches[0].pageX - this.x;
    this.touchedItem.css('left', distance);
    this.nextSlide.css('left', distance - this.w);
    this.prevSlide.css('left', distance + this.w);
  };

  var onTouchEndHandler = function(e) {
    if(!this.isTouching) {
      return;
    }
    if (this.isAnimating) {
      return;
    }
    this.isTouching = false;
    var distance = e.originalEvent.changedTouches[0].pageX - this.x;
    var idx = this.fakeEl.find('a.selected').html() - 1;
    var len = this.fakeEl.data('len');
    this.fakeEl.find('a.selected').removeClass('selected');
    if (distance > 0) {
      var nextIdx = idx + 1;
      if(nextIdx >= len) {
        nextIdx = 0;
      }
      slideNextTo.call(this, nextIdx);
      this.fakeEl.find('a[href="#' + nextIdx + '"]').addClass('selected');
    } else {
      var prevIdx = idx - 1;
      if(prevIdx < 0) {
        prevIdx = len - 1;
      }
      slidePrevTo.call(this, prevIdx);
      this.fakeEl.find('a[href="#' + prevIdx + '"]').addClass('selected');
    }
  };

  function Plugin(element, options) {
    this.element = $(element);
    this.options = $.extend({}, $.fn[pluginName].defaults, this.element.data(), options);
    this.element.before(createBtn('init', this.init.bind(this)))
      .before(createBtn('destroy', this.destroy.bind(this)));
  }

  Plugin.prototype = {
    init: function() {
      if (this.fakeEl) {
        return;
      }
      var that = this;
      var opts = this.options;
      this.isAnimating = false;
      this.effect = opts.effect || opts.Effects.SLIDE;
      this.interval = opts.interval || opts.TIMER.INTERVAL;
      this.duration = opts.duration || opts.TIMER.duration;
      this.curSlide = 0;
      this.fakeEl = this.element.clone(true, true);
      this.element.replaceWith(this.fakeEl);
      toFakeEl.call(this);
      setMiddle.call(this, 0);
      this.fakeEl.find('a').eq(0).addClass('selected');
      this.fakeEl.before(createAdditionalBtn.call(this));
      setValForAdditionalBtn.call(this, this.effect, this.duration, this.interval);

      if (this.fakeEl.data('autoplay')) {
        setTimeout(autoPlay.bind(this), this.interval);
      }

      this.fakeEl.find('a').on('click.' + pluginName, onNavigatorClickHandler.bind(this));
      this.fakeEl.find('.btn.next').on('click.' + pluginName, onNextClickHandler.bind(this));
      this.fakeEl.find('.btn.prev').on('click.' + pluginName, onPrevClickHandler.bind(this));
      doc.on('keydown', function(e) {
        if (e.which === 37) { //LEFT ARROW
          onPrevClickHandler.call(that);
        }
      });
      doc.on('keydown', function(e) {
        if (e.which === 39) { //RIGHT ARROW
          onNextClickHandler.call(that);
        }
      });
      this.fakeEl.find('.' + opts.Classes.SLIDER)
        .on('touchstart.' + pluginName, onTouchStartHandler.bind(this))
        .on('touchmove.' + pluginName, onTouchMoveHandler.bind(this))
        .on('touchend.' + pluginName, onTouchEndHandler.bind(this));
    },

    changeEffect: function(effect) {
      this.effect = effect;
    },

    changeDuration: function(duration) {
      this.duration = duration;
    },

    changeInterval: function(interval) {
      this.interval = interval;
    },

    destroy: function() {
      if (this.fakeEl) {
        this.fakeEl.replaceWith(this.element);
        doc.off('.' + pluginName);
        this.fakeEl.remove();
        this.fakeEl = null;
        $.removeData(this.element[0], pluginName);
      }
    }
  };

  $.fn[pluginName] = function(options, params) {
    return this.each(function() {
      var instance = $.data(this, pluginName);
      if (!instance) {
        $.data(this, pluginName, new Plugin(this, options));
      } else if (instance[options]) {
        instance[options](params);
      } else {
        window.console && console.log(options ? options + ' method is not exists in ' + pluginName : pluginName + ' plugin has been initialized');
      }
    });
  };

  $.fn[pluginName].defaults = {
    Pos: {
      LEFT: 'pos-left',
      MIDDLE: 'pos-middle',
      RIGHT: 'pos-right',
      BELOW: 'pos-below',
      ABOVE: 'pos-above',
      LEFT_VAL: '-100%',
      RIGHT_VAL: '100%',
      MIDDLE_VAL: 0,
    },
    Animate: {
      TO_MIDDLE: 'to-middle',
      MIDDLE_TO_LEFT: 'middle-to-left',
      MIDDLE_TO_RIGHT: 'middle-to-right',
      FADE_IN: 'in',
      FADE_OUT: 'out'
    },
    Effects: {
      SLIDE: 'slide',
      SHOW: 'show',
      FADE: 'fade',
      RANDOM: 'random'
    },
    Classes: {
      CONTAINER: 'slide-container',
      SLIDER: 'slider',
      HIDE: 'hide'
    },
    Ids: {
      EFFECT_SELECT: '#effect-select',
      DURATION_BTN: '#duration-btn',
      INTERVAL_BTN: '#interval-btn',
      DURATION_TXTBOX: '#duration-input',
      INTERVAL_TXTBOX: '#interval-input',
    },
    TIMER: {
      INTERVAL: 2000,
      DURATION: '2s'
    }
  };

  $(function() {
    $('[data-' + pluginName + ']')[pluginName]({});
    $('[data-' + pluginName + ']').on('customEvent', function() {
      // to do
    });
  });

}(jQuery, window));
