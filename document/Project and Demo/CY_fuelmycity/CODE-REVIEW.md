Frontend Code Review
===========

## Date: 08/06/2015
  - **Branch**: frontend-template
  - **Reviewed by**: vinh.pq
  - **Status**: fixed
  - **Fixed by**: truong.tt

### File: \source\assets\js\back-to-top.js
  - Issue 1: Using $(document) instead of using $('html, body')

### File: \source\assets\js\animate-scroll.js
  - Issue 1: Declare "that" variable not necessary(line 21)

### File: \source\assets\js\home-slider.js
  - Issue 1: There are four unused variables when i check this file with jshint
  - Issue 2: Should use "that" variable if you declared. It will make code clearly
  ```
  ele = this.element
  ```

### File: \source\assets\js\dropdown.js
  - Issue 1: There are three unused variables when i check this file with jshint
  - Issue 2: (line 106 to line 138) why are they placed here?

### File: \source\assets\js\load-more.js
  - Issue 1: There is one unused variable when i check this file with jshint
  - Issue 2:(line 50) declare "that" variable not necessary

### File: \source\assets\js\popup.js
  - Issue 1: There are seven unused variables when i check this file with jshint
  - Issue 2: (line 31 to line 81, line 88, line 155 to line 180,line 183 to line 200, line 206 to line 215)should use "that" variable if you declared. It will make code clearly
  - Issue 3: Using $(document) instead of using $('html, body')

### File: \source\assets\js\rating.js
  - Issue 1: There are two unused variables and six warnings when i check this file with jshint
  - Issue 2: (line 83, line 104) should use "that" variable if you declared. It will make code clearly

### File: \source\assets\js\share.js
  - Issue 1: (line 41) should use "that" variable if you declared. It will make code clearly

### File: \source\assets\js\scrolling.js
  - Issue 1: There is one warning when i check this file with jshint(line 34)

### File: \source\assets\js\social-feed.js
  - Issue 1: There are twelve warnings and sixteen unused variables when i check this file with jshint
  - Issue 2: Should use "that" variable if you declared. It will make code clearly

### File: \source\assets\js\tab.js
  - Issue 1: There is one unused variable when i check this file with jshint
  - Issue 2: Should use "that" variable if you declared. It will make code clearly

## Date: 29/05/2015
  - **Branch**: frontend-social-feed-page
  - **Reviewed by**: son.pham
  - **Status**: Closed
  - **Fixed by**: truong.tt

### File: \source\assets\js\social-feed.js
  - Issue 1: Should remove hard code 500.
  ```
  loadingEle.fadeOut(500);
  loadingEle.fadeOut(500);
  .replace('{{altThumb}}', 'Social feed image');
  ```

  - Issue 2: Duplicated code in some function. Should using variable instead.
  ```
  idx + 1
  idx - 1
  arrDataDisplayed.length
  arrData.length
  tempArr.length
  ```

### File: \source\assets\js\animate-scroll.js
  - Issue 1: Duplicated code.
  ```
  $(window)
  el.offset().top
  win.scrollTop()
  ```

## Date: 22/05/2015
  - **Branch**: frontend-post-detail-page
  - **Reviewed by**: an.nvt
  - **Status**: Open
  - **Fixed by**: yen.phan

### File: [source/views/post-details.jade](source/views/post-details.jade)
  - Issue 1: Class name
    + Add class `.cover-block` for `<figure>` and move next element - `.grid-fluid` out cover block
    + Add class `.share-block` for share, rating block`

  - Isuse 2: Content dynamic
    + Follow demo at [http://jsfiddle.net/nm4tutau/](http://jsfiddle.net/nm4tutau/) for order number

### File: [source/views/about.jade](source/views/about.jade)
  - Isuse 1: No need multiple row
    + Just using 1 `.row` with 3 `.col-sm-4` for heading block

## Date: 21/05/2015
  - **Branch**: frontend-home-page
  - **Reviewed by**: an.nvt
  - **Status**: Closed
  - **Fixed by**: yen.phan

### File: [source/views/blocks/footer.jade](source/views/blocks/footer.jade)
  - Issue 1: missing class name
    + Add class `.nav` for navigation link at footer

  - Isuse 2: No need multiple row
    + Just using 1 `.row` with 2 `.col-sm-3` and 1 `.col-sm-6` for footer

### File: [source/views/about.jade](source/views/about.jade)
  - Isuse 1: No need multiple row
    + Just using 1 `.row` with 3 `.col-sm-4` for heading block

## Date: 18/05/2015
  - **Branch**: frontend-home-page
  - **Reviewed by**: son.pham
  - **Status**: Closed
  - **Fixed by**: truong.tt

### File: \source\assets\js\social-feed.js
  - Issue 1: remove type: 'GET' in ajax because this is default.

    ```
    $.ajax({
      url: serverUrl,
      type: 'GET',
      dataType: 'json',
    ```

  - Issue 2: should hide loading after render data.

    ```
    loadingEle.hide();
    showGridSocialFeed.call(that, idx);
    ```
    change to
    ```
    showGridSocialFeed.call(that, idx);
    loadingEle.hide();
    ```

  - Issue 3:
    ```
    if (typeof itemData[fieldFeed.POST_IMG] !== 'undefined') {
      ...
    }
    ```
    should change to
    ```
    if (itemData[fieldFeed.POST_IMG]) {
      ...
    }
    ```

## Date: 13/05/2015 (reverse chronological order)
  - **Branch**: frontend-home-page
  - **Reviewed by**: an.nvt
  - **Status**: Closed
  - **Fixed by**: yen.phan

### File: [source/views/blocks/single-post.jade](source/views/blocks/single-post.jade)
  - Issue 1: missing ID for scroll to
    + Add ID for this block to scroll from another blocks

