Frontend Technical Document
===========================

## Project overview
  - Project summary

## Source control
  - GIT repository @ [http://gitlab.dan.com.sg/caltex/fuelmycity.git](http://gitlab.dan.com.sg/caltex/fuelmycity.git)

## Technical used
  - HTML5
  - CSS3
  - JavaScript
  - Build tools
    + NodeJS
    + Grunt
    + Jade
    + Less
  - Frameworks
    + jQuery
  - Plugins
    + Slide show @ [http://kenwheeler.github.io/slick/](http://kenwheeler.github.io/slick/)
    + Back to top
    + Popup
    + Social feed
    + Scroll to
    + Dropdown
    + Scrolling
    + Rating
    + Share
    + Animation scroll

## Tracking / Tagging
  - Google Analytics

## Social network integration
  - Facebook
  - Twitter
  - Instagram

## Code standards
  - Follow Frontend code conventions
    + Checklist
    + Unit test
  - Follow W3C standards
  - Follow WCAG 2.0 Standards

## OSs supported
  - Windows 7, 8, 8.1
  - OSX

## Browsers supported
  - Microsoft Internet Explorer 10, 11
  - Firefox latest
  - Chrome latest
  - Safari latest

## Devices supported
  - Tablet:
    + iOS 6+ (iPad 4, iPad Air)
    + Android 4.2+ (Galaxy Tab 3)
  - Mobile:
    + iOS 6+ (iPhone 5/6/6+)
    + Android 4.2+ (Galaxy S4/S5)
    + Windows Phone 8.0

## Responsive approach
  - Breakpoints
    + Large screen: 1200 and above
    + Medium screen: 1024 to 1199
    + Small screen: 768 to 1023
    + Extra small screen: below 768
