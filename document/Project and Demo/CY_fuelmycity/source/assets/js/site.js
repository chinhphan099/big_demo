/**
 * @name Site
 * @description Define global variables and functions
 * @version 1.0
 */
var Site = (function($, window, undefined) {

  function checkWidthWindow() {
    if (window.Modernizr.touch) {
      return $(window).width();
    }
    else {
      if (navigator.userAgent.match(/safari/i) && !navigator.userAgent.match(/chrome/i)) {
        return document.documentElement.clientWidth;
      }
      else {
        return window.innerWidth || document.documentElement.clientWidth;
      }
    }
  }

  function isMobile() {
    return window.Modernizr.mq('(max-width: 767px)');
  }

  function setCookie(nameCookie, valueCookie, path) {
    document.cookie = nameCookie + '=' + valueCookie + '; path=' + path;
  }

  function getCookie(nameCookie) {
    var theCookie = document.cookie.split(';'),
        entry;
    for (var i = 0, l = theCookie.length; i < l; i++) {
      entry = theCookie[i];
      if (entry.charAt(0) === ' ') {
        entry = entry.substring(1);
      }
      if (entry.indexOf(nameCookie) === 0) {
        return entry;
      }
    }
    return '';
  }

  return {
    widthWindow: checkWidthWindow,
    isMobile: isMobile,
    setCookie: setCookie,
    getCookie: getCookie
  };

})(jQuery, window);

jQuery(function() {
});
