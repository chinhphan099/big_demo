/**
 *  @name: tab
 *  @description: used to switch between tabs
 *  @version 1.0
 *  @options
 *    activeClass: class to identify current tab
 *    handlers: element trigger to switch between tabs
 *    contents: content of each tab
 *  @events
 *    no event
 *  @methods
 *    init
 *    destroy
 */
;(function($, window, undefined) {
  'use strict';

  var pluginName = 'tab';

  function Plugin(element, options) {
    this.element = $(element);
    this.options = $.extend({}, $.fn[pluginName].defaults, this.element.data(), options);
    this.init();
  }

  Plugin.prototype = {
    init: function() {
      var that = this,
        opt = that.options,
        el = that.element,
        tabs = $(opt.tabs, el),
        heading = $('.heading', el),
        contents = $(opt.contents, el);

      tabs.each(function(index) {
        var tab = $(this);

        tab.off('click.' + pluginName).on('click.' + pluginName, function(e) {
          var self = $(this);
          e.preventDefault();

          if (self.hasClass(opt.activeClass)) {
            return;
          }

          var tabName = self.data('handler');

          if (tabName === 'map-view') {
            el.addClass('map-view');
            heading.removeClass('white-state');
          } else {
            el.removeClass('map-view');
            heading.addClass('white-state');
          }

          tabs.removeClass(opt.activeClass);
          tab.addClass(opt.activeClass);

          contents.addClass(opt.hiddenClass).eq(index).removeClass(opt.hiddenClass);

          el.trigger('afterChange.' + pluginName, [tabName]);
        });
      });
    },
    destroy: function() {
      $.removeData(this.element[0], pluginName);
    }
  };

  $.fn[pluginName] = function(options, params) {
    return this.each(function() {
      var instance = $.data(this, pluginName);
      if (!instance) {
        $.data(this, pluginName, new Plugin(this, options));
      } else if (instance[options]) {
        instance[options](params);
      } else {
        window.console && console.log(options ? options + ' method is not exists in ' + pluginName : pluginName + ' plugin has been initialized');
      }
    });
  };

  $.fn[pluginName].defaults = {
    activeClass: 'current',
    tabs: '[data-handler]',
    contents: '[data-content]',
    hiddenClass: 'hidden'
  };

  $(function() {
    $('[data-' + pluginName + ']')[pluginName]({});
    $('[data-' + pluginName + ']').on('afterChange.' + pluginName, function(e, tabName) {
      var self = $(this),
          userType = $('#filter-user').find('.scopes').find('.selected').children('span').data('val');

      $('#loading').fadeIn(500, function() {

        if (tabName === 'map-view') {
          self['social-feed']('setGirdView', false);
        }
        else {
          self['social-feed']('setGirdView', true);
        }
        self['social-feed']('setUserType', userType);
        self['social-feed']('ifyUserData');
        self['social-feed']('checkPaging');
        self['social-feed']('showSocialFeed', true);
      });
    });
  });

}(jQuery, window));
