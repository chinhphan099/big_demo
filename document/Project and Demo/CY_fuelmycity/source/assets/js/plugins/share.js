/**
 *  @name share
 *  @description share an url on social network
 *  @version 1.0
 *  @options
 *      share: name of social network need share
 *      shareUrl: an url to share on social network
 *      width: width of popup
 *      height: height of popup
 *  @events
 *    no event
 *  @methods
 *    init
 *    destroy
 */
;(function($, window, undefined) {
  'use strict';

  var pluginName = 'share',
    socialUrl = {
      facebook: 'http://www.facebook.com/sharer.php?u=',
      twitter: 'https://twitter.com/intent/tweet?',
      googleplus: 'https://plus.google.com/share?url='
    };

  var fbShare = function(link, picture, caption, description) {
    FB.ui( {
        method: 'feed',
        name: 'Caltex Fuel My City',
        link: link,
        picture: picture,
        caption: caption,
        description: description
    }, function( response ) {
        // do nothing
    } );
  };

  function Plugin(element, options) {
    this.element = $(element);
    this.options = $.extend({}, $.fn[pluginName].defaults, this.element.data(), options);
    this.init();
  }

  Plugin.prototype = {
    init: function() {
      var that = this,
        ele = this.element,
        win = $(window),
        opt = that.options,
        position = {
          top: win.height() / 2 - opt.height / 2,
          left: win.width() / 2 - opt.width / 2
        },
        href = ele.attr('href'),
        socialName = ele.data(pluginName),
        isFooter = ele.data('on-footer'),
        shareUrl;


        if (socialName === 'twitter') {
          href = socialUrl.twitter + 'text=' + ele.data('text');
          ele.attr('href', href);
        }


      that.element.off('click.' + pluginName).on('click.' + pluginName, function(e) {
        e.preventDefault();
        if (socialName === 'facebook') {
          fbShare(ele.data('bitly-link'), ele.data('img'), ele.data('caption'), ele.data('text'));
        }
        if (socialName === 'googleplus') {
          shareUrl = ele.data('share-url') ? ele.data('share-url') : window.location.href;
          window.open(socialUrl['googleplus'] + shareUrl, 'Share', 'width=' + opt.width + ', height=' + opt.height + ', top=' + position.top + ', left=' + position.left);
        }
      });
    },
    destroy: function() {
      $.removeData(this.element[0], pluginName);
    }
  };

  $.fn[pluginName] = function(options, params) {
    return this.each(function() {
      var instance = $.data(this, pluginName);
      if (!instance) {
        $.data(this, pluginName, new Plugin(this, options));
      } else if (instance[options]) {
        instance[options](params);
      } else {
        window.console && console.log(options ? options + ' method is not exists in ' + pluginName : pluginName + ' plugin has been initialized');
      }
    });
  };

  $.fn[pluginName].defaults = {
    share: 'facebook',
    shareUrl: '',
    width: 500,
    height: 380
  };

  $(function() {
    $('[data-' + pluginName + ']')[pluginName]({});
  });

}(jQuery, window));
