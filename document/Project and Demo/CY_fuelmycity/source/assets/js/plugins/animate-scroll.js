/**
 *  @name animate-scroll
 *  @description: add animated into specific element when scroll
 *  @version 1.0
 *  @options
 *     animateClass: animated class to add to element
 *     offset: distance appear of element before applying animated
 *  @events
 *    no event
 *  @methods
 *    init
 *    destroy
 */
;(function($, window, undefined) {
  'use strict';

  var pluginName = 'animate-scroll',
      win = $(window);

  var animation = function() {
      var el = this.element,
        opt = this.options,
        scrollTop = win.scrollTop(),
        offsetTop = el.offset().top,
        isMobile = Site.isMobile(),
        target = {
          top: offsetTop + opt.offset,
          bottom: offsetTop + opt.offset + el.height()
        },
        winSize = {
          top: scrollTop,
          bottom: scrollTop + win.height()
        };

      if (target.top < winSize.bottom && target.bottom > winSize.top && !isMobile) {
        el.addClass(opt.animateClass);
      }

    };

  function Plugin(element, options) {
    this.element = $(element);
    this.options = $.extend({}, $.fn[pluginName].defaults, this.element.data(), options);
    this.init();
  }

  Plugin.prototype = {
    init: function() {
      var that = this;

     win.on('scroll.' + pluginName, function() {
        animation.call(that);
      });

      win.on('load.' + pluginName, function(){
        animation.call(that);
      });

    },
    destroy: function() {
      $.removeData(this.element[0], pluginName);
    }
  };

  $.fn[pluginName] = function(options, params) {
    return this.each(function() {
      var instance = $.data(this, pluginName);
      if (!instance) {
        $.data(this, pluginName, new Plugin(this, options));
      } else if (instance[options]) {
        instance[options](params);
      } else {
        window.console && console.log(options ? options + ' method is not exists in ' + pluginName : pluginName + ' plugin has been initialized');
      }
    });
  };

  $.fn[pluginName].defaults = {
    animateClass: 'scrolling',
    offset: 100
  };

  $(function() {
    $('[data-' + pluginName + ']')[pluginName]({});
  });

}(jQuery, window));
