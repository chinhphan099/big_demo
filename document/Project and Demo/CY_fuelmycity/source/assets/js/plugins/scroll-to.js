/**
 *  @name scroll-to
 *  @description
 *  @version 1.0
 *  @options
 *      duration: time to scroll to bottom
 *  @events
 *    no event
 *  @methods
 *    init
 *    destroy
 */
;(function($, window, undefined) {
  'use strict';

  var pluginName = 'scroll-to';

  function Plugin(element, options) {
    this.element = $(element);
    this.options = $.extend({}, $.fn[pluginName].defaults, this.element.data(), options);
    this.init();
  }

  Plugin.prototype = {
    init: function() {
      var that = this,
        el = that.element,
        opt = that.options,
        offset = 0,
        deviation = $(opt.deviation),
        htmlBody = $('html, body'),
        target = $(el.data(pluginName));

      el.off('click.' + pluginName).on('click.' + pluginName, function(e) {
        e.preventDefault();
        if (Site.isMobile()) {
          offset = deviation.hasClass('open') ? deviation.find('.main-nav').height() : 0;
        } else {
          offset = $('body').hasClass('home') ? deviation.height() : 0;
        }
        htmlBody.animate({
          scrollTop: (target.length) ? target.offset().top - offset : $(document).height() - $(window).height()
        }, opt.duration);
      });

    },
    destroy: function() {
      $.removeData(this.element[0], pluginName);
    }
  };

  $.fn[pluginName] = function(options, params) {
    return this.each(function() {
      var instance = $.data(this, pluginName);
      if (!instance) {
        $.data(this, pluginName, new Plugin(this, options));
      } else if (instance[options]) {
        instance[options](params);
      } else {
        window.console && console.log(options ? options + ' method is not exists in ' + pluginName : pluginName + ' plugin has been initialized');
      }
    });
  };

  $.fn[pluginName].defaults = {
    duration: 1000,
    deviation: '#header'
  };

  $(function() {
    $('[data-' + pluginName + ']')[pluginName]({});
  });

}(jQuery, window));
