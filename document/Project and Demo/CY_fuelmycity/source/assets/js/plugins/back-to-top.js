/**
 *  @name back-to-top
 *  @description
 *  @version 1.0
 *  @options
 *      duration: time to scroll to top
 *  @events
 *    no event
 *  @methods
 *    init
 *    destroy
 */
;(function($, window, undefined) {
  'use strict';

  var pluginName = 'back-to-top';

  function Plugin(element, options) {
    this.element = $(element);
    this.options = $.extend({}, $.fn[pluginName].defaults, this.element.data(), options);
    this.init();
  }

  Plugin.prototype = {
    init: function() {
      var that = this,
        el = that.element,
        htmlBody = $('html, body'),
        target = $(el.data(pluginName)),
        destination = target.length ? target : $('html');

      el.off('click.' + pluginName).on('click.' + pluginName, function(e) {
        e.preventDefault();
        htmlBody.animate({
          scrollTop: destination.offset().top
        }, that.options.duration);
      });

    },
    destroy: function() {
      $.removeData(this.element[0], pluginName);
    }
  };

  $.fn[pluginName] = function(options, params) {
    return this.each(function() {
      var instance = $.data(this, pluginName);
      if (!instance) {
        $.data(this, pluginName, new Plugin(this, options));
      } else if (instance[options]) {
        instance[options](params);
      } else {
        window.console && console.log(options ? options + ' method is not exists in ' + pluginName : pluginName + ' plugin has been initialized');
      }
    });
  };

  $.fn[pluginName].defaults = {
    duration: 1000
  };

  $(function() {
    $('[data-' + pluginName + ']')[pluginName]({});
  });

}(jQuery, window));
