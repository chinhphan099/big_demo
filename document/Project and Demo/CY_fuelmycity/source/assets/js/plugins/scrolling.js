/**
 *  @name scrolling
 *  @description: show/hide a specific element when scroll
 *  @version 1.0
 *  @options
 *      activeClass: class used to show/hide element
 *  @events
 *    no event
 *  @methods
 *    init
 *    destroy
 */
;(function($, window, undefined) {
  'use strict';

  var pluginName = 'scrolling';

  function Plugin(element, options) {
    this.element = $(element);
    this.options = $.extend({}, $.fn[pluginName].defaults, this.element.data(), options);
    this.init();
  }

  Plugin.prototype = {
    init: function() {
      var that = this,
        el = that.element,
        opt = that.options,
        win = $(window);

      if ($('body').hasClass('home') && !Site.isMobile()) {
        win.off('load.' + pluginName).on('load.' + pluginName, function(){
          win.off('scroll.' + pluginName).on('scroll.' + pluginName, function() {
            if (win.scrollTop() > 0) {
              el.addClass(opt.activeClass);
            }
            else {
              el.removeClass(opt.activeClass);
            }
          }).trigger('scroll.' + pluginName);
        }).trigger('scroll.' + pluginName);
      }

    },
    destroy: function() {
      $.removeData(this.element[0], pluginName);
    }
  };

  $.fn[pluginName] = function(options, params) {
    return this.each(function() {
      var instance = $.data(this, pluginName);
      if (!instance) {
        $.data(this, pluginName, new Plugin(this, options));
      } else if (instance[options]) {
        instance[options](params);
      } else {
        window.console && console.log(options ? options + ' method is not exists in ' + pluginName : pluginName + ' plugin has been initialized');
      }
    });
  };

  $.fn[pluginName].defaults = {
    activeClass: 'scrolling'
  };

  $(function() {
    $('[data-' + pluginName + ']')[pluginName]({});
  });

}(jQuery, window));
