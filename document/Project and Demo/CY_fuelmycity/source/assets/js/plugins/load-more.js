/**
 *  @name load-more
 *  @description: load dynamic post from server by ajax
 *  @version 1.0
 *  @options
 *    template: template of each post
 *    container: contain all posts on page
 *    loading: loading icon while load ajax
 *    overlay: overlay to prevent interactive of user while load ajax
 *    hiddenClass: class to hide load-more button while load ajax
 *  @events
 *    no event
 *  @methods
 *    init
 *    destroy
 */
;(function($, window, undefined) {
  'use strict';

  var pluginName = 'load-more',
    setting = settings.journeys;

  var template = function(data) {
    var that = this,
      html = '';

    $.each(data, function(index, value) {

      var keys = Object.keys(value).map(function(key) {
          return '{' + key + '}';
        }),
        regex = new RegExp(keys.join('|'), 'gi'),
        template = that.options.template.replace(regex, function(matched) {
          var property = matched.slice(1, matched.length - 1).toString();
          return value[property];
        });

      template = template.replace('{animate}', (index % 2 !== 0 && !Site.isMobile()) ? 'scrolling' : '');

      html += (index % 2) ? template + '</div>' : '<div class="row">' + template;
      html += (data.length % 2 !== 0 && index + 1 === data.length) ? '</div>' : '';

    });

    that.vars.container.append(html);
    overlay.call(that, 'hide');
  };

  var overlay = function(action) {
    var opt = this.options,
      overlay = this.vars.overlay,
      loading = this.vars.loading;

    switch (action) {
      case 'hide':
        loading.addClass(opt.hiddenClass);
        overlay.addClass(opt.hiddenClass);
        break;
      case 'show':
        loading.removeClass(opt.hiddenClass);
        overlay.removeClass(opt.hiddenClass);
        break;
    }

  };

  var ajax = function() {
    var that = this,
      el = that.element,
      opt = that.options;

    overlay.call(that, 'show');

    $.ajax(setting).done(function(res) {

      if (!$.isEmptyObject(res)) {

        if (res.url) {
          setting.url = res.url;
        } else {
          el.addClass(opt.hiddenClass);
        }
        template.call(that, res.data);
      } else {
        overlay.call(that, 'hide');
      }

    }).fail(function() {
      overlay.call(that, 'hide');
    });

  };

  function Plugin(element, options) {
    this.element = $(element);
    this.options = $.extend({}, $.fn[pluginName].defaults, this.element.data(), options);
    this.init();
  }

  Plugin.prototype = {
    init: function() {
      var that = this,
        opt = that.options;

      that.vars = {
        container: $(opt.container),
        loading: $(opt.loading),
        overlay: $(opt.overlay)
      };

      that.element.off('click.' + pluginName).on('click.' + pluginName, function(e) {
        e.preventDefault();
        e.stopPropagation();
        ajax.call(that);
      });

      that.vars.overlay.off('click.' + pluginName).on('click.' + pluginName, function() {
        return false;
      });
    },
    destroy: function() {
      $.removeData(this.element[0], pluginName);
    }
  };

  $.fn[pluginName] = function(options, params) {
    return this.each(function() {
      var instance = $.data(this, pluginName);
      if (!instance) {
        $.data(this, pluginName, new Plugin(this, options));
      } else if (instance[options]) {
        instance[options](params);
      } else {
        window.console && console.log(options ? options + ' method is not exists in ' + pluginName : pluginName + ' plugin has been initialized');
      }
    });
  };

  $.fn[pluginName].defaults = {
    container: '.post-list',
    template: '<div class="col-sm-6"><a href="{post-url}" title="alt" class="post-preview {animate}">' +
      '<div class="thumb"><img src="{post-image}" alt="{alt-image}"></div>' +
      '<div class="content"><h3 class="title">{title}</h3>' +
      '<div class="desc"><span class="name">{user-name}</span><span class="year">{year}</span></div>' +
      '</div></a></div>',
    overlay: '#overlay',
    loading: '#loading',
    hiddenClass: 'hidden'
  };

  $(function() {
    $('[data-' + pluginName + ']')[pluginName]({});
  });

}(jQuery, window));
