/**
 *  @name rating
 *  @description: used to rating an article or post
 *  @version 1.0
 *  @options
 *    active: class used to active star
 *  @events
 *    no event
 *  @methods
 *    init
 *    setStars: used to handle stars
 *    getStars: used to get number of stars is activing
 *    destroy
 */
;(function($, window, undefined) {
  'use strict';

  var pluginName = 'rating',
    origin = 0,
    setting = settings.rating,
    isTouch = Modernizr.touch;

  var stars = function(current) {
    var that = this,
      el = that.element,
      prev = parseInt(el.data('index'), 10);

    if (prev && current === prev && isTouch) {
      that.setStars(current, 'removeOne');
    } else {
      that.setStars(current, 'toggle');
    }
    el.data('index', current);
  };

  var ajax = function() {
    var that = this,
      rating = that.getStars(),
      id = that.element.data('id');

    if (rating > 0) {
      setting.data = {
        id: id,
        rating: rating
      };
      $.ajax(setting).done(function(res) {
        var response = JSON.parse(res);
        if (response['status'] && response['status'].toLowerCase() === 'ok') {
          that.setStars(response['data'], 'toggle');
          origin = response['data'];
        } else {
          that.setStars(origin, 'toggle');
        }
      }).fail(function() {
        that.setStars(origin, 'toggle');
      });

    }
  };

  function Plugin(element, options) {
    this.element = $(element);
    this.options = $.extend({}, $.fn[pluginName].defaults, this.element.data(), options);
    this.init();
  }

  Plugin.prototype = {
    init: function() {
      var that = this,
        el = that.element;


      that.vars = {
        stars: $(el.data(pluginName), el)
      };

      origin = that.getStars();

      if (!isTouch) {
        that.vars.stars.off('mouseover.' + pluginName).on('mouseover.' + pluginName, function() {
          var current = $(this).index() + 1;
          stars.call(that, current);
          el.data('isClick', false);
        });

        el.off('mouseout.' + pluginName).on('mouseout.' + pluginName, function() {
          if (!el.data('isClick')) {
            that.setStars(origin, 'toggle');
          }
        });

        $(window).off('click.' + pluginName).on('click.' + pluginName, function(e) {
          e.stopPropagation();
          if ($(e.target).closest(el).length) {
            ajax.call(that);
            el.data('isClick', true);
          }
        });
      }

      that.vars.stars.off('touchstart.' + pluginName).on('touchstart.' + pluginName, function(e) {
        var current = $(this).index() + 1;
        stars.call(that, current);
        $(e.target).closest(el).length && ajax.call(that);
      });

    },
    getStars: function() {
      return this.vars.stars.filter('.' + this.options.active).length;
    },
    setStars: function(index, action) {
      var that = this,
        opt = that.options,
        stars = that.vars.stars;

      switch (action) {
        case 'add':
          stars.slice(0, index).removeClass().addClass(opt.active);
          break;
        case 'addOne':
          stars.eq(index - 1).removeClass().addClass(opt.active);
          break;
        case 'remove':
          stars.slice(0, index).removeClass();
          break;
        case 'removeOne':
          stars.eq(index - 1).removeClass();
          break;
        case 'toggle':
          stars.slice(0, index).removeClass().addClass(opt.active)
            .end().slice(index).removeClass();
          break;
      }
    },
    destroy: function() {
      $.removeData(this.element[0], pluginName);
    }
  };

  $.fn[pluginName] = function(options, params) {
    return this.each(function() {
      var instance = $.data(this, pluginName);
      if (!instance) {
        $.data(this, pluginName, new Plugin(this, options));
      } else if (instance[options]) {
        instance[options](params);
      } else {
        window.console && console.log(options ? options + ' method is not exists in ' + pluginName : pluginName + ' plugin has been initialized');
      }
    });
  };

  $.fn[pluginName].defaults = {
    active: 'active'
  };

  $(function() {
    $('[data-' + pluginName + ']')[pluginName]({});
  });

}(jQuery, window));
