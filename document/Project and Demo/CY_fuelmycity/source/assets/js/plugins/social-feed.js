/**
 *  @name social-feed
 *  @description description
 *  @version 1.0
 *  @options
 *    option
 *  @events
 *    event
 *  @methods
 *    init
 *    publicMethod
 *    destroy
 */
;(function($, window, undefined) {
  'use strict';

  var pluginName = 'social-feed',
      win = $(window),
      body = $('body');

  var templateFeedImg = '<div class="col-md-4 col-sm-5 col-sm-offset-1 col-md-offset-0 {{hiddenXS}} {{hiddenSM}}">' +
                            '<div class="social-preview" data-index="{{index}}">' +
                              '<div class="content">' +
                                '<div class="thumb">' +
                                  '<span>' +
                                    '<img src="{{imageThumb}}" alt="{{altThumb}}">' +
                                  '</span>' +
                                '</div>' +
                                '<p class="desc">{{desc}}</p>' +
                              '</div>' +
                              '<div class="author">' +
                                '<div class="user">' +
                                  '<img src="{{userAvata}}" alt="{{altUserAvata}}">' +
                                  '<span class="name">{{username}}</span>' +
                                '</div>' +
                                '<div class="social-icon">' +
                                  '<span class="{{socialLogo}}"></span>' +
                                '</div>' +
                              '</div>' +
                             '</div>' +
                          '</div>',
      templateFeedNoneImg = '<div class="col-md-4 col-sm-5 col-sm-offset-1 col-md-offset-0 {{hiddenXS}} {{hiddenSM}}">' +
                              '<div class="social-preview bgd-blue" data-index="{{index}}">' +
                                '<div class="content">' +
                                  '<p class="desc">{{desc}}</p>' +
                                '</div>' +
                                '<div class="author">' +
                                  '<div class="user">' +
                                    '<img src="{{userAvata}}" alt="{{altUserAvata}}">' +
                                    '<span class="name">{{username}}</span>' +
                                  '</div>' +
                                  '<div class="social-icon">' +
                                    '<span class="{{socialLogo}}"></span>' +
                                  '</div>' +
                                '</div>' +
                              '</div>' +
                            '</div>',
      templateGridFeedDetail = '<h2 class="title">{{locationName}}</h2>' +
                               '<div class="thumb">' +
                                  '<img src="{{imageThumb}}" alt="{{altThumb}}"/>' +
                               '</div>' +
                               '<div class="author">' +
                                  '<div class="user">' +
                                      '<img src="{{userAvata}}" alt="{{altUserAvata}}"/>' +
                                      '<span class="name">{{username}}</span>' +
                                  '</div>' +
                                  '<div class="social-icon">' +
                                      '<span class="text">{{timeFeed}}</span>' +
                                      '<span class="{{socialLogo}}"></span>' +
                                  '</div>' +
                               '</div>' +
                               '<div class="footer">' +
                                  '<div class="desc">' +
                                    '{{desc}}' +
                                  '</div>' +
                                  '<div class="button-slick">' +
                                    '<a href="javascript:;" title="Previous" class="icon-left"></a>' +
                                    '<a href="javascript:;" title="Next" class="icon-right"></a>' +
                                  '</div>' +
                               '</div>',
      errCallAjax = '<h3 class="error-ajax">' + L10n.socialFeed.errorGetFeed + '</h3>';

  var MILI_DAY = 84600000,
      loadingEle = $('#loading'),
      feedPage = {
        HOME: 'home-page',
        SOCIAL: 'social-feed-page'
      },
      fieldFeed = {
        ESTAB_NAME: 'establish-name',
        ESTAB_ADDRESS: 'establish-address',
        ESTAB_PHONE: 'establish-phone',
        ESTAB_TIME: 'establish-time',
        POST_IMG: 'post-image',
        SHORT_DESC: 'short-description',
        FULL_DESC: 'full-description',
        USER_AVATA: 'user-image',
        USERNAME: 'user-name',
        SOCIAL_NAME: 'social-name',
        FEED_TIME: 'feed-time',
        LOCATION_NAME: 'location-name'
      },
      socialName = {
        TW: 'sf-twitter',
        FB: 'sf-facebook',
        IN: 'sf-instagram'
      },
      cleanSocialContent = {
        link: function(inputText) {
          return inputText.replace(/\b(((https*\:\/\/)|www\.)[^\"\']+?)(([!?,.\)]+)?(\s|$))/g, function(link, m1, m2, m3, m4) {
            var http = m2.match(/w/) ? 'http://' : '';
            return '<a class="twtr-hyperlink" target="_blank" href="' + http + m1 + '">' + ((m1.length > 25) ? m1.substr(0, 24) + '...' : m1) + '</a>' + m4;
          });
        },
        at: function(inputText, url) {
          return inputText.replace(/\B[@＠]([a-zA-Z0-9_]{1,20})/g, function(m, username) {
            return '<a target="_blank" href="' + url + username + '">@' + username + '</a>';
          });
        },
        list: function(inputText) {
          return inputText.replace(/\B[@＠]([a-zA-Z0-9_]{1,20}\/\w+)/g, function(m, userlist) {
            return '<a target="_blank" class="twtr-atreply" href="http://twitter.com/' + userlist + '">@' + userlist + '</a>';
          });
        },
        hash: function(inputText, url) {
          return inputText.replace(/(^|\s+)#(\w+)/gi, function(m, before, hash) {
            return before + '<a target="_blank" href="' + url + hash + '">#' + hash + '</a>';
          });
        },
        clean: function(inputText, social) {
          switch(social) {
            case socialName.TW:
              return this.hash(this.at(this.list(this.link(inputText)), 'http://twitter.com/intent/user?screen_name='), 'http://twitter.com/search?q=%23');
            case socialName.IN:
              return this.hash(this.at(this.link(inputText), 'http://instagram.com/'), 'http://statigr.am/tag/');
            case socialName.FB:
              return this.hash(this.link(inputText), 'https://www.facebook.com/hashtag/');
          }
        }
      };

  var calculateTime = function(time) {
    return Math.max(Math.floor((new Date() - new Date(time)) / MILI_DAY), 0);
  };

  var findObjFeed = function(idObj) {
    return this.vars.currDataDisplayed.filter(function(obj) {
      if (obj.id === idObj) {
        return obj;
      }
    })[0];
  };

  var setScrollMap = function(isScroll) {
    if (this.vars.map) {
      this.vars.map.setOptions({scrollwheel: isScroll});
    }
  };

  var initMap = function() {
    var that = this,
        styles = [
        {
          'featureType': 'landscape',
          'elementType': 'labels',
          'stylers': [{
            'visibility': 'on'
          }]
        },
        {
          'featureType': 'transit',
          'elementType': 'labels',
          'stylers': [{
            'visibility': 'on'
          }]
        },
        {
          'featureType': 'poi',
          'elementType': 'labels',
          'stylers': [{
            'visibility': 'on'
          }]
        },
        {
          'featureType': 'water',
          'elementType': 'labels',
          'stylers': [{
            'visibility': 'off'
          }]
        },
        {
          'featureType': 'road',
          'elementType': 'labels.icon',
          'stylers': [{
            'visibility': 'off'
          }]
        },
        {
          'stylers': [{
            'hue': '#00aaff'
          }, {
            'saturation': -100
          }, {
            'gamma': 2.15
          }, {
            'lightness': 12
          }]
        },
        {
          'featureType': 'road',
          'elementType': 'labels.text.fill',
          'stylers': [{
            'visibility': 'on'
          }, {
            'lightness': 24
          }]
        },
        {
          'featureType': 'road',
          'elementType': 'geometry',
          'stylers': [{
            'lightness': 57
          }]
      }],
      modernizr = window.Modernizr,
      optMap = {
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: false,
        streetViewControl: false,
        zoomControl: true,
        zoomControlOptions: {
          style: google.maps.ZoomControlStyle.DEFAULT,
          position: google.maps.ControlPosition.RIGHT_CENTER
        },
        panControl: false,
        scrollwheel: false,
        draggable: modernizr.mobile || modernizr.tablet ? false : true
      };
    if (that.vars.mapView.length && that.vars.template === feedPage.SOCIAL) {
      that.vars.map = new google.maps.Map(that.vars.mapView.get(0), optMap);
      that.vars.map.setOptions({styles: styles});
      that.vars.bounds = new google.maps.LatLngBounds();
      google.maps.event.addListener(that.vars.map, 'mousedown', function(){
        setScrollMap.call(that, true);
      });
    }
  };

  var pushData = function(arrData, arrBePush) {
    for (var i = 0, l = arrData.length; i < l; i++) {
      arrBePush.push(arrData[i]);
    }
  };

  var getInitFeed = function(successCB, errorCB) {
    var vars = this.vars,
        that = this;
    $.when(
      $.ajax({
        url: settings.socialFeed.gridUrl.urlKOL,
        dataType: 'json',
      }),
      $.ajax({
        url: settings.socialFeed.gridUrl.urlPubs,
        dataType: 'json',
      })
    )
    .then(function(resKOL, resPUB) {
      // success
      if (vars.isGridView) {
        settings.socialFeed.gridUrl.urlKOL = resKOL[0].paging;
        settings.socialFeed.gridUrl.urlPubs = resPUB[0].paging;
        pushData(resKOL[0].data, that.vars.dataKOL);
        pushData(resPUB[0].data, that.vars.dataPublic);
      }
      if ($.isFunction(successCB)) {
        successCB();
      }
    }, function(err) {
      // fail
      if ($.isFunction(errorCB)) {
        errorCB(err);
      }
    });
  };

  var getSocialFeed = function(link, successCB, errorCB) {
    $.ajax({
      url: link,
      dataType: 'json'
    })
    .done(function(respone) {
      if ($.isFunction(successCB)) {
        successCB(respone);
      }
    })
    .fail(function(err) {
      if ($.isFunction(errorCB)) {
        errorCB(err);
      }
    });
  };

  var toggleLoadMoreFeed = function() {
    var vars = this.vars;
    if (!vars.currPaging && vars.currData.length === vars.currDataDisplayed.length) {
      vars.loadMoreGridEle.addClass(this.options.classHidden);
    }
    else {
      vars.loadMoreGridEle.removeClass(this.options.classHidden);
    }
  };

  var gotoFeed = function(act) {
    var that = this,
        vars = that.vars,
        lengthData = vars.currData.length,
        lengthDataDisplayed = vars.currDataDisplayed.length,
        tempIdx;

    that.vars.socialPopup.addClass('change').css('opacity', '');
    switch (act) {
      case 'next':
        tempIdx = vars.currentIdx + 1;
        if (tempIdx === lengthData && !vars.currPaging) {
          vars.socialPopup.find('.button-slick').children('.icon-right').remove();
          return;
        }
        if (vars.isGridView) {
          if (tempIdx === lengthDataDisplayed && lengthDataDisplayed < lengthData) {
            that.loadMoreFeed();
            that.vars.currentIdx = tempIdx;
          }
          else if (tempIdx === lengthDataDisplayed && lengthDataDisplayed === lengthData) {
            that.loadMoreFeed(function(res) {
              pushData(res.data, that.vars.currData);
              if (that.vars.userSelected === 'KOL') {
                settings.socialFeed.gridUrl.urlKOL = res.paging;
              }
              else {
                settings.socialFeed.gridUrl.urlPubs = res.paging;
              }
              that.checkPaging();
              gotoFeed.call(that, act);
            });
            return;
          }
          else {
            that.vars.currentIdx = tempIdx;
          }
        }
        break;
      case 'prev':
        tempIdx = vars.currentIdx - 1;
        if (tempIdx < 0) {
          return;
        }
        if (vars.isGridView) {
          that.vars.currentIdx = tempIdx;
        }
        break;
    }
    setTimeout(function() {
      if (vars.isGridView) {
        appendDetailFeed.call(that, vars.currDataDisplayed[tempIdx]);
        that.vars.socialPopup.removeClass('change');
      }
      else {
        checkMarker.call(that, vars.markers[tempIdx], tempIdx);
      }
    }, 700);
  };

  var checkMapFeed = function() {
    var that = this,
        url = this.vars.userSelected === 'KOL' ? settings.socialFeed.mapUrl.urlKOL : settings.socialFeed.mapUrl.urlPubs;
    if (that.vars.currData.length) {
      showMapFeed.call(that);
    }
    else {
      getSocialFeed.call(that, url,
        function(res) {
          pushData(res.data, that.vars.currData);
          showMapFeed.call(that);
        },
        function() {
          that.element.find('.heading').after(errCallAjax);
          that.vars.loadMoreGridEle.addClass(that.options.classHidden);
          loadingEle.hide();
        });
    }
  };

  var checkMarker = function(marker, idx, act) {
    var that = this,
        idFeed = marker.idFeed,
        entry = findObjFeed.call(that, idFeed);

    that.vars.currentIdx = idx;
    if (entry) {
      appendDetailFeed.call(that, entry);
      if (act === 'clickMarker') {
        that.vars.initPopupEle.popup('show');
      }
      else {
        that.vars.socialPopup.removeClass('change');
      }
    }
    else {
      loadingEle.fadeIn(500, function() {
        getSocialFeed.call(that, settings.socialFeed.mapUrl.urlOneFeed + idFeed,
          function(res) {
            if (res.data.length) {
              that.vars.currDataDisplayed.push(res.data[0]);
              appendDetailFeed.call(that, res.data[0]);
              if (act === 'clickMarker') {
                that.vars.initPopupEle.popup('show');
              }
              else {
                that.vars.socialPopup.removeClass('change');
              }
            }
            loadingEle.hide();
          },
          function() {
            that.element.find('.heading').after(errCallAjax);
            that.vars.loadMoreGridEle.addClass(that.options.classHidden);
            loadingEle.hide();
          });
      });
    }
  };

  var clickMarker = function(that, marker, idx) {
    return function() {
      checkMarker.call(that, marker, idx, 'clickMarker');
    };
  };

  var setIcon = function(that, mouseEvent, marker) {
    var vars = that.vars,
        options = that.options;
    return function() {
      switch (mouseEvent) {
        case 'mouseover':
          vars.tempIcon = marker.getIcon();
          marker.setIcon(options.icon.black);
          break;
        case 'mouseout':
          marker.setIcon(vars.tempIcon);
          break;
      }
    };
  };

  var showItemGridFeed = function(that, entry, i) {
    var vars = that.vars,
        item = entry[fieldFeed.POST_IMG] ? templateFeedImg : templateFeedNoneImg;

    item = item
            .replace('{{index}}', i)
            .replace('{{desc}}', entry[fieldFeed.SHORT_DESC] ? cleanSocialContent.clean(entry[fieldFeed.SHORT_DESC], entry[fieldFeed.SOCIAL_NAME]) : '')
            .replace('{{userAvata}}', entry[fieldFeed.USER_AVATA] ? entry[fieldFeed.USER_AVATA] : '')
            .replace('{{altUserAvata}}', entry[fieldFeed.USERNAME] ? entry[fieldFeed.USERNAME] : '')
            .replace('{{username}}', entry[fieldFeed.USERNAME] ? entry[fieldFeed.USERNAME] : '')
            .replace('{{socialLogo}}', entry[fieldFeed.SOCIAL_NAME] ? entry[fieldFeed.SOCIAL_NAME] : '');

    if (entry[fieldFeed.POST_IMG]) {
      item = item
              .replace('{{imageThumb}}', entry[fieldFeed.POST_IMG])
              .replace('{{altThumb}}', 'Sociale feed image');
    }

    if (vars.template === feedPage.HOME && i > 1) {
      item = item.replace('{{hiddenXS}}', 'hidden-xs');
    }
    else {
      item = item.replace('{{hiddenXS}}', '');
    }

    if (vars.template === feedPage.HOME && i > 3) {
      item = item.replace('{{hiddenSM}}', 'hidden-sm');
    }
    else {
      item = item.replace('{{hiddenSM}}', '');
    }

    that.vars.gridView.append(item);
  };

  var showGridFeed = function(isFilter) {
    var vars = this.vars,
        numShowItem = vars.template === feedPage.HOME ? this.options.numHomeItem : this.options.numSocialItem,
        lenData = vars.currData.length,
        lenDataDisplayed = vars.currDataDisplayed.length,
        idxStart, limitIdx, tempArr;

    if (!isFilter) {
      tempArr = vars.currData;
      idxStart = lenDataDisplayed ? lenDataDisplayed : 0;
      limitIdx = (idxStart + numShowItem) > lenData ? lenData : idxStart + numShowItem;
    }
    else {
      tempArr = lenDataDisplayed ? vars.currDataDisplayed : vars.currData;
      idxStart = 0;
      if (!lenDataDisplayed){
        limitIdx = tempArr.length < numShowItem ? tempArr.length : numShowItem;
      }
      else {
        limitIdx = lenDataDisplayed;
      }
      vars.gridView.empty();
    }

    if (lenData && lenDataDisplayed <= lenData) {
      for (var i = idxStart, l = limitIdx; i < l; i++) {
        showItemGridFeed(this, tempArr[i], i);
        if (!isFilter) {
          this.vars.currDataDisplayed.push(tempArr[i]);
        }
        if (isFilter && limitIdx <= numShowItem && lenDataDisplayed < limitIdx) {
          this.vars.currDataDisplayed.push(tempArr[i]);
        }
      }
    }
    toggleLoadMoreFeed.call(this);
    loadingEle.fadeOut(500);
  };

  var showMapFeed = function() {
    var that = this,
        vars = that.vars,
        dataLocation = vars.currData,
        newLat, newLng, latLong, marker, numRandom;

    that.vars.markers = [];
    if (that.vars.markerCluster) {
      that.vars.markerCluster.clearMarkers();
    }
    for (var i = 0, l = dataLocation.length; i < l; i++) {
      numRandom = (Math.random() - 0.5) / 1500;
      newLat = dataLocation[i].location.latitude + numRandom;
      newLng = dataLocation[i].location.longitude + numRandom;
      latLong = new google.maps.LatLng(newLat, newLng);

      marker = new google.maps.Marker({
        position: latLong,
        map: vars.map,
        icon: vars.userSelected === 'KOL' ? that.options.icon.amb : that.options.icon.pub,
        idFeed: dataLocation[i].id
      });

      google.maps.event.addListener(marker, 'click', clickMarker(that, marker, i));
      if (!Site.isMobile && !window.Modernizr.touch) {
        google.maps.event.addListener(marker, 'mouseover', setIcon(that, 'mouseover', marker));
        google.maps.event.addListener(marker, 'mouseout', setIcon(that, 'mouseout', marker));
      }
      that.vars.bounds.extend(latLong);
      that.vars.markers.push(marker);
    }
    google.maps.event.trigger(that.vars.map, 'resize');
    that.vars.markerCluster = new MarkerClusterer(that.vars.map, that.vars.markers);
    that.vars.map.fitBounds(that.vars.bounds);
    google.maps.event.addListenerOnce(that.vars.map, "idle", function() {
      if (that.vars.map.getZoom() > 18) {
        that.vars.map.setZoom(18);
      }
    });
    loadingEle.fadeOut(500);
  };

  var appendDetailFeed = function(objFeed) {
    var that = this,
        itemData = objFeed,
        templateDetail = templateGridFeedDetail,
        isHaveImg = false,
        bodyPopup = that.vars.socialPopup.find('.body'),
        btnArrows;

    templateDetail = templateDetail
                        .replace('{{desc}}', itemData[fieldFeed.FULL_DESC] ? cleanSocialContent.clean(itemData[fieldFeed.FULL_DESC], itemData[fieldFeed.SOCIAL_NAME]) : '')
                        .replace('{{userAvata}}', itemData[fieldFeed.USER_AVATA] ? itemData[fieldFeed.USER_AVATA] : '')
                        .replace('{{altUserAvata}}', itemData[fieldFeed.USERNAME] ? itemData[fieldFeed.USERNAME] : '')
                        .replace('{{username}}', itemData[fieldFeed.USERNAME] ? itemData[fieldFeed.USERNAME] : '')
                        .replace('{{socialLogo}}', itemData[fieldFeed.SOCIAL_NAME] ? itemData[fieldFeed.SOCIAL_NAME] : '')
                        .replace('{{timeFeed}}', itemData[fieldFeed.FEED_TIME] ? calculateTime(itemData[fieldFeed.FEED_TIME]) + 'd' : '')
                        .replace('{{locationName}}', itemData[fieldFeed.LOCATION_NAME] ? itemData[fieldFeed.LOCATION_NAME] : '');

    if (itemData[fieldFeed.POST_IMG]) {
      templateDetail = templateDetail
                          .replace('{{imageThumb}}', itemData[fieldFeed.POST_IMG])
                          .replace('{{altThumb}}', 'Social feed image');
      isHaveImg = true;
    }
    else {
      templateDetail = templateDetail
                          .replace('{{imageThumb}}', '')
                          .replace('{{altThumb}}', 'Social feed image');
    }

    templateDetail = $(templateDetail);
    if (!isHaveImg) {
      templateDetail.splice(1, 1);
    }
    if (!itemData[fieldFeed.LOCATION_NAME]) {
      templateDetail.splice(0, 1);
    }

    if (that.vars.template === feedPage.HOME) {
      templateDetail.find('.button-slick').empty();
    }
    else {
      btnArrows = templateDetail.find('.button-slick');
      btnArrows
        .off('click.' + pluginName, '.icon-left')
        .on('click.' + pluginName, '.icon-left', function() {
          gotoFeed.call(that, 'prev');
        })
        .off('click.' + pluginName, '.icon-right')
        .on('click.' + pluginName, '.icon-right', function() {
          gotoFeed.call(that, 'next');
        });
      if (!that.vars.currentIdx) {
        btnArrows.children('.icon-left').remove();
      }
      if (that.vars.currentIdx === (that.vars.currData.length - 1) && !that.vars.currPaging) {
        btnArrows.children('.icon-right').remove();
      }
    }

    if (that.vars.socialPopup.length) {
      bodyPopup
        .empty().append(templateDetail);
      if (!that.vars.socialPopup.hasClass('hidden')) {
        if (bodyPopup.children('.thumb').length) {
          bodyPopup.find('.thumb img').on('load', function() {
            that.vars.initPopupEle.popup('setPosition');
          });
        }
        else {
          that.vars.initPopupEle.popup('setPosition');
        }
      }
    }
  };

  var getArrVal = function(arr, key) {
    for (var i = 0, l = arr.length; i < l; i++) {
      if (arr[i].indexOf(key) !== -1) {
        return arr[i].split('=')[1];
      }
    }
  };

  var viewOnMap = function() {
    var hrefLocation = window.location.href,
        pathname = window.location.pathname,
        id, latitude, longitude;

    if (pathname.indexOf('social-feeds') !== -1) {
      if (hrefLocation.indexOf('?') !== -1) {
        hrefLocation = hrefLocation.split('?')[1];
        this.vars.hrefLocation = hrefLocation[0];
        if (hrefLocation.indexOf('&') !== -1) {
          hrefLocation = hrefLocation.split('&');
        }
      }
      if (hrefLocation.length >= 3 && hrefLocation[2].indexOf('id') !== -1 &&
            hrefLocation[0].indexOf('latitude') !== -1 && hrefLocation[1].indexOf('longitude') !== -1) {
        id = getArrVal(hrefLocation, 'id');
        latitude = parseFloat(getArrVal(hrefLocation, 'latitude'));
        longitude = parseFloat(getArrVal(hrefLocation, 'longitude'));
        if (isNaN(id) || isNaN(latitude) || isNaN(longitude)) {
          window.location.href = pathname;
        }
        this.vars.currData.push({
          'id': id,
          'location': {
            'latitude': latitude,
            'longitude': longitude
          }
        });
        $('.heading', this.element).addClass(this.options.classHidden);
        this.element.removeClass('bgd-red').addClass('map-view');
        this.vars.gridView.closest('.grid-view').addClass(this.options.classHidden);
        this.vars.mapView.removeClass(this.options.classHidden);
        checkMapFeed.call(this);
        return 1;
      }
    }
  };

  function Plugin(element, options) {
    this.element = $(element);
    this.options = $.extend({}, $.fn[pluginName].defaults, this.element.data(), options);
    this.init();
  }

  Plugin.prototype = {
    init: function() {
      var that = this,
          ele = this.element;

      this.vars = {
        mapView: ele.find(that.options.mapViewArea),
        gridView: ele.find(that.options.gridViewArea),
        loadMoreGridEle: ele.find(that.options.loadMoreGridEle),
        initPopupEle: ele.find('[data-popup]'),
        dataAll: [],
        dataKOL: [],
        dataKOLDisplayed: [],
        dataPublic: [],
        dataPublicDisplayed: [],
        currData: [],
        currDataDisplayed: [],
        currPaging: '',
        dataKOLMap: [],
        dataKOLDisplayedMap: [],
        dataPublicMap: [],
        dataPublicDisplayedMap: [],
        userSelected: ele.data('user-default'),
        isGridView: true,
        template: ele.data(pluginName)
      };
      that.vars.socialPopup = $('#' + that.vars.initPopupEle.data('id-popup'));

      initMap.call(that);

      body.on('mousedown.' + pluginName, function(e) {
        if (!$(e.target).parents('#map').length) {
          setScrollMap.call(that, false);
        }
      });

      win.on('scroll.' + pluginName, function() {
        setScrollMap.call(that, false);
      });

      if (viewOnMap.call(that)) {
        return;
      }

      if (that.vars.template === feedPage.HOME) {
        getSocialFeed.call(that, settings.socialFeed.urlHome, function(res) {
          that.vars.dataAll = res.data;
          that.ifyUserData();
          that.showSocialFeed(false);
        },
        function() {
          ele.find('.heading').after(errCallAjax);
          that.vars.loadMoreGridEle.addClass(that.options.classHidden);
          loadingEle.hide();
        });
      }
      else {
        if (that.vars.isGridView) {
          getInitFeed.call(that, function() {
            that.ifyUserData();
            that.checkPaging();
            that.showSocialFeed(false);
          },
          function() {
            ele.find('.heading').after(errCallAjax);
            that.vars.loadMoreGridEle.addClass(that.options.classHidden);
            loadingEle.hide();
          });
        }
        else {
          that.ifyUserData();
          that.showSocialFeed();
        }
      }

      that.vars.loadMoreGridEle.on('click.' + pluginName, function(e) {
        e.preventDefault();
        that.loadMoreFeed(function(res){
          pushData(res.data, that.vars.currData);
          if (that.vars.userSelected === 'KOL') {
            settings.socialFeed.gridUrl.urlKOL = res.paging;
          }
          else {
            settings.socialFeed.gridUrl.urlPubs = res.paging;
          }
          that.checkPaging();
          that.showSocialFeed(false);
        });
      });

      that.vars.gridView
        .on('click.' + pluginName, 'a', function(e) {
          e.stopPropagation();
        })
        .on('click.' + pluginName, '[data-index]', function() {
          var idx = $(this).data('index');
          that.vars.currentIdx = idx;
          appendDetailFeed.call(that, that.vars.currDataDisplayed[idx]);
          that.vars.initPopupEle.popup('show');
        });

      that.vars.initPopupEle.on('popupHide', function() {
        if (that.vars.isGridView) {
          return;
        }
        that.resizeMapView();
      });

    },
    ifyUserData: function() {
      var vars = this.vars;
      switch (vars.userSelected) {
        case 'KOL':
          if (vars.isGridView) {
            vars.currData = vars.dataKOL;
            vars.currDataDisplayed = vars.dataKOLDisplayed;
          }
          else {
            vars.currData = vars.dataKOLMap;
            vars.currDataDisplayed = vars.dataKOLDisplayedMap;
          }
          break;
        case 'PUBLIC':
          if (vars.isGridView) {
            vars.currData = vars.dataPublic;
            vars.currDataDisplayed = vars.dataPublicDisplayed;
          }
          else {
            vars.currData = vars.dataPublicMap;
            vars.currDataDisplayed = vars.dataPublicDisplayedMap;
          }
          break;
        default:
          vars.currData = vars.dataAll;
          vars.currDataDisplayed = [];
          break;
      }
    },
    checkPaging: function() {
      var vars = this.vars;
      if (vars.userSelected === 'KOL') {
        vars.currPaging = vars.isGridView ? settings.socialFeed.gridUrl.urlKOL : '';
      }
      else if (vars.userSelected === 'PUBLIC') {
        vars.currPaging = vars.isGridView ? settings.socialFeed.gridUrl.urlPubs : '';
      }
    },
    loadMoreFeed: function(successCB, errorCB) {
      var vars = this.vars,
          that = this;

      loadingEle.fadeIn(500, function() {
        if (vars.currData.length === vars.currDataDisplayed.length) {
          if (vars.currPaging !== '') {
            getSocialFeed.call(that, vars.currPaging, successCB, errorCB);
          }
        }
        else {
          that.showSocialFeed(false);
        }
      });
    },
    showSocialFeed: function(isFilter) {
      if (this.vars.isGridView) {
        showGridFeed.call(this, isFilter);
      }
      else {
        checkMapFeed.call(this);
      }
    },
    setUserType: function(userType) {
      if (userType === 'KOL' || userType === 'PUBLIC') {
        this.vars.userSelected = userType;
      }
    },
    setGirdView: function(isGridView) {
      this.vars.isGridView = isGridView;
    },
    resizeMapView: function() {
      google.maps.event.trigger(this.vars.map, 'resize');
    },
    destroy: function() {
      // remove events
      this.vars.gridView
        .off('click.' + pluginName, 'a')
        .off('click.' + pluginName, '[data-index]');
      body.off('mousedown.' + pluginName);
      win.on('scroll.' + pluginName);
      this.vars.loadMoreGridEle.off('click.' + pluginName);
      // deinitialize
      $.removeData(this.element[0], pluginName);
    }
  };

  $.fn[pluginName] = function(options, params) {
    return this.each(function() {
      var instance = $.data(this, pluginName);
      if (!instance) {
        $.data(this, pluginName, new Plugin(this, options));
      } else if (instance[options]) {
        instance[options](params);
      } else {
        window.console && console.log(options ? options + ' method is not exists in ' + pluginName : pluginName + ' plugin has been initialized');
      }
    });
  };

  $.fn[pluginName].defaults = {
    numHomeItem: 6,
    numSocialItem: 12,
    mapViewArea: '.map',
    gridViewArea: '.social-list .row',
    loadMoreGridEle: '#more-grid-feed',
    classHidden: 'hidden',
    zoom: 3,
    centerPoint: {
      lat: 80,
      lng: -50
    },
    icon: {
      pub: settings.iconMarker.pub,
      amb: settings.iconMarker.amb,
      black: settings.iconMarker.black
    }
  };

  $(function() {
    $('[data-' + pluginName + ']')[pluginName]({});
  });

}(jQuery, window));
