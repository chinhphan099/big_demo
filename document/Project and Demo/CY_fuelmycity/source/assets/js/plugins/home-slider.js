/**
 *  @name home-slider
 *  @description modify slider from slick slider
 *  @version 1.0
 *  @options
 *    option
 *  @events
 *    init: event of slick slider
 *    afterChange: event of slick slider
 *  @methods
 *    init
 *    destroy
 */
;(function($, window, undefined) {
  'use strict';

  var pluginName = 'home-slider';

  var MIN_WIDTH_MEDIUM = 768,
      win = $(window);

  var setPositionArrows = function(arrowsObj) {
    var itemActive = this.element.find('.slick-active').children('.thumb'),
        topArrows;
    topArrows = Site.widthWindow() >= MIN_WIDTH_MEDIUM ? itemActive.height() / 2 : itemActive.height() - arrowsObj.height() - this.options.bottomArrowsMobile;
    arrowsObj.css('top', topArrows);
  };

  function Plugin(element, options) {
    this.element = $(element);
    this.options = $.extend({}, $.fn[pluginName].defaults, this.element.data(), options);
    this.init();
  }

  Plugin.prototype = {
    init: function() {
      var that = this,
          ele = that.element,
          timeout, arrows;

      win
        .off('resize.' + pluginName)
        .on('resize.' + pluginName, function() {
          if (timeout) { clearTimeout(timeout); }
          timeout = setTimeout(function() {
            setPositionArrows.call(that, arrows);
          }, 200);
        });

      ele
        .on('init.' + pluginName, function() {
          ele.find('.thumb').first().children('img').on('load', function() {
            arrows = ele.find('button.slick-prev, button.slick-next');

            win.trigger('resize.' + pluginName);
          });
        })
        .on('afterChange.' + pluginName, function() {
          setPositionArrows.call(that, arrows);
        });

      ele.slick(that.options);

    },
    initSlider: function() {
      this.element.slick(this.options);
    },
    gotoNextPrev: function(act) {
      var actSlick;
      if (act === 'next') {
        actSlick = 'slickNext';
      }
      else {
        actSlick = 'slickPrev';
      }
      this.element.slick(actSlick);
      win.triggerHandler('resize.' + pluginName);
    },
    destroy: function() {
      $.removeData(this.element[0], pluginName);
      this.element
        .slick('unslick')
        .off('init.' + pluginName)
        .off('afterChange.' + pluginName);
      win.off('resize.' + pluginName);
    }
  };

  $.fn[pluginName] = function(options, params) {
    return this.each(function() {
      var instance = $.data(this, pluginName);
      if (!instance) {
        $.data(this, pluginName, new Plugin(this, options));
      } else if (instance[options]) {
        instance[options](params);
      } else {
        window.console && console.log(options ? options + ' method is not exists in ' + pluginName : pluginName + ' plugin has been initialized');
      }
    });
  };

  $.fn[pluginName].defaults = {
    dots: false,
    fade: false,
    speed: 500,
    infinite: true,
    adaptiveHeight: false,
    autoplay: true,
    autoplaySpeed: 6000,
    pauseOnHover: false,
    bottomArrowsMobile: 20
  };

  $(function() {
    $('[data-' + pluginName + ']')[pluginName]({});
  });

}(jQuery, window));
