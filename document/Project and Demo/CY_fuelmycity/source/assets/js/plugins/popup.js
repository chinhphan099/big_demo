/**
 *  @name popup
 *  @description description
 *  @version 1.0
 *  @options
 *    option
 *  @events
 *    event
 *  @methods
 *    init
 *    publicMethod
 *    destroy
 */
;(function($, window, undefined) {
  'use strict';

  var pluginName = 'popup',
      win = $(window),
      doc = $(document),
      body = $('body'),
      container = $('#container');

  var personDetail = 'personDetail',
      socialFeedDetail = 'socialFeedDetail',
      overlay = $('#overlay'),
      template = {};


  var createPopup = function() {
    var that = this,
        options = that.options,
        ele = that.element;

    template[personDetail] = '<div class="popup-1 popup-about" id="' + ele.data('id-popup') + '">' +
                                '<div class="header">' +
                                  '<a href="javascript:;" title="Close" class="icon-close"><span></span></a>' +
                                '</div>' +
                                '<div class="body">' +
                                    '<div class="thumb">' +
                                      '<img src="" alt="About person"/>' +
                                    '</div>' +
                                    '<h3 class="title"></h3>' +
                                    '<div class="desc"></div>' +
                                    '<a href="" class="button-style" title="View post">' +
                                      '<span class="text"></span>' +
                                      '<span class="arrow-right"></span>' +
                                    '</a>' +
                                '</div>' +
                             '</div>';
    template[socialFeedDetail] = '<div class="popup-1 popup-social-feed" id="' +
                                  ele.data('id-popup') + '">' +
                                    '<div class="header">' +
                                      '<a href="javascript:;" title="Close" class="icon-close"><span></span></a>' +
                                    '</div>' +
                                    '<div class="body"></div>' +
                                 '</div>';

    switch (that.vars.templatePopup) {
      case personDetail:
        that.vars.wrapper = $(template[personDetail]);
        break;
      case socialFeedDetail:
        that.vars.wrapper = $(template[socialFeedDetail]);
        break;
    }

    body.append(that.vars.wrapper);
    that.vars.wrapper.addClass(options.classHidden);
    that.vars.closeEle = that.vars.wrapper.find('.header').children('.icon-close');
    that.vars.contentPopup = that.vars.wrapper.find('.body');

    if (!overlay.length) {
      overlay = $('<div id="overlay"></div>');
      body.append(overlay);
    }

    overlay.on('click.' + pluginName, function() {
      that.hide();
    });

    that.vars.closeEle.on('click.' + pluginName, function() {
      that.hide();
    });
  };

  var clickDetailPerson = function() {
    var that = this;
    that.element.on('click.' + pluginName, 'a[data-detail-img]', function() {
      var self = $(this);
      that.vars.wrapper
        .find('.thumb')
        .children('img')
        .attr({
          'src': self.data('detail-img'),
          'alt': self.data('detail-name')
        })
        .end().end()
        .find('.title').text(self.data('detail-name'))
        .end()
        .find('.desc').html(self.attr('data-detail-decs'))
        .end()
        .find('.button-style').attr('href', self.data('link-btn'))
        .children('.text').text(self.data('text-btn'));
      that.show();
    });
  };

  var setProperty = function() {
    this.vars.wrapper
      .removeClass(this.options.classHidden).css('opacity', 0);
    this.setPosition();
    this.vars.wrapper.css('opacity', 1);
  };

  function Plugin(element, options) {
    this.element = $(element);
    this.options = $.extend({}, $.fn[pluginName].defaults, this.element.data(), options);
    this.init();
  }

  Plugin.prototype = {
    init: function() {
      var that = this,
          ele = this.element,
          timeout;
      this.vars = {
        wrapper: null,
        closeEle: null,
        contentPopup: null,
        templatePopup: ele.data(pluginName),
        scrollPopup: null,
        heightPopup: null
      };

      createPopup.call(this);

      if (this.vars.templatePopup === personDetail) {
        clickDetailPerson.call(this);
      }

      win
        .off('resize.' + pluginName)
        .on('resize.' + pluginName, function() {
          if (that.vars.wrapper.is(':visible')) {
            if (timeout) {clearTimeout(timeout);}
            timeout = setTimeout(function() {
              that.setPosition();
            }, 200);
          }
      });
    },
    show: function() {
      var that = this;

      that.vars.mainSlier = $('[data-home-slider]');

      that.vars.topScrolled = win.scrollTop();

      overlay.fadeIn(500, function() {
        if (Site.isMobile()) {
          container.fadeOut(that.options.duration, function() {
            setProperty.call(that);
            doc.scrollTop(0);
          });
        }
        else {
          setProperty.call(that);
        }
      });
    },
    hide: function() {
      var that = this,
          options = that.options;
      if (overlay.is(':animated')) { return; }

      if (Site.isMobile()) {
        container.fadeIn(that.options.duration, function() {
          var widthWindow = Site.widthWindow();
          if (that.vars.mainSlier.length) {
            that.vars.mainSlier['home-slider']('gotoNextPrev', 'next');
            doc.scrollTop(that.vars.topScrolled);
            that.vars.wrapper.addClass(options.classHidden);
            setTimeout(function() {
              overlay.fadeOut(500);
            }, 300);
          }
          else {
            doc.scrollTop(that.vars.topScrolled);
            that.vars.wrapper.addClass(options.classHidden);
            overlay.fadeOut(500);
          }
          that.element.trigger('popupHide');
        });
      }
      else {
        doc.scrollTop(that.vars.topScrolled);
        that.vars.wrapper.addClass(options.classHidden);
        overlay.fadeOut(500);
      }
    },
    setPosition: function() {
      var heightWin = win.height(),
          widthWin = win.width(),
          widthPopup, heightPopup, heightBodyPopup, topPopup, leftPopup;

      if (!Site.isMobile()) {
        container.fadeIn(this.options.duration);
        this.vars.contentPopup.css('height', '');
        this.vars.wrapper.css({
          'left': '',
          'top': ''
        });
        heightPopup = this.vars.wrapper.height();
        widthPopup = this.vars.wrapper.width();

        if (heightWin < heightPopup) {
          topPopup = 0;
          heightBodyPopup = heightWin;
          leftPopup = (widthWin - widthPopup) / 2;
        }
        else {
          leftPopup = (widthWin - widthPopup) / 2;
          heightBodyPopup = '';
          topPopup = (heightWin - heightPopup) / 2;
        }
        this.vars.wrapper.css({
          'top': topPopup,
          'left': leftPopup
        });
        this.vars.contentPopup.css('height', heightBodyPopup);
      }
    },
    destroy: function() {
      this.vars.closeEle.off('click.' + pluginName);
      this.element.remove(this.vars.wrapper);
      $.removeData(this.element[0], pluginName);
    }
  };

  $.fn[pluginName] = function(options, params) {
    return this.each(function() {
      var instance = $.data(this, pluginName);
      if (!instance) {
        $.data(this, pluginName, new Plugin(this, options));
      } else if (instance[options]) {
        instance[options](params);
      } else {
        window.console && console.log(options ? options + ' method is not exists in ' + pluginName : pluginName + ' plugin has been initialized');
      }
    });
  };

  $.fn[pluginName].defaults = {
    duration: 300,
    classHidden: 'hidden'
  };

  $(function() {
    $('[data-' + pluginName + ']')[pluginName]({
    });
  });

}(jQuery, window));
