/**
 *  @name: dropdown
 *  @description: toggle show/hide an element
 *  @version 1.0
 *  @options
 *      target: element used to show/hide
 *      duration: time animation happen
 *      activeClass: class used to show/hide element
 *      closeByDocument: whether hide element when click on document or not?
 *      closeByTarget: whether hide element when click on target element or not?
 *  @events
 *    no event
 *  @methods
 *    init
 *    destroy
 */
;(function($, window, undefined) {
  'use strict';

  var pluginName = 'dropdown',
      win = $(window);

  function Plugin(element, options) {
    this.element = $(element);
    this.options = $.extend({}, $.fn[pluginName].defaults, this.element.data(), options);
    this.init();
  }

  Plugin.prototype = {
    init: function() {
      var that = this,
        opt = that.options,
        el = that.element,
        dropdown = $(el.data(pluginName), el),
        target = $(opt.target, el);

      if (dropdown.length) {
        dropdown.off('click.' + pluginName).on('click.' + pluginName, function(e) {
          e.preventDefault();
          e.stopPropagation();
          el.toggleClass(opt.activeClass);
          var isClick = el.data('isClick');
          if (target.selector === '.navbar-collapse' && !Site.isMobile()) {
            return;
          }
          (isClick) ? target.stop(true, true).slideUp(opt.duration): target.stop(true, true).slideDown(opt.duration);
          el.data('isClick', !isClick);

        });
      }

      if (opt.closeByTarget) {
        target.off('click.' + pluginName).on('click.' + pluginName, function(e) {
          e.stopPropagation();
          dropdown.trigger('click');
        });
      }

      if (opt.closeByDocument) {
        win.on('click.' + pluginName, function(e) {
          if (!Site.isMobile()) {
            return;
          }
          if (!$(e.target).is(target) && el.data('isClick')) {
            dropdown.trigger('click');
          }
        });
      }

      win.on('resize.' + pluginName, function() {
        if (opt.target === '.navbar-collapse' && !that.element.hasClass(opt.activeClass)) {
          if (Site.isMobile()) {
            target.css('display', 'none');
            el.removeClass(opt.activeClass);
            el.data('isClick', false);
          } else {
            target.css('display', 'table-cell');
          }
        }
      });
    },
    destroy: function() {
      $.removeData(this.element[0], pluginName);
    }
  };

  $.fn[pluginName] = function(options, params) {
    return this.each(function() {
      var instance = $.data(this, pluginName);
      if (!instance) {
        $.data(this, pluginName, new Plugin(this, options));
      } else if (instance[options]) {
        instance[options](params);
      } else {
        window.console && console.log(options ? options + ' method is not exists in ' + pluginName : pluginName + ' plugin has been initialized');
      }
    });
  };

  $.fn[pluginName].defaults = {
    target: '',
    duration: 200,
    activeClass: 'open',
    closeByTarget: true,
    closeByDocument: true,
  };

  $(function() {
    $('[data-' + pluginName + ']')[pluginName]({});

    /*
    *   action when change user type (KOL or Public)
    */
    var socialSelect = $('#social-page');
    if(socialSelect.length) {
      var filterUserEle = $('#filter-user'),
          ulList = filterUserEle.find('.scopes'),
          duration = 500;
      filterUserEle
        .children('.link-view')
        .children('span')
        .text(ulList.find('.selected').children('span').text());
      ulList
        .off('click', 'a')
        .on('click', 'a', function() {
          var self = $(this),
              prevVal = self.parent('li').siblings().find('.selected').children('span').data('val'),
              thisVal = self.children('span').data('val'),
              loading = $('#loading');

          if (prevVal && thisVal && prevVal !== thisVal) {
            loading.fadeIn(duration, function() {
              socialSelect['social-feed']('setUserType', thisVal);
              socialSelect['social-feed']('ifyUserData');
              socialSelect['social-feed']('checkPaging');
              socialSelect['social-feed']('showSocialFeed', true);
              self.addClass('selected')
                .parent('li').siblings().children('a')
                .removeClass('selected');
              filterUserEle.children('.link-view').children('span').text(self.children('span').text());
              loading.hide();
            });
          }
        });
    }
  });

}(jQuery, window));
