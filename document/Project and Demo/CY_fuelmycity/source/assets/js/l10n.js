var L10n = {
  socialFeed: {
    altThumb: 'Social feed image',
    errorGetFeed: 'Sorry, can not get social feed!'
  },
  socialShare: {
    onFooter: {
      twitter: 'Discover cool journeys in and around {{nameCity}} on {{link}}'
    },
    onKOLShare: {
      twitter: 'Check out {{nameKOL}}\'s journey around {{nameCity}} at {{link}}'
    }
  }
};
