/**
 * @name Site
 * @description Global variables and functions
 * @version 1.0
 */

var Site = (function($, window, undefined) {
  'use strict';

  var win = $(window),
    resize = ('onorientationchange' in window) ? 'orientationchange' : 'resize',
    mess = {},
    popup = {},
    eventMethod = window.addEventListener ? 'addEventListener' : 'attachEvent',
    eventer = window[eventMethod],
    messageEvent = eventMethod == 'attachEvent' ? 'onmessage' : 'message',
    timeResizeIframe;

  var globalFct = function() {
    win.on('load', function() {
      imgIsLoaded($('body img'), communication());
      getName();
    });
    customSelect();
    mCustomScrollbar();

    win.on(resize, function() {
      if(timeResizeIframe) {
        clearTimeout(timeResizeIframe);
      }
      timeResizeIframe = setTimeout(function() {
        sendMess();
      }, 1000);
    });
  };

  var mCustomScrollbar = function() {
    if($('[data-scroll]').length) {
      $('[data-scroll]').mCustomScrollbar({
        theme: '3d-thick'
      });
    }
  };

  var customSelect = function() {
    $('.custom-select select').each(function() {
      $(this).on('change', function () {
        var str = '';
        $(this).find('option:selected').each(function() {
          str += $(this).text() + ' ';
        });
        $(this).prev('span').text(str);
      })
      .change();
    });
  };

  var getName = function() {
    var regex = /[?&]([^=#]+)=([^&#]*)/g,
      url = window.location.href,
      params = {},
      match;

    while(match = regex.exec(url)) {
      params[match[1]] = match[2];
    }

    if(params.name) {
      $('[data-name]').html(params.name.replace(/%20/g, ' '));
    }
  };

  var sendUrlLink = function(isLogged, name) {
    $('.product-list').off('click.buynow', 'a').on('click.buynow', 'a', function (e) {
      var urlIframe = isLogged ? '/popup_fb_loged.html' : '/popup_fb_not_loged.html';

      e.preventDefault();
      popup.isLink = true;
      popup.url = '//' + location.host + urlIframe;
      popup.params = '';
      if(this.getAttribute('tid')) {
        popup.params = '?id=' + this.getAttribute('tid');
        if(name) {
          popup.params += '&name=' + name;
        }
      }
      else if(name) {
        popup.params += '?name=' + name;
      }
      parent.postMessage(popup, '*');
    });
  };

  var imgIsLoaded = function(elm, callback) {
    var images_loaded = 0,
      total_images = elm.length;

    elm.each(function() {
      var fakeSrc = $(this).attr('src');

      $('<img/>')
        .attr('src', fakeSrc).css('display', 'none')
        .load(function() {
          images_loaded++;
          if (images_loaded === total_images) {
            $.isFunction(callback) && callback();
          }
        })
        .error(function(){
          images_loaded++;
          if (images_loaded === total_images) {
            $.isFunction(callback) && callback();
          }
        });
    });
  };

  var communication = function() {
    eventer(messageEvent, function(e) {
      if(e.data.idIframe) {
        mess.idIframe = e.data.idIframe;
        mess.height = document.body.clientHeight;
        window.parent.postMessage(mess, '*');
      }
    }, false);
  };

  var sendMess = function() {
    mess.height = document.body.clientHeight;
    window.parent.postMessage(mess, '*');
  };

  return {
    mess: mess,
    popup: popup,
    sendMess: sendMess,
    globalFct: globalFct,
    sendUrlLink: sendUrlLink
  };

})(jQuery, window);

jQuery(function() {
  Site.globalFct();
});

// SLIDER
;(function($, window, undefined) {
  'use strict';

  var pluginName = 'slider',
    timeResize,
    win = $(window),
    resize = ('onorientationchange' in window) ? 'orientationchange.resize' + pluginName : 'resize.resize' + pluginName,
    TypeSliders = {
      SINGLE: 'single',
      CAROUSEL: 'carousel'
    };

  function Plugin(element, options) {
    this.element = $(element);
    this.options = $.extend({}, $.fn[pluginName].defaults, this.element.data(), options);
    this.init();
  }

  Plugin.prototype = {
    init: function() {
      if(this.options[pluginName].initUnder) {
        if(this.options[pluginName].initUnder > win.width()) {
          if(this.element.hasClass('slick-initialized')) {
            this.setPositionArrows();
          }
          else {
            this.initialize();
          }
        }
        else if(this.element.hasClass('slick-initialized')) {
          this.element.slick('unslick');
        }
      }
      else {
        if(this.element.hasClass('slick-initialized')) {
          this.setPositionArrows();
        }
        else {
          this.initialize();
        }
      }
    },
    initialize: function() {
      if(!!this.options[pluginName].initUnder) {
        if(this.options[pluginName].initUnder > win.width()) {
          if(this.element.find('img').length) {
            this.checkImgLoad();
          }
          else {
            this.initSlider();
          }
        }
      }
      else {
        if(this.element.find('img').length) {
          this.checkImgLoad();
        }
        else {
          this.initSlider();
        }
      }
    },
    checkImgLoad: function() {
      var that = this,
        imagesLoaded = 0,
        totalImages = this.element.find('img').length;

      this.element.find('img').each(function() {
        var fakeSrc = $(this).attr('src');

        $('<img />')
          .attr('src', fakeSrc).css('display', 'none')
          .load(function() {
            ++imagesLoaded;
            if (imagesLoaded === totalImages) {
              $.isFunction(that.initSlider) && that.initSlider();
            }
          })
          .error(function() {
            ++imagesLoaded;
            if (imagesLoaded === totalImages) {
              $.isFunction(that.initSlider) && that.initSlider();
            }
          });
      });
    },
    initSlider: function() {
      var that = this,
        option,
        navFor = {};

      switch(this.options[pluginName].type) {
        case TypeSliders.SINGLE:
          this.element.slick(this.options.singleSlider);
          break;
        case TypeSliders.CAROUSEL:
          this.element.slick(this.options.carousel);
          break;
        default:
      }

      this.setPositionArrows();
      this.element.on('afterChange.' + pluginName, function() {
        that.setPositionArrows();
      });
    },
    setPositionArrows: function() {
      var arrowControl = this.element.find('.slick-arrow'),
        imgVisible = this.element.find('[aria-hidden="false"] .img-view'),
        maxHeight = 0,
        posTop = 0;

      $(imgVisible).each(function() {
        maxHeight = Math.max($(this).height(), maxHeight);
      });

      posTop = (maxHeight / 2) - (arrowControl.outerHeight(true) / 2);
      arrowControl.animate({'top': posTop}, 300);
      setTimeout(function() {
        Site.mess.height = document.body.clientHeight;
        window.parent.postMessage(Site.mess, '*');
      }, 300);
    },
    destroy: function() {
      this.element
        .slick('unslick')
        .off('afterChange.' + pluginName);
      win.off(resize);
      $.removeData(this.element[0], pluginName);
    }
  };

  $.fn[pluginName] = function(options, params) {
    return this.each(function() {
      var instance = $.data(this, pluginName);
      if (!instance) {
        $.data(this, pluginName, new Plugin(this, options));
      } else if (instance[options]) {
        instance[options](params);
      }
    });
  };

  $.fn[pluginName].defaults = {
    singleSlider: {
      arrows: true,
      infinite: true,
      speed: 300,
      slidesToShow: 1,
      rtl: $('html').attr('dir') === 'rtl' ? true : false
    },
    carousel: {
      arrows: true,
      infinite: true,
      speed: 300,
      slidesToShow: 6,
      slidesToScroll: 6,
      autoplay: false,
      touchMove: true,
      autoplaySpeed: 3000,
      rtl: $('html').attr('dir') === 'rtl' ? true : false,
      responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 4
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3
          }
        },
        {
          breakpoint: 320,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    }
  };

  $(function() {
    $('[data-' + pluginName + ']')[pluginName]();
    win.off(resize).on(resize, function() {
      if(timeResize) {
        clearTimeout(timeResize);
      }
      timeResize = setTimeout(function() {
        $('[data-' + pluginName + ']')[pluginName]('init');
      }, 600);
    });
  });

}(jQuery, window));
