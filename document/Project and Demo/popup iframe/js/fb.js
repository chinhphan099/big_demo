function statusChangeCallback(response) {
  if (response.status === 'connected') {
    FB.api('/me', function(info) {
      Site.sendUrlLink(true, info.name);
    });
  }
  else if (response.status === 'not_authorized') {
    Site.sendUrlLink(false);
  }
  else {
    Site.sendUrlLink(false);
  }
}

function fb_login() {
  Site.popup.changeLink = true;
  Site.popup.url = '//' + location.host + '/popup_fb_loged.html';
  Site.popup.params = window.location.search;
  FB.login(function(response) {
    if (response.authResponse) {
      FB.api('/me', function(info) {
        if(Site.popup.params) {
          Site.popup.params += '&name=' + info.name;
        }
        else {
          Site.popup.params += '?name=' + info.name;
        }
        parent.postMessage({loged: true}, '*');
        parent.postMessage(Site.popup, '*');
      });
    }
  },
  {
    scope: 'publish_stream,email'
  });
}

function checkLoginState() {
  FB.getLoginStatus(function(response) {
    statusChangeCallback(response);
  });
}

window.fbAsyncInit = function() {
  FB.init({
    appId      : '1654953481460608',
    cookie     : true,
    xfbml      : true,
    version    : 'v2.5'
  });

  checkLoginState();
};

(function(d, s, id){
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) {return;}
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

