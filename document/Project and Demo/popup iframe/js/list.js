var resize = ('onorientationchange' in window) ? 'orientationchange' : 'resize',
  eventMethod = window.addEventListener ? 'addEventListener' : 'attachEvent',
  eventer = window[eventMethod],
  messageEvent = eventMethod == 'attachEvent' ? 'onmessage' : 'message',
  showCls = 'show-iframe',
  wrap = document.getElementsByClassName('iframe-wrapper'),
  overlayId = 'overlay-iframe',
  popupIframe = 'popup-iframe',
  closePopupId = 'close-popup',
  overlayPopupId = 'overlay-iframe',
  cssLink = '/css/style-main.css',
  baseUrlIframe = 'sliderAllScreen.html';

var funct = {
  initialize: function() {
    for(var i = 0, n = wrap.length; i < n; i++) {
      var iframes = document.createElement('iframe');
        iframes.src = baseUrlIframe;
        iframes.setAttribute('style', 'overflow: hidden');
        iframes.setAttribute('allowtransparency', 'true');
        iframes.setAttribute('frameborder', '');
        iframes.setAttribute('scrolling', 'no');
        iframes.setAttribute('id', 'iframe-' + i);

      wrap[i].appendChild(iframes);
      this.loadCss();
      this.checkLoad(iframes, i);
    }
  },
  loadCss: function() {
    var cssUrl = document.createElement('link')
      cssUrl.setAttribute('rel', 'stylesheet')
      cssUrl.setAttribute('type', 'text/css')
      cssUrl.setAttribute('href', cssLink)
      document.getElementsByTagName('head')[0].appendChild(cssUrl);
  },
  checkLoad: function(iframe, i) {
    var that = this;
    iframe.onload = function() {
      iframe.contentWindow.postMessage({idIframe: 'iframe-' + i}, '*');
      that.setWidth();
    };
  },
  setWidth: function() {
    for(var j = 0, m = wrap.length; j < m; j++) {
      wrap[j].classList.remove(showCls);
      wrap[j].childNodes[0].width = wrap[j].offsetWidth;
      this.setHeight();
    }
  },
  setHeight: function() {
    eventer(messageEvent, function(e) {
      if(e.data.idIframe) {
        var iframe = document.getElementById(e.data.idIframe);
        iframe.height = e.data.height;
        iframe.parentNode.classList.add(showCls);
      }
    }, false);
  },
  listenerPopup: function() {
    var that = this;
    eventer(messageEvent, function(e) {
      if(e.data.changeLink) {
        that.removePopup();
        that.createPopup(e.data.url, e.data.params);
      }
      else {
        if(e.data.isLink) {
          that.createPopup(e.data.url, e.data.params);
        }
      }
    }, false);
  },
  createPopup: function(url, params) {
    var that = this;
    var iframeWrap = document.createElement('div'),
      close = document.createElement('i'),
      iframes = document.createElement('iframe'),
      overlayPopup = document.createElement('div');

      close.setAttribute('id', closePopupId);
      iframeWrap.setAttribute('id', popupIframe);
      overlayPopup.setAttribute('id', overlayPopupId);
      iframes.src = url;
      if(params) {
        iframes.src += params;
      }
      iframes.setAttribute('style', 'overflow: hidden');
      iframes.setAttribute('allowtransparency', 'true');
      iframes.setAttribute('frameborder', '');

    document.body.appendChild(overlayPopup);
    iframeWrap.appendChild(close);
    iframeWrap.appendChild(iframes);
    document.body.appendChild(iframeWrap);
    iframes.onload = function() {
      document.body.classList.add('popup-iframe-showed');
      iframeWrap.classList.add('showed');
    };

    document.getElementById(overlayId).onclick = function() {
      funct.removePopup();
    };
    document.getElementById(closePopupId).onclick = function() {
      funct.removePopup();
    };

    //listener facebook loged
    eventer(messageEvent, function(e) {
      if(e.data.loged) {
        that.reloadAllIframe();
      }
    }, false);
  },
  removePopup: function() {
    document.getElementById(overlayId).remove();
    document.getElementById(popupIframe).remove();
    document.body.classList.remove('popup-iframe-showed');
  },
  reloadAllIframe: function() {
    var f_list = document.getElementsByTagName('iframe');
    for (var i = 0, n = f_list.length; i < n; i++) {
      f_list[i].src = f_list[i].src;
    }
  }
};

window.onload = function() {
  funct.initialize();
  funct.listenerPopup();
};
window.addEventListener(resize, function() {
  funct.setWidth();
}, false);
