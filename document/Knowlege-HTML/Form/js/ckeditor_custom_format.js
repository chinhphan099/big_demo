
// FO HTML
CKEDITOR.config.toolbar = [
    ['Format'],
    [ 'Bold','Italic','Underline'],
    ['NumberedList','BulletedList','Blockquote'],
    [ 'Link','Unlink','Anchor'],    
];