;(function($){
	$.fn.validForm = function(options){
		var defaults = {
			rules: null,
			msgClass: 'text-error',
			zIndex: 9999,
			timeCloseMsg: 3000,
			separator: '|',
			emailReg: /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/,
			linkReg: /^((https?|ftp):\/\/)?(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i,
			allowTrigger: false,
			autoFocus: true,
			autoSelect: true,
			showAt: null,
			opacity: 1,
			onValid: function(){}
		}
		options = $.extend(defaults, options);
		
		function createAll(){
			var that = $(this);
			var timeOut = null;
			that.vars = {
				timeOut: null,
				tooltip: null
			};
			init(that);
			that.unbind('submit.va').bind('submit.va', function(e){
				for(var i = 0, l = options.rules.length; i < l; i++){
					var rule = options.rules[i];
					if(!isValidated(rule, that)){
						return false;
					}
				}
				if(options.onValid && options.onValid.call(that.get(0), e) == false){
					return false;
				}
			});
			$(window).bind('resize.validform', function(){
				if(that.vars.messing){
					setTooltipPos(that.vars.curElem, that.vars.tooltip, that.vars.curPos);
				}
			});
		};
		this.resetValidate = createAll;

		return this.each(createAll);
		
		function init(that){
			that.vars.tooltip = $('.' + options.msgClass);
			if(!that.vars.tooltip.length){				
				that.vars.tooltip = $('<div class="' + options.msgClass + '">').css({
					position: 'absolute',
					zIndex: options.zIndex,
					top: -1000,
					left: -1000
				}).appendTo($(document.body));
			}
			
			for(var i = 0, l = options.rules.length; i < l; i++){
				var rule = options.rules[i];
				var elem;
				if(rule.name) elem = that.find('[name=' + rule.name + ']');
				else if(rule.id) elem = that.find('#' + rule.id);
				var elemType;
				if(elem.length){
					elemType = getInputType(elem);
					options.rules[i].elem = elem;
					if(rule.initValue && (elemType == 'text' || elemType == 'textarea')){
						elem.data('initValue', rule.initValue).val(rule.initValue);
						elem.unbind('blur.va').bind('blur.va', function(){
							if($(this).val() == ''){
								$(this).val($(this).data('initValue'));
							}
							}).unbind('focus.va').bind('focus.va', function(){
							if($(this).val() == $(this).data('initValue')){
								$(this).val('');
							}
						});
						
					}
					elem.unbind('change.va keydown.va').bind('change.va keydown.va', function(){					
						hideMessage(that);
					});
				}
			}
			
			if(options.allowTrigger){
				that.unbind('showAlert.va').bind('showAlert.va', function(e, elem, msg, pos){
					showMessage($(elem), msg || '', that, pos);
					elem.unbind('change.va keydown.va').bind('change.va keydown.va', function(){
						hideMessage(that);
					});
				}).unbind('hideAlert.va').bind('hideAlert.va', function(e){
					hideMessage(that);
				});
			}
			
			that.unbind('reset.va').bind('reset.va', function(){
				setTimeout(function(){
					init(that);
				}, 1);
			});
		}
		
		function hideMessage(that){
			if(that.vars.tooltip){
				that.vars.tooltip.css({
					left: -1000
				});
				clearTimeout(that.timeOut);
			}
			that.vars.messing = false;
		}
		
		function setTooltipPos(elem, tooltip, pos){
			tooltip.css({
				top: elem.offset().top + elem.outerHeight() + pos.top,
				left: elem.offset().left + pos.left
			});
		}
		
		function showMessage(elem, msg, that, pos){
			if (!elem.is(':visible')){
				elem = elem.siblings(':input');
			}
			options.onInvalid&&options.onInvalid();
			that.vars.messing = true;
			that.vars.curElem = elem;
			pos = $.extend({left: 0, top: 0}, pos);
			that.vars.curPos = pos;
			that.vars.tooltip.html(msg).css({
				color: '#f00',
				opacity: options.opacity,
				zIndex: options.zIndex
			});
			if(elem.offset().top < $(window).scrollTop()){
				$('html, body').animate({
					scrollTop: elem.offset().top
				}, 1000);
			}
				
			elem.focus().select();
			setTooltipPos(elem, that.vars.tooltip, pos);
			clearTimeout(that.vars.timeOut);
			that.vars.timeOut = setTimeout(function(){
				hideMessage(that);
			}, options.timeCloseMsg);
		}
		
		function isValidated(rule, that){
		try{
			var elem = rule.elem;
			if((elem && elem.length == 0)||elem == undefined){
				return true;
			}
			if(rule.valid){
				var valid = rule.valid.split(options.separator);
				var msg = rule.message?rule.message.split(options.separator):['invalid'];
				var checkValue = elem.val();

				for (var x in valid){
					switch(valid[x]){
						case 'required':
							if(elem.length>1){
								for(var i = 0; i <elem.length; i++){
									checkValue = elem.eq(i).val();
									if($.trim(checkValue) == '' || checkValue == rule.initValue || (elem[0].selectedIndex != undefined && elem.eq(i)[0].selectedIndex == 0)){
										showMessage(rule.showAt || elem.eq(i), msg[x], that, rule.pos);
										return false;
									}
								}
							}
							else if($.trim(checkValue) == '' || checkValue == rule.initValue || (elem[0].selectedIndex != undefined && elem[0].selectedIndex == 0)||checkValue==elem.attr('placeholder')){
								showMessage(rule.showAt || elem, msg[x], that, rule.pos);
								return false;
							}
							break;
						case 'email':
							if(!options.emailReg.test(checkValue)){
								showMessage(rule.showAt || elem, msg[x], that, rule.pos);
								return false;
							}
							break;
						case 'checked':
							if(!elem.filter(':checked').length){
								showMessage(rule.showAt || elem, msg[x], that, rule.pos);
								return false;
							}
							break;
						case 'link':
							if(checkValue.length == 0 || options.linkReg.test(checkValue) == false){
								showMessage(rule.showAt || elem, msg[x], that, rule.pos);
								return false;
							}
							break;
						case 'custom':
							if(rule.customCheck && rule.customCheck.call(that.get(0), checkValue, elem) == false){
								if(rule.customShow){
									rule.customShow();
								}else{
									showMessage(rule.showAt || elem.parent().parent(), msg[x], that, rule.pos);	
								} 
								return false;
							}
							break;
						case 'customAdd':
							if(elem.find('a').length == 0){
								showMessage(rule.showAt || elem, msg[x], that, rule.pos);
								return false;
							}
							break;
						case 'similar':
							if(rule.similar.val() != checkValue){
								showMessage(rule.showAt || elem, msg[x], that, rule.pos);
								return false;
							}
							break;
						case 'nPhone':
							if(checkValue.length > 0 && !/^[0-9]+$/i.test(checkValue)){
								showMessage(rule.showAt || elem, msg[x], that, rule.pos);
								return false;
							}
							break;
						case 'nEmail':
							if(checkValue.length > 0 && !options.emailReg.test(checkValue)){
								showMessage(rule.showAt || elem, msg[x], that, rule.pos);
								return false;
							}
							break;
						case 'nLink':
							if(checkValue.length > 0 && !options.linkReg.test(checkValue)){
								showMessage(rule.showAt || elem, msg[x], that, rule.pos);
								return false;
							}
							break;
					}
				}
			}
			}catch(e){console.log(e); return false;}
			return true;
		}
		
		function getInputType(elem){
			if(elem.is('input')){
				if(elem.attr('type')){
					return elem.attr('type').toLowerCase();
				}
				return 'text';
			}
			return elem.get(0).tagName.toLowerCase();
		}
	}
})(jQuery);

var Placeholder = (function($){
	return{
		init: function(){
			var ie = /msie/i.test(window.navigator.userAgent);
			var ie10 = /msie[ ]10/i.test(window.navigator.userAgent);
			if(ie&&!ie10){
				var item = $('[placeholder]');
				item.each(function(){
					var input1 = $(this);
					if(this.type == "password"){
						var attrs = this.attributes;
						var text = '<input type="text" ';
						var n,v;
						for(var i=0;i<attrs.length;i++){
							n = attrs[i].name;
							v = attrs[i].value;
							if(n!="type"&&n!="style"&&n!="value"&&n!="id")
								text += n+'="'+v+'" ';
						}
						text += 'value="'+input1.attr('placeholder')+'"></input>';
						var input2 = $(text).insertAfter(input1);
						
						input2.unbind('focus').focus(function(){
							input2.hide();
							input1.show().focus();
						})
						input1.unbind('blur').blur(function(){
							if($.trim(input1.val()) == ''){
								input1.hide();
								input2.show();
							}
						})
						input1.hide();
						if(input1.val().length>0) input2.focus();
					}else{
						input1.focus(function(){							
							if($.trim(this.value)==input1.attr('placeholder')){
								this.value = '';
							}							
						});
						input1.blur(function(){	
							if($.trim(this.value) == ''){
								this.value = input1.attr('placeholder');
							}
						});						
						if(!input1.val().length){
							input1.val(input1.attr('placeholder'));
						}
					}
				});
			}
		}
	}
})(jQuery);

;(function($, window, undefined) {
	var pluginName = 'smAutocomplete';
	var privateVar = null;
	var privateMethod = function() {

	};

	function Plugin(element, options) {
		this.element = $(element);
		this.options = $.extend({}, $.fn[pluginName].defaults, options);
		this.init();
	};

	Plugin.prototype = {
		init: function() {
			var that = this;
			var element = that.element;
			//var filterLayer = (that.options.animation) ? $('<ul class="autocomplete-filter animation '+ element.attr('id') +'"></ul>') : $('<ul class="autocomplete-filter '+ element.attr('id') +'"></ul>');
			var filterLayer = (that.options.animation) ? $('<ul class="autocomplete-filter animation '+ element.attr('id') +'"></ul>') : $('<ul class="unstyled autocomplete-1"></ul>');
			var rowSelected  = null;
			var currentIndex = -1;
			var txtSearch = null;
			var pattern = [];
			var scrolltop = 0;
			var currIndx = 0;
			var cancelBtn = $('.cancel');
			var patternText = [];			
			if(that.options.typeData == 'array' && that.options.remote == false){
				pattern = that.options.source;				
				$(pattern).each(function(index, el){
					var name = $(el).find('.media-body a').text();
					patternText.push(name);
				});		
			}
			if(that.options.typeData == 'json' && that.options.remote == false){
				var obj = jQuery.parseJSON(that.options.source);
				for(var j = 0 ; j < obj.length ; j++){
					pattern.push(obj[j].label);
				}
			}
			if(that.options.typeData == 'combobox' && that.options.remote == false){
				var optEls = element.prev().find('option').not(':first');
				for(var j = 0 ; j < optEls.length ; j++){
					pattern.push(optEls.eq(j).val());
				}
			}
			if(that.options.autoFocus){
				element.focus();
			}
			
			filterLayer.appendTo($(that.options.appendTo)).css({
				'display':'none',
				'top':0,
				'left':0,
				'z-index':1
			});
			
			$('input[type="submit"]').submit(function(e){
				e.preventDefault();
				return false;
			});

			filterLayer.delegate('li', 'click',function(){
				if(that.options.multipleValue == true){
					var aTag = $(this).find('.media-heading a');
					var spanEl = '<a title="' + aTag.text() + '" key="' + $(this).attr('key') + '" class="btn btn-default" href="#"><i class="icon-ban-circle"></i>' + aTag.text() + '</a>';
					$('#address').val($('#address').val() + ',' + $(this).attr('key'));
					$(spanEl).insertBefore(element.parent());
					that.change('');
					element.focus();
					$('.tag-input .active').removeClass('active');
					currIndx = element.closest('.tag-input').find('> span').length;
					$('.tag-input').children('a').unbind('click').bind('click',function(e){
						if(e){ 
							e.preventDefault();
						}
						$(this).remove();
						var key = $(this).attr('key');
						$('#address').val($('#address').val().replace(',' + key, ''));
						element.focus();
					});
				}
				else if(that.options.multipleValue == false){
					that.change($(this).html());
				}
				currentIndex = -1;
				if(that.options.animation){
					filterLayer.animate({'height':0},500,function(){ filterLayer.html(''); });
				}
				else{
					filterLayer.css('display','none');
				}
				return;
			});

			element.unbind('keyup.filter').bind('keyup.filter',function(e){
				if(e.keyCode != 40 && e.keyCode != 38 && e.keyCode != 13 && e.keyCode != 27){
					txtSearch = element.val();
					filterLayer.html('');
					if(that.options.remote == false){
						for(var i = 0 ; i < pattern.length ; i++){
							var patt = new RegExp(txtSearch.toUpperCase());
							if(patt.test(patternText[i].toUpperCase())){
								$('<li>' + pattern[i] + '</li>').appendTo(filterLayer);
							}
						}
						if(txtSearch != ''){
							if(that.options.animation){
								var filterHeight = filterLayer.css({'display':'block','position':'absolute'}).children().length * filterLayer.children().eq(0).outerHeight(true);
								filterLayer.css({'display':'none','height':0});
								
								if(that.options.scrollable == true){
									if(filterLayer.children().length > 5){
										filterLayer.css({
											'display':'block',
											'top':element.position().top + element.outerHeight(true),
											'left':element.position().left,
											'z-index':1,
											'overflow-y':'scroll'
										}).animate({'height':filterLayer.children().eq(0).outerHeight(true)*5},500);
									}
									else{
										filterLayer.css({
											'display':'block',
											'top':element.position().top + element.outerHeight(true),
											'left':element.position().left,
											'z-index':1,
											'overflow-y':'hidden'
										}).animate({'height':filterLayer.children().eq(0).outerHeight(true)*filterLayer.children().length},500);
									}
								}
								else{
									filterLayer.css({
										'display':'block',
										'top':element.position().top + element.outerHeight(true),
										'left':element.position().left,
										'z-index':1
									}).animate({'height':filterHeight},500);
								}
							}
							else{
								if(that.options.scrollable == true){
									if(filterLayer.children().length > 5){
										filterLayer.css({
											'display':'block',
											'position':'absolute',
											'top':element.position().top + element.outerHeight(true),
											'left':element.position().left,
											'z-index':1,										
											'overflow-y':'scroll'
										});
										filterLayer.css({										
											'height': (filterLayer.children().eq(0).outerHeight(true)*5)										
										});
									}
									else{
										filterLayer.css({
											'display':'block',
											'position':'absolute',
											'top':element.position().top + element.outerHeight(true),
											'left':element.position().left,
											'z-index':1,										
											'overflow-y':'hidden'
										});
										filterLayer.css({										
											'height': filterLayer.children().eq(0).outerHeight(true)*filterLayer.children().length										
										});
									}
									
								}
								else{
									filterLayer.css({
										'position':'absolute',
										'display':'block',
										'top':element.offset().top + element.outerHeight(true),
										'left':element.offset().left,
										'z-index':1
									});
								}
								
							}
						}
						else{
							if(that.options.animation){
								filterLayer.animate({'height':0},500,function(){ filterLayer.html(''); });
							}
							else{
								filterLayer.css('display','none');
								filterLayer.html('');
							}
						}
					}
					if(that.options.remote == true && that.options.urlAjax != null){
						$.ajax({
							url: that.options.urlAjax,
							type: 'POST',
							data: {
								'address-text': txtSearch,
								'address': $('#address').val()
							},
							success: function(resp){
								if(that.options.typeData == 'array'){
									pattern = resp;
									for(var i = 0 ; i < pattern.length ; i++){
										var patt = new RegExp(txtSearch.toUpperCase());
										if(patt.test(pattern[i].toUpperCase())){
											$('<li>' + pattern[i] + '</li>').appendTo(filterLayer);
										}
									}
								}
								else if(that.options.typeData == 'json'){
									var obj = resp.content;
									pattern = [];
									for(var m = 0 ; m < obj.length ; m++){
										pattern.push(obj[m].content);
									}
									for(var i = 0 ; i < pattern.length ; i++){
										var patt = new RegExp(txtSearch.toUpperCase());
										if(patt.test(pattern[i].toUpperCase())){
											$('<li key="' + obj[i].key + '">' + pattern[i] + '</li>').appendTo(filterLayer);
										}
									}
								}
								if(txtSearch != ''){
									if(that.options.animation){
										var filterHeight = filterLayer.css({'display':'block','position':'absolute'}).children().length * filterLayer.children().eq(0).outerHeight(true);
										filterLayer.css({'display':'none','height':0});
										filterLayer.css({
											'display':'block',
											'top':element.position().top + element.outerHeight(true),
											'left':element.position().left,
											'z-index':1
										}).animate({'height':filterHeight},500);
									}
									else{
										filterLayer.css({
											'position':'absolute',
											'display':'block',
											'top':element.offset().top + element.outerHeight(true),
											'left':element.offset().left,
											'z-index':1
										});
									}
								}
								else{
									if(that.options.animation){
										filterLayer.animate({'height':0},500,function(){ filterLayer.html(''); });
									}
									else{
										filterLayer.css('display','none');
										filterLayer.html('');
									}
								}
							}
						});
					}

					if(that.options.multipleValue == true && e.keyCode == 8){
						if(parseInt(element.css('width')) > 10){
							element.css({'width':parseInt(element.css('width')) - 10});
						}
					}
					if(that.options.multipleValue == true && e.keyCode == 37 && element.val().length == 0) {
						if(currIndx > 0){
							currIndx--;
							element.closest('.tag-input').find('.active').removeClass('active');
							element.closest('.tag-input').find('> span').eq(currIndx).addClass('active');
						}
					}
					if(that.options.multipleValue == true && e.keyCode == 39 && element.val().length == 0){
						if(currIndx >= 0 && currIndx < $('.tag-input > span').length - 1){
							currIndx++;
							element.closest('.tag-input').find('.active').removeClass('active');
							element.closest('.tag-input').find('> span').eq(currIndx).addClass('active');
						}
					}
					if(that.options.multipleValue == true && e.keyCode == 46){
						$('.tag-input .active').remove();
						currIndx = element.closest('.tag-input').find('> span').length;
					}
				}
			});
			element.unbind('keydown.filter').bind('keydown.filter',function(e){
				if(filterLayer.is(':visible')){
					if(that.options.multipleValue == true && e.keyCode != 8 && e.keyCode != 37 && e.keyCode != 38 && e.keyCode != 39 && e.keyCode != 40){
						element.css({'width':parseInt(element.css('width')) + 10});
					}
					if(e.keyCode == 40){
						if(currentIndex == -1){
							currentIndex = 0;
							filterLayer.children().eq(currentIndex).addClass(that.options.activeClass);
							if(that.options.multipleValue){
								element.css({'width':filterLayer.children().eq(currentIndex).find('.media-heading a').length * 10});
							}
							element.val(filterLayer.children().eq(currentIndex).find('.media-heading a').text());
							if(that.options.scrollable == true){
								filterLayer.scrollTop(0);
							}
						}
						else if(currentIndex >= 0 && currentIndex < filterLayer.children().length){
							rowSelected  = filterLayer.find('.'+that.options.activeClass);
							currentIndex = rowSelected.index();
							filterLayer.children().removeClass(that.options.activeClass);
							currentIndex++;
							filterLayer.children().eq(currentIndex).addClass(that.options.activeClass);
							if(that.options.multipleValue){
								element.css({'width':filterLayer.children().eq(currentIndex).find('.media-heading a').length * 10});
							}
							element.val(filterLayer.children().eq(currentIndex).find('.media-heading a').text());
							if(currentIndex == filterLayer.children().length){
								currentIndex = -1;
								filterLayer.children().removeClass(that.options.activeClass);
								if(that.options.multipleValue){
									element.css({'width':txtSearch.length * 10});
								}
								element.val(txtSearch);
							}
							if(that.options.scrollable == true){
								scrollTop = filterLayer.scrollTop() + filterLayer.children().eq(currentIndex).outerHeight(true);
								filterLayer.scrollTop(scrollTop);
							}
						}
					}
					if(e.keyCode == 38){
						if(currentIndex == -1){
							currentIndex = filterLayer.children().length - 1;
							filterLayer.children().eq(currentIndex).addClass(that.options.activeClass);
							if(that.options.multipleValue){
								element.css({'width':filterLayer.children().eq(currentIndex).html().length * 10});
							}
							element.val(filterLayer.children().eq(currentIndex).html());
							if(that.options.scrollable == true){
								filterLayer.scrollTop(filterLayer.children().length * filterLayer.outerHeight(true));
							}
						}
						else if(currentIndex > 0 &&currentIndex < filterLayer.children().length){
							rowSelected  = filterLayer.find('.'+that.options.activeClass);
							currentIndex = rowSelected.index();
							filterLayer.children().removeClass(that.options.activeClass);
							currentIndex--;
							filterLayer.children().eq(currentIndex).addClass(that.options.activeClass);
							if(that.options.multipleValue){
								element.css({'width':filterLayer.children().eq(currentIndex).html().length * 10});
							}
							element.val(filterLayer.children().eq(currentIndex).html());
							if(that.options.scrollable == true){
								scrollTop = filterLayer.scrollTop() - filterLayer.children().eq(currentIndex).outerHeight(true);
								filterLayer.scrollTop(scrollTop);
							}
						}
						else{
							currentIndex = -1;
							filterLayer.children().removeClass(that.options.activeClass);
							if(that.options.multipleValue){
								element.css({'width':txtSearch.length * 10});
							}
							element.val(txtSearch);
						}
					}
					if(e.keyCode == 13){
						if(that.options.multipleValue == false){
							if(currentIndex == -1){
								element.val(txtSearch);
							}
							else{
								that.change(filterLayer.children().eq(currentIndex).html());
							}
						}
						else if(that.options.multipleValue == true){
							element.css('width',10);
							if(currentIndex == -1){
								var spanEl = $('<a title="' + txtSearch + '" class="btn btn-default" href="#"><i class="icon-ban-circle"></i>' + txtSearch + '</a>');
							}
							else{
								var aTag = filterLayer.children().eq(currentIndex).find('.media-heading a');
								var spanEl = $('<a title="' + aTag.text() + '" class="btn btn-default" href="#"><i class="icon-ban-circle"></i>' + aTag.text() + '</a>');
							}
							spanEl.insertBefore(element.parent());
							$('#address').val($('#address').val() + ',' + filterLayer.children().eq(currentIndex).attr('key'));
							that.change('');
							element.focus();
							$('.tag-input .active').removeClass('active');
							currIndx = element.closest('.tag-input').find('> span').length;
							$('.tag-input').children('a').unbind('click').bind('click',function(e){
								if(e) e.preventDefault();
								$(this).remove();
								element.focus();
							});
						}
						currentIndex = -1;
						if(that.options.animation){
							filterLayer.animate({'height':0},500,function(){ filterLayer.html(''); });
						}
						else{
							filterLayer.css('display','none');
						}
						return false;
					}
					if(e.keyCode == 27){
						if(that.options.multipleValue == true){
							element.val('');
							element.css('width',10);
						}
						if(that.options.animation){
							filterLayer.animate({'height':0},500,function(){ filterLayer.html(''); });
						}
						else{
							filterLayer.css('display','none');
						}
					}
				}
			});
			$(document).unbind('click.layer').bind('click.layer',function(e){
				var layerFilters = $('.autocomplete-1').not($('.animation'));
				var layerAnimates = $('.animation');
				layerFilters.each(function(indx){
					$(this).css('display','none');
				});
				layerAnimates.each(function(indx){
					$(this).animate({'height':0},500,function(){ $(this).html(''); });
				});
			});
			$('.tag-input').each(function(index){
				$(this).unbind('click').bind('click',function(e){
					$(this).find('.tag-inner input').focus();
				});
			});
		},
		change:function(value){
			var that = this;
			that.element.val(value);
			$.isFunction(that.options.change) && that.options.change.call(that.element);
		}
	};

	$.fn[pluginName] = function(options, params) {
		return this.each(function() {
			var instance = $.data(this, pluginName);
			if (!instance) {
				$.data(this, pluginName, new Plugin(this, options));
			} else if (instance[options]) {
				instance[options](params);
			} else {
				console.warn(options ? options + ' method is not exists in ' + pluginName : pluginName + ' plugin has been initialized');
			}
		});
	};

	$.fn[pluginName].defaults = {
		source: null,
		urlAjax: null,
		appendTo: 'body',
		autoFocus: true,
		activeClass: 'active',
		typeData: 'array',
		remote: false,
		scrollable: false,
		multipleValue: false,
		multipleRemote: false,
		animation: false,
		change: function(){}
	};
}(jQuery, window));


