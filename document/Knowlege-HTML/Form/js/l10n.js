/**
 * @class: L10N
 * @description: Defines L10N functions
 * @version: 1.0
 **/

var L10N = {
	required: {			
		username: 'please enter your name',
		password: 'please enter your password',
		email: 'please enter your email address',
		firstname: 'please enter your first name',
		lastname: 'please enter your last name',
		address: 'address is required',
		gender: 'gender is required',
		job: 'please fill this field',
		city: 'please fill this field',
		state: 'please fill this field',
		postalcode: 'please fill this field',
		org: 'please fill this field',
		language: 'please fill this field',
		country: 'please fill this field',
		language: 'please add a language',
		date: 'please select a date',
		signature: 'please enter a signature',
		bio: 'please fill this field',
		birthday: 'please fill this field',
		gender: 'please fill this field',
		timezone: 'please fill this field',
		groupname: 'please enter your group name',
		desc: 'please enter group description',
		visibilitysettings: 'please select visibility Settings',
		joiningmodelsettings: 'please select joining Model Settings'
	},
	valid: {
		email: 'This email address is not valid',
		file: 'please choose an image',
		phone: 'please enter a phone number',
		link: 'please enter valid link',
		date: 'End Date should be greater than Start Date',
		emailUsed: 'This email address is not associated with any user account',
		loginErr: 'The email or password you entered is incorrect',
		dateBlank: 'Please enter your date',
		dateFormat: 'Please enter valid date format'
	},
	available:{
		username: 'This username is available'
	},
	unique:{
		email: 'Email that has been used. Try another email?',
		username: 'Username that has been used. Try another username?'
	},
	confirm: {
		email: 'Your email is not match',
		password: 'Your password is not match'
	},
	alert: {
		register: 'your account has been created successfully'
	},
	init: {
		job: 'Choose your position (type to search)',
		city: 'Choose your city (type to search)',
		state: 'Choose your state (type to search)'
	}
};