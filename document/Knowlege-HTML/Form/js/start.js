/**
 * Website start here
 */

Array.prototype.searchkey = function (val) {
	for (var i = 0; i < this.length; i++)
		if (this[i].toString().toLowerCase().indexOf(val.value.toLowerCase()) != -1) {
			val.value = this[i].toString();
			return this[i].toString().toLowerCase().indexOf(val.value.toLowerCase());
		}
	return -1;
};

var Gain = (function ($) {
	var ie = /msie/i.test(window.navigator.userAgent);
	var ie10 = /msie[ ]10/i.test(window.navigator.userAgent);
	var notAllow = {
		"37" : 1,
		"38" : 1,
		"39" : 1,
		"40" : 1,
		"13" : 1,
		"20" : 1,
		"16" : 1,
		"17" : 1,
		"18" : 1,
		"19" : 1,
		"33" : 1,
		"34" : 1,
		"35" : 1,
		"36" : 1,
		"44" : 1,
		"45" : 1,
		"46" : 1,
		"112" : 1,
		"113" : 1,
		"114" : 1,
		"117" : 1,
		"118" : 1,
		"119" : 1,
		"120" : 1,
		"121" : 1,
		"114" : 1,
		"145" : 1
	};
	var numberic = {
		"8" : 1,
		"9" : 1,
		"17" : 1,
		"49" : 1,
		"50" : 1,
		"51" : 1,
		"52" : 1,
		"53" : 1,
		"54" : 1,
		"55" : 1,
		"56" : 1,
		"57" : 1,
		"48" : 1,
		"46" : 1,
		"40" : 1,
		"39" : 1,
		"37" : 1,
		"38" : 1,
		"96" : 1,
		"97" : 1,
		"98" : 1,
		"99" : 1,
		"102" : 1,
		"101" : 1,
		"100" : 1,
		"103" : 1,
		"104" : 1,
		"105" : 1
	};
	function initAcodion(ele, data) {
		var lVa;
		ele.keyup(function (e) {
			if (!notAllow[e.keyCode || e.which] && lVa != this.value) {
				var t1 = $(this).siblings('.icon-refresh');
				if ($.trim(this.value) != '')
					t1.show();
				else
					t1.hide();
				if(data.join().toLowerCase().indexOf(this.value) == -1)
					t1.hide();
				lVa = this.value;
			}
		}).bind('blur.aco', function () {
			if ($.trim(this.value) != '' && data.searchkey(this) == -1)
				ele.val((ie && !ie10 && ele.attr('placeholder')) || '').css('color', '#888888');
			if ($.trim(this.value) == '')
				ele.val((ie && !ie10 && ele.attr('placeholder')) || '').css('color', '#888888');
		}).autocomplete({
			source : data,
			open : function () {
				$(this).siblings('.icon-refresh').hide();
			},
			close : function () {
				$(this).siblings('.icon-refresh').hide();
			}
		});
	};
	function initComboAdd(id, data) {
		var input = $('#' + id);
		var container = $('#container-' + id);
		var addBnt = $('#add-' + id);
		var hidden = $('#hidden_' + id);

		if (data){
			  input
		      .bind( "keydown", function(event){
		        if (event.keyCode === $.ui.keyCode.TAB &&
		            $(this).data("ui-autocomplete").menu.active){
		          	event.preventDefault();
		        }
		      })
		      .autocomplete({
		        minLength: 1,
		        source: function(request, response){
		          	response($.ui.autocomplete.filter(
		            data, extractLast(request.term)));
		        },
		        focus: function() {
		          	return false;
		        },
		        select: function( event, ui ) {
			        var terms = split( this.value );
			        terms.pop();
			        terms.push(ui.item.value);
			        terms.push("");
			        this.value = terms.join(", ");
			        return false;
		        }
		      });
		}

		container.delegate('a.btn').click(function (e) {
			e.preventDefault();
			if(e.target.tagName.toLowerCase() == 'a'){
				var targetVal = decodeURIComponent($(e.target).attr('data-content'));
				var hiddenVal = hidden.val();
				var val = hiddenVal.replace(targetVal + ',', '');
				if(val == hiddenVal) {
					val = val.replace(',' + targetVal, '');
					if(val == hiddenVal) val = val.replace(targetVal, '');
				}
				$(e.target).remove();
				hidden.val(val);
			}
		});

		input.keydown(function (e) {
			if ((e.keyCode || e.which) == 13) {
				if(data){
					e.preventDefault();
				}else{
					$(this).blur();
					addBnt.click();
				}
			}
		});

		addBnt.click(function (e) {
			e.preventDefault();
			if(input.val()==input.attr('placeholder')) return;
			var arrText = $.trim(input.val()).split(',');
			for (var i = 0, l = arrText.length; i < l; i++) {
				var text = $.trim(arrText[i]);
				if (text.length > 0 && (findAttr(container.find('a'), encodeURIComponent(text)))) {
					container.append('<a data-content="' + encodeURIComponent(text) + '" href="javascript:void(0);" title="' + text + '" class="btn width-1">' + text + '</a>');
					if (hidden.val() == '')
						hidden.val(text);
					else hidden.val(hidden.val() + ',' + text);
				}
			}
			input.val((ie && !ie10 && input.attr('placeholder')) || '');
		});

		function findAttr(list, selector) {
			for (var i = 0; i < list.length; i++) {
				if (list.eq(i).attr('data-content') == selector)
					return false;
			}
			return true;
		};
	};
	function initNumberOnly(ele) {
		ele.keydown(function (e) {
			if (!numberic[e.keyCode || e.which]) {
				e.preventDefault();
				e.stopPropagation();
				return false;
			}
		}).blur(function () {
			if($.trim(this.value).length && this.value != $(this).attr('placeholder')){
				if (!/^[0-9]+$/i.test(this.value))
					this.value = this.value.match(/[0-9]+/) || '';
			}
		});
	};
	function initFormRegister() {

		var form1 = $('#frm-register');

		/*data sample for autocomplete*/
		var data1 = ["ActionScript", "AppleScript", "Asp", "BASIC", "C", "C++", "Clojure", "COBOL", "ColdFusion", "Erlang", "Fortran", "Groovy", "Haskell", "Java", "JavaScript", "Lisp", "Perl", "PHP", "Python", "Ruby", "Scala", "Scheme"];
		var data2 = ["ActionScript", "AppleScript", "Asp", "BASIC", "C", "C++", "Clojure", "COBOL", "ColdFusion", "Erlang", "Fortran", "Groovy", "Haskell", "Java", "JavaScript", "Lisp", "Perl", "PHP", "Python", "Ruby", "Scala", "Scheme"];
		var data3 = ["ActionScript", "AppleScript", "Asp", "BASIC", "C", "C++", "Clojure", "COBOL", "ColdFusion", "Erlang", "Fortran", "Groovy", "Haskell", "Java", "JavaScript", "Lisp", "Perl", "PHP", "Python", "Ruby", "Scala", "Scheme"];

		var form2 = $('#frm-login');
		var step1 = $('.step-1:first');
		var step2 = $('.step-2:first');
		var back = $('#back');
		var file = $('#upfile');
		var label = $('#fileNameStore');
		var logMsg = $('.modal:first').removeClass('fade');
		var emailReg = form1.find('#email-2');

		var usernameReg = form1.find('#username');
		var isUniqueUsername = false;
		var checkUsernameTimeout = null;
		var xhrUsername = null;
		var ajaxUsername = null;

		var emailReg = form1.find('#email-2');
		var isUniqueEmail = false;
		var checkEmailTimeout = null;
		var xhrEmail = null;
		var ajaxEmail = null;

		var emailConfirm = form1.find('#email-3')

		var step = 1;
		var hideErrMsgTimeout = null;
		var port1 = $();
		var show = false;
		var lMOlay = $('<div id="olay-1"></div>').css({
				display : 'none',
				zIndex : 98,
				width : '100%',
				height : '100%',
				top : 0,
				left : 0,
				background : '#000000',
				opacity : 0.7,
				position : 'fixed'
			}).appendTo(document.body);

		data1 = $('#role_data').val() ? $('#role_data').val().split('|') : data1;
		data2 = $('#hidden_city').val() ? $('#hidden_city').val().split('|') : data2;
		data3 = $('#state_data').val() ? $('#state_data').val().split('|') : data3;

		$('.icon-refresh').css('display', 'none');

		initAcodion($('#job-role'), data1);
		initAcodion($('#city'), data2);
		initAcodion($('#state-region'), data3);

		back.click(function () {
			step2.hide();
			step1.hide().removeClass('hidden').fadeIn(400);
			initForm();
		});

		file.change(function () {
			label.html('<strong>File:&nbsp;</strong>' + $(this).val());
		});

		function hideErrMsg(){
			$('.text-error').css({
				left: -1000
			}).removeData('target');
		}

		function showErrMsg(elm, message, error){
			if(!error){
				$('.text-error').html(message).css({
					color: '#f00',
					top: elm.offset().top + elm.outerHeight(),
					left: elm.offset().left
				}).data('target', elm);
			}else{
				$('.text-error').html(message).css({
					color: 'green',
					top: elm.offset().top + elm.outerHeight(),
					left: elm.offset().left
				}).data('target', elm);
			}
		}

		usernameReg.unbind('keyup.valid').bind('keyup.valid', function(){
			var input = $(this);
            var val = $.trim(input.val());
			if(hideErrMsgTimeout) clearTimeout(hideErrMsgTimeout);
			hideErrMsg();
            if(checkUsernameTimeout) clearTimeout(checkUsernameTimeout);
            if(xhrUsername) {
            	xhrUsername.abort();
            	ajaxUsername = false;
            	input.siblings('.icon-refresh').hide();
            }
			if(val){
				input.siblings('.icon-refresh').show();
				ajaxUsername = true;
	            checkUsernameTimeout = setTimeout(function(){
	            	xhrUsername = $.ajax({
	                    type: 'POST',
	                    url: $('#link-ajax-username').val(),
	                    data: {
	                    	username: val
	                    },
	                    success : function(result) {
	                    	if(result == 1){
	                    		showErrMsg(usernameReg, L10N.unique.username);
	                    		hideErrMsgTimeout = setTimeout(hideErrMsg, 3000);
	                    		isUniqueUsername = false;
	                    	}
	                    	else{
	                    		showErrMsg(usernameReg, L10N.available.username, true);
	                    		hideErrMsgTimeout = setTimeout(hideErrMsg, 3000);
	                    		isUniqueUsername = true;
	                    	}
	                    	ajaxUsername = false;
	                    	input.siblings('.icon-refresh').hide();
	                	}
	            	});
	        	}, 200)
	        }
		});

		emailReg.unbind('blur.valid').bind('blur.valid', function(){
			var input = $(this);
            var val = $.trim(input.val());
			if(hideErrMsgTimeout) clearTimeout(hideErrMsgTimeout);
			hideErrMsg();
            if(checkEmailTimeout) clearTimeout(checkEmailTimeout);
            if(xhrEmail) {
            	xhrEmail.abort();
            	ajaxEmail = false;
            }
			if(val){
				if(!/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(val)){
					showErrMsg(emailReg, L10N.valid.email);
            		hideErrMsgTimeout = setTimeout(hideErrMsg, 3000);
				}
				else{
					ajaxEmail = true;
		            checkEmailTimeout = setTimeout(function(){
		            	xhrEmail = $.ajax({
		                    type: 'POST',
		                    url: $('#link-ajax-email').val(),
		                    data: {
		                    	email: val
		                    },
		                    success : function(result) {
		                    	if(result == 1){
		                    		showErrMsg(emailReg, L10N.unique.email)
		                    		hideErrMsgTimeout = setTimeout(hideErrMsg, 3000);
		                    		isUniqueEmail = false;
		                    	}
		                    	else{
		                    		isUniqueEmail = true;
		                    	}
		                    	ajaxEmail = false;
		                	}
		            	});
		        	}, 200)
				}
	        }
    	});

		emailConfirm.unbind('blur.valid').bind('blur.valid', function(){
			if(hideErrMsgTimeout) clearTimeout(hideErrMsgTimeout);
			hideErrMsg();
			var val = $.trim($(this).val());
			if(val != ''){
				if(val != $.trim(emailReg.val())){
					showErrMsg(emailConfirm, L10N.confirm.email);
	        		hideErrMsgTimeout = setTimeout(hideErrMsg, 3000);
				}
			}
		});

		$(window).bind('resize.reposition', function(){
			var tooltipErr = $('.text-error');
			var target = tooltipErr.data('target');
			if(target){
				tooltipErr.css({
					top: target.offset().top + target.outerHeight(),
					left: target.offset().left
				})
			}
		});

		function initForm() {
			form1.validForm({
				rules : [{
						id : 'firstname',
						valid : 'required',
						message : L10N.required.firstname
					}, {
						id : 'lastname',
						valid : 'required',
						message : L10N.required.lastname
					}, {
						id : 'email-2',
						valid : 'required',
						message : L10N.required.email
					}, {
						id : 'email-2',
						valid : 'email',
						message : L10N.valid.email
					}, {
						id : 'email-3',
						valid : 'similar',
						similar : form1.find('#email-2'),
						message : L10N.confirm.email
					}, {
						id : 'password-2',
						valid : 'required',
						message : L10N.required.password
					}, {
						id : 'password-3',
						valid : 'similar',
						similar : form1.find('#password-2'),
						message : L10N.confirm.password
					}, {
						id : 'username',
						valid : 'required',
						message : L10N.required.username
					}
				],
				allowTrigger : true,
				onValid : function () {
					if(!ajaxUsername && !ajaxEmail){
						if(hideErrMsgTimeout) clearTimeout(hideErrMsgTimeout);
						hideErrMsg();
						if(!isUniqueEmail){
							showErrMsg(emailReg, L10N.unique.email);
							hideErrMsgTimeout = setTimeout(hideErrMsg, 3000);
						}
						else{
							if(!isUniqueUsername){
								showErrMsg(usernameReg, L10N.unique.username);
								hideErrMsgTimeout = setTimeout(hideErrMsg, 3000);
							}
							else{
								step1.hide();
								step2.hide().removeClass('hidden').fadeIn(400);
								$('.get-image').unbind('click.chooseFile').bind('click.chooseFile', function(e){
									e.preventDefault();
									$('#upfile').trigger('click');
								});
								$('#appendedInputButton').unbind('focus.chooseFile').bind('focus.chooseFile', function(e){
									e.preventDefault();
									$('#upfile').trigger('click');
								});
								form1.validForm({
									rules : [{
											id : 'upfile',
											valid : 'custom',
											message : L10N.valid.file,
											customCheck : function (val) {
												if ($('#upfile').val().length > 0){
													return /[.]jpg$|[.]gif$|[.]png$/i.test(val);
												}
												return true;
											}
										}, {
											id : 'job-role',
											valid : 'required',
											message : L10N.required.job,
											initValue : L10N.init.job
										}, {
											id : 'city',
											valid : 'required',
											message : L10N.required.city,
											initValue : L10N.init.city
										}, {
											id : 'organization',
											valid : 'required',
											message : L10N.required.org
										}, {
											id : 'state-region',
											valid : 'required',
											message : L10N.required.state,
											initValue : L10N.init.state
										}, {
											id : 'language',
											valid : 'required',
											message : L10N.required.language
										}, {
											id : 'country',
											valid : 'required',
											message : L10N.required.country
										}
									],
									allowTrigger : true,
									onValid : function () {
										port1.remove();
										port1 = $('<iframe style="display:none;" name="postFrame1"></iframe>').appendTo(document.body);
										port1.load(function () {
											var jd = $.parseJSON(port1.contents().text());

											logMsg.find('.modal-header h4').text(jd.msg.header);
											logMsg.find('.modal-body').html(jd.msg.content);
											lMOlay.fadeIn(400);
											logMsg.css({
												opacity : 1,
												visibility : 'visible',
												top : '50%',
												marginTop :  - (logMsg.height() / 2) >> 0,
												zIndex : 99
											}).fadeIn(400);
										});
									}
								}).attr('target', 'postFrame1');
							}
						}
					}
					return false;
				}
			});
		};

		function closePopup() {
			lMOlay.fadeOut(300);
			logMsg.fadeOut(300);
		};

		initForm();

		logMsg.find('.close').click(closePopup);
		logMsg.find('.btn').click(closePopup);

		$('#fogot-btn').click(function () {
			try {
				show = !show;
				if (show) {
					$('#frm-pwd-1').validForm({
						rules : [{
								id : 'email-4',
								valid : 'email',
								message : L10N.required.email
							}
						],
						allowTrigger : true,
						onValid : function () {
							if(hideErrMsgTimeout) clearTimeout(hideErrMsgTimeout);
							hideErrMsg();
							var email = $('#frm-pwd-1').find('#email-4');
							$.ajax({
								type : 'POST',
								url : $('#link-ajax-mail').val(),
								data : {
									email : email.val()
								},
								success : function (result) {
									if(result == 0){
										showErrMsg(email, L10N.valid.emailUsed);
										hideErrMsgTimeout = setTimeout(hideErrMsg, 3000);
									}
									else{
										email.closest('.block-1').addClass('hidden').next().removeClass('hidden').find('.email-name').html(email.val());
									}
								}
							})
							return false;
						}
					});
				}
			} catch (e) {
				return false;
			}
		});

		form2.validForm({
			rules : [{
					id : 'email-1',
					valid : 'email',
					message : L10N.required.email
				}, {
					id : 'password-1',
					valid : 'required',
					message : L10N.required.password
				}
			],
			allowTrigger : true,
			onValid : function () {
				if(hideErrMsgTimeout) clearTimeout(hideErrMsgTimeout);
				hideErrMsg();
				var email = form2.find('#email-1');
				var password = form2.find('#password-1');
				$.ajax({
					type : 'POST',
					url : $('#link-ajax-login').val(),
					data : {
						email : email.val(),
						password: password.val()
					},
					success : function (result) {
						if(result == 1){
							form2.unbind('submit').submit();
						}
						else{
							showErrMsg(email, L10N.valid.loginErr);
							hideErrMsgTimeout = setTimeout(hideErrMsg, 3000);
						}
					}
				});
				return false;
			}
		});
	};
	function initFormEditProfile() {
		var form = $('#frm-edit-profile');
		var ed = $('#enddate').datepicker({
			changeMonth: true,
      		changeYear: true
		});
		var sd = $('#startdate').datepicker({
			changeMonth: true,
      		changeYear: true
		});

		var file = $('#upfile');
		var label = $('#fileNameStore');
		var imgThumb = $('#imgThumb');
		var main = $('#main-language');
		var other = $('#other-language');
		var addMain = $('#addMainLanguage');
		var addOther = $('#addOtherLanguage');
		var hiddenOther = $('#hidden_other_language');
		var container = $('#language-container');
		var userIM = $('#im-username');
		var messageType = $('#messaging');
		var addUIM = $('#add-im-username');
		var conUIM = $('#container-im-username');
		var hiddenIM = $('#hidden_instant_messages')

		/*data sample for autocomplete*/
		var dataMem = ["ActionScript", "AppleScript", "Asp", "BASIC", "C", "C++", "Clojure", "COBOL", "ColdFusion", "Erlang", "Fortran", "Groovy", "Haskell", "Java", "JavaScript", "Lisp", "Perl", "PHP", "Python", "Ruby", "Scala", "Scheme"];
		var dataInt = ["ActionScript", "AppleScript", "Asp", "BASIC", "C", "C++", "Clojure", "COBOL", "ColdFusion", "Erlang", "Fortran", "Groovy", "Haskell", "Java", "JavaScript", "Lisp", "Perl", "PHP", "Python", "Ruby", "Scala", "Scheme"];

		var dataJob = ["ActionScript", "AppleScript", "Asp", "BASIC", "C", "C++", "Clojure", "COBOL", "ColdFusion", "Erlang", "Fortran", "Groovy", "Haskell", "Java", "JavaScript", "Lisp", "Perl", "PHP", "Python", "Ruby", "Scala", "Scheme"];
		var dataCity = ["ActionScript", "AppleScript", "Asp", "BASIC", "C", "C++", "Clojure", "COBOL", "ColdFusion", "Erlang", "Fortran", "Groovy", "Haskell", "Java", "JavaScript", "Lisp", "Perl", "PHP", "Python", "Ruby", "Scala", "Scheme"];
		var dataState = ["ActionScript", "AppleScript", "Asp", "BASIC", "C", "C++", "Clojure", "COBOL", "ColdFusion", "Erlang", "Fortran", "Groovy", "Haskell", "Java", "JavaScript", "Lisp", "Perl", "PHP", "Python", "Ruby", "Scala", "Scheme"];

		$('.icon-refresh').css('display', 'none');

		$('#birthday').datepicker({
			changeMonth: true,
      		changeYear: true,
      		yearRange: "1940:2012"
		});

		ed.siblings('a.btn').click(function (e) {
			ed.focus();
			e.preventDefault();
		});
		sd.siblings('a.btn').click(function (e) {
			sd.focus();
			e.preventDefault();
		});
		$('#save-btn').click(function () {
			form.submit();
		});

		$('.get-image').unbind('click.chooseFile').bind('click.chooseFile', function(e){
			e.preventDefault();
			$('#upfile').trigger('click');
		});
		$('#upload').unbind('focus.chooseFile').bind('focus.chooseFile', function(e){
			e.preventDefault();
			$('#upfile').trigger('click');
		});

		file.change(function () {
			label.html('<strong>File:&nbsp;</strong>' + $(this).val());
		});

		container.delegate('a.btn').click(function (e) {
			if(e.target.tagName.toLowerCase() == 'a'){
				e.preventDefault();
				var targetVal = $(e.target).attr('data-content');
				var hiddenVal = hiddenOther.val();
				var val = hiddenVal.replace(targetVal + '|', '');
				if(val == hiddenVal) {
					val = val.replace('|' + targetVal, '');
					if(val == hiddenVal) val = val.replace(targetVal, '');
				}
				hiddenOther.val(val);
				$(e.target).remove();
			}
		});

		addMain.click(function (e) {
			e.preventDefault();
			var val = main.val();
			var text = main.find('option').eq(main[0].selectedIndex).text();
			if (val != 0 && container.find('[data-content=' + text + ']').length == 0) {
				container.append('<a data-content="' + text + '" href="#" title="' + text + '" class="btn width-1">' + text + '</a>');
			}
		});

		addOther.click(function (e) {
			e.preventDefault();
			var val = other.val();
			var text = other.find('option').eq(other[0].selectedIndex).text();
			if (val != 0 && container.find('[data-content=' + val + ']').length == 0) {
				container.append('<a data-content="' + val + '" href="#" title="' + text + '" class="btn width-1">' + text + '</a>');
				if (hiddenOther.val() == '')
					hiddenOther.val(val);
				else hiddenOther.val(hiddenOther.val() + '|' + val);
			}
		});


		dataMem = $('#hidden_all_members_tags').val() ? $('#hidden_all_members_tags').val().split(',') : dataMem;
		dataInt = $('#city_data').val() ? $('#int_data').val().split('|') : dataInt;

		dataJob = $('#hidden_roles').val() ? $('#hidden_roles').val().split('|') : dataJob;
		dataCity = $('#hidden_city').val() ? $('#hidden_city').val().split('|') : dataCity;
		dataState = $('#hidden_state').val() ? $('#hidden_state').val().split('|') : dataState;

		initComboAdd('membertags', dataMem);
		initComboAdd('interests', dataInt);

		initAcodion($('#title'), dataJob);
		initAcodion($('#edit-city'), dataCity);
		initAcodion($('#stateregion'), dataState);

		initNumberOnly($('#officephone'));
		initNumberOnly($('#mobilephone'));
		initNumberOnly($('#homephone'));
		initNumberOnly($('#postalcode'));

		addUIM.click(function () {
			var username = $.trim(userIM.val());
			var val = messageType.val();
			var text = messageType.find('option').eq(messageType[0].selectedIndex).text();

			if (username.length == 0) {
				userIM.focus();
				return;
			}

			if (val && username.length > 0 && (findAttr(conUIM.find('li'), encodeURIComponent(val + ':' + username)))) {
				conUIM.append('<li data-content="' + encodeURIComponent(val + ':' + username) + '"><a class="btn width-1" href="javascript:void(0);">' + text + '</a><span><strong>' + username + '</strong></span></li>');
				if(hiddenIM.val() == ''){
					hiddenIM.val(val + ':' + username);
				}
				else hiddenIM.val(hiddenIM.val() + '|' + val + ':' + username);

			}

			function findAttr(list, selector) {
				for (var i = 0; i < list.length; i++) {
					if (list.eq(i).attr('data-content') == selector)
						return false;
				}
				return true;
			};
		});

		conUIM.delegate('a.btn').click(function (e) {
			e.preventDefault();
			if(e.target.tagName.toLowerCase() == 'a'){
				var targetVal = decodeURIComponent($(e.target).closest('li').attr('data-content'));
				var hiddenVal = hiddenIM.val();
				var val = hiddenVal.replace(targetVal + '|', '');
				if(val == hiddenVal) {
					val = val.replace('|' + targetVal, '');
					if(val == hiddenVal) val = val.replace(targetVal, '');
				}
				hiddenIM.val(val);
				$(e.target).closest('li').remove();
			}
		});

		$('.hide-btn').click(function () {
			$(this).closest('.border-block').slideUp(300);
		});
		$('.hide-btn').click();

		$('.show-settings').click(function(){
            $('.settings-block').hide();
            var bid=$(this).attr('data-content');
            $('#'+bid).slideDown(200);
		})

		function validCheckboxGroup(blockId){
			var block = $(blockId);
			var checkboxGroup = block.find('.row:last');
			function disableCheckbox(){
				checkboxGroup.find('.checkbox').css('opacity', '0.5');
				checkboxGroup.find(':checkbox').attr({
					 disabled: 'disabled'
				}).removeAttr('checked');
			}
			function enableCheckbox(){
				checkboxGroup.find('.checkbox').css('opacity', '1');
				checkboxGroup.find(':checkbox').removeAttr('disabled')
			}

			block.find(':radio').bind('change.check', function(e){
             	e.stopPropagation();
				if(block.find(':radio:checked').attr('id').indexOf('limited') != -1){
					enableCheckbox();
				}
				else{
					disableCheckbox();
				}
			}).trigger('change.check');
		}

		for(var i=1; i<=12; i++)	validCheckboxGroup('#settings-' + i);

		form.validForm({
			rules : [{
					id : 'firstname',
					valid : 'required',
					message : L10N.required.firstname
				}, {
					id : 'lastname',
					valid : 'required',
					message : L10N.required.lastname
				}, {
					id : 'title',
					valid : 'required',
					message : L10N.required.job
				}, {
					id : 'language-container',
					showAt : $('#main-language'),
					valid : 'customAdd',
					message : L10N.required.language
				}, {
					id : 'upfile',
					valid : 'custom',
					message : L10N.valid.file,
					customCheck : function (val) {
						if ($('#upfile').val().length > 0){
							return /[.]jpg$|[.]gif$|[.]png$/i.test(val);
						}
						return true;
					}
				}, {
					id : 'enddate',
					valid : 'custom',
					message : L10N.valid.dateBlank,
					customCheck: function(val){
						if ($('#edit-field-featured-und').is(':checked')){
							var end = $('#enddate');
							if (end.val() == ''){
								return false;
							}
						}
						return true;
					}
				}, {
					id : 'enddate',
					valid : 'custom',
					message : L10N.valid.date,
					customCheck : function (val) {
						if ($('#edit-field-featured-und').is(':checked')){
							var start = $('#startdate').datepicker('getDate');
							var end = $('#enddate').datepicker('getDate');

							if (end && start){
								return end.getTime() > start.getTime();
							}
							if (start && !end) {
								return false;
							}
						}
						return true;
					}
				}, {
					id : 'enddate',
					valid : 'custom',
					message : L10N.valid.dateFormat,
					customCheck : function (val) {
						if ($('#edit-field-featured-und').is(':checked')){
							var end = $('#enddate');
							if (end.val() != ''){
								if (!validDateFormat(end.val())){
									return false;
								}
							}
						}
						return true;
					}
				}, {
					id : 'startdate',
					valid : 'custom',
					message : L10N.valid.dateBlank,
					customCheck: function(val){
						if ($('#edit-field-featured-und').is(':checked')){
							var start = $('#startdate');
							if (start.val() == ''){
								return false;
							}
						}
						return true;
					}
				}, {
					id : 'startdate',
					valid : 'custom',
					message : L10N.valid.date,
					customCheck : function (val) {
						if ($('#edit-field-featured-und').is(':checked')){
							var start = $('#startdate').datepicker('getDate');
							var end = $('#enddate').datepicker('getDate');

							if (end && start){
								return end.getTime() > start.getTime();
							}
							if (!start && end) {
								return false;
							}
						}
						return true;
					}
				}, {
					id : 'startdate',
					valid : 'custom',
					message : L10N.valid.dateFormat,
					customCheck: function(val){
						if ($('#edit-field-featured-und').is(':checked')){
							var start = $('#startdate');
							if (start.val() != ''){
								if (!validDateFormat(start.val())){
									return false;
								}
							}
						}
						return true;
					}
				}, {
					id : 'officephone',
					valid : 'nPhone',
					message : L10N.valid.phone
				}, {
					id : 'mobilephone',
					valid : 'nPhone',
					message : L10N.valid.phone
				}, {
					id : 'homephone',
					valid : 'nPhone',
					message : L10N.valid.phone
				}, {
					id : 'officeemail',
					valid : 'required',
					message : L10N.required.email
				}, {
					id : 'officeemail',
					valid : 'nEmail',
					message : L10N.valid.email
				},  {
					id : 'homeemail',
					valid : 'nEmail',
					message : L10N.valid.email
				}, {
					id : 'otheremail',
					valid : 'nEmail',
					message : L10N.valid.email
				}, {
					id : 'siteurl-1',
					valid : 'nLink',
					message : L10N.valid.link
				}, {
					id : 'siteurl-2',
					valid : 'nLink',
					message : L10N.valid.link
				}, {
					id : 'siteurl-3',
					valid : 'nLink',
					message : L10N.valid.link
				}, {
					id : 'siteurl-4',
					valid : 'nLink',
					message : L10N.valid.link
				}, {
					id : 'city-2',
					valid : 'required',
					message : L10N.required.city
				}, {
					id : 'stateregion',
					valid : 'required',
					message : L10N.required.state
				}, {
					id : 'country-2',
					valid : 'required',
					message : L10N.required.country
				}, {
					id : 'postalcode',
				//	valid : 'required',  // not require to validate - Son
					message : L10N.required.postalcode
				}
			],
			allowTrigger : true,
			onValid : function () {}
		});
	};
	function initFormEditAccount(){
		var form = $('#frm-edit-account');
		if(!form)
			return;
		form.validForm({
			rules : [
				{
					id : 'email',
					valid : 'required',
					message : L10N.required.email
				}, {
					id : 'email',
					valid : 'email',
					message : L10N.valid.email
				}, {
					id : 'password-1',
					showAt: $('#password-1'),
					valid : 'required',
					message : L10N.required.password
				}, {
					id : 'password-2',
					showAt: $('#password-2'),
					valid : 'similar',
					similar : form.find('#password-1'),
					message : L10N.confirm.password
				}
			],
			allowTrigger : true,
			onValid : function () {}
		});
	};
	function initFormChangePassword(){
		var form = $('#guser-form-change-password');
		if(!form)
			return;
		form.validForm({
			rules : [
				{
					id : 'edit-pass-pass1--2',
					valid : 'required',
					message : L10N.required.password
				}, {
					id : 'edit-pass-pass2--2',
					valid : 'similar',
					similar : form.find('#edit-pass-pass1--2'),
					message : L10N.confirm.password
				}
			],
			allowTrigger : true,
			onValid : function () {}
		});
	};
	function validDateFormat (dateString) {
		var test = dateString.match(/^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d+$/);
		if (test == null){
			return false;
		}
		return true;
	};
 	function split(val) {
		return val.split( /,\s*/ );
	};
	function extractLast(term) {
      return split( term ).pop();
    };
	function initFormCreateGroup(){
		var addGrpForm = $('#frm-create-group');
		if (!addGrpForm){
			return;
		}

		var groupTags = $('#grouptags-text');
		groupTags
				.bind( "keydown", function( event ) {
		        if ( event.keyCode === $.ui.keyCode.TAB &&
		            $( this ).data( "ui-autocomplete" ).menu.active ) {
		          	event.preventDefault();
		        }
		      })
		      .autocomplete({
		        source: function( request, response ) {
		          var url = $('#grouptags-autocomplete').val() + '/' + request.term;
		          $.getJSON( url, {}, response );
		        },
		        search: function() {
		          var term = extractLast( this.value );
		          if ( term.length < 2 ) {
		            return false;
		          }
		        },
		        focus: function() {
		          return false;
		        },
		        select: function( event, ui ) {
		          var terms = split( this.value );
		          terms.pop();
		          terms.push( ui.item.value );
		          terms.push( "" );
		          this.value = terms.join( ", " );
		          return false;
		        }
		      });

		var input = $('#grouptags-text');
		var addBnt = $('#add-membertags');
		var hidden = $('#grouptags');
		var container = $('#container-grouptags');
		var file = $('#uplogo');

		container.delegate('a.btn').click(function (e) {
			e.preventDefault();
			if(e.target.tagName.toLowerCase() == 'a'){
				var targetVal = decodeURIComponent($(e.target).attr('data-content'));
				var hiddenVal = hidden.val();
				var val = hiddenVal.replace(targetVal + ',', '');
				if(val == hiddenVal) {
					val = val.replace(',' + targetVal, '');
					if(val == hiddenVal) val = val.replace(targetVal, '');
				}
				$(e.target).remove();
				hidden.val(val);
			}
		});

		input.keydown(function (e) {
			if ((e.keyCode || e.which) == 13) {
				e.preventDefault();
			}
		});

		addBnt.click(function (e) {
			e.preventDefault();
			if(input.val()==input.attr('placeholder')) return;
			var arrText = $.trim(input.val()).split(',');
			for (var i = 0, l = arrText.length; i < l; i++) {
				var text = $.trim(arrText[i]);
				if (text.length > 0 && (findAttr(container.find('a'), encodeURIComponent(text)))) {
					container.append('<a data-content="' + encodeURIComponent(text) + '" href="javascript:void(0);" title="' + text + '" class="btn width-1">' + text + '</a>');
					if (hidden.val() == ''){
						hidden.val(text);
					}
					else{
						hidden.val(hidden.val() + ',' + text);
					}
				}
			}
			input.val((ie && !ie10 && input.attr('placeholder')) || '');
		});

		function findAttr(list, selector) {
			for (var i = 0; i < list.length; i++) {
				if (list.eq(i).attr('data-content') == selector)
					return false;
			}
			return true;
		};


		function log( message ) {
			$( "<div>" ).text( message ).prependTo( "#log" );
			$( "#log" ).scrollTop( 0 );
		}
		$('#organization-text').autocomplete({
			source:  function( request, response ) {
	          var url = $('#organization-autocomplete').val() + '/' + request.term;
	          $.getJSON( url, {}, function(data,status,xhr){
	        	  response( $.map( data, function( label, value ) {
	                return {
	                  label: $(label).text(),
	                  value: value.replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1")
	                }
	              }));
	          });
	        },
		    minLength: 2,
	        focus: function() {
	          return false;
	        },
	        select: function( event, ui ) {
	        	$(this).val(ui.item.label);
	        	$('#' + $(this).attr('id').replace('-text', '')).val(ui.item.value.replace(/\\/g, ''));
	        	return false;
	        }
	    });

		if ($('#edit-field-featured').length > 0){
			if (!$('#edit-field-featured').is(':checked')){
				//$('#startdate').datepicker('disable');
				//$('#enddate').datepicker('disable');
			}
		}

		$('#edit-field-featured').click(function() {
			var thisCheck = $(this);
			if (thisCheck.is(':checked')){
				$('#featuring-datetime-show-todate').attr('checked','checked');
			} else{
				//$('#startdate').datepicker('disable');
				//$('#enddate').datepicker('disable');
				$('#featuring-datetime-show-todate').attr('checked','');
			}
		});

		$('#btn-uplogo').unbind('click.chooseFile').bind('click.chooseFile', function(e){
			e.preventDefault();
			$('#uplogo').trigger('click');
		});
		file.change(function () {
			$('#grouplogo').val($(this).val());
		});

		addGrpForm.validForm({
			rules: [{
					id: 'groupname',
					valid: 'required',
					message : L10N.required.groupname
				}, {
					id : 'startdate',
					valid : 'custom',
					message : L10N.valid.date,
					customCheck : function (val) {
						if ($('#edit-field-featured').is(':checked')){
							var start = $('#startdate').datepicker('getDate');
							var end = $('#enddate').datepicker('getDate');

							if (end && start)
								return end.getTime() > start.getTime();
							if (!start && end) {
								return false;
							}
						}
						return true;
					}
				}, {
					id : 'uplogo',
					valid : 'custom',
					showAt: $('#grouplogo'),
					message : L10N.valid.file,
					customCheck : function (val) {
						if ($('#uplogo').val().length > 0){
							return /[.]jpg$|[.]gif$|[.]png$/i.test(val);
						}
						return true;
					}
				}, {
					name: '"field_visibility_settings[und]"',
					valid: 'required',
					message: L10N.required.visibilitysettings
				}, {
					name: '"field_joining_model_settings[und]"',
					valid: 'required',
					message: L10N.required.joiningmodelsettings
				}],
			allowTrigger: true,
			onValid: function() {}
		});
	}
	function accodionPlus() {
		$('.show-1').click(function () {
			$(this).hide();
		});
		$('.hide-1').click(function () {
			$(this).closest('.accordion-group').find('.show-1').stop().fadeIn();
		});
		$('.select-email').find('input').click(function () {
			$('#email').val($('#mail_' + this.value).val());
		});
	};
	function ajaxLinkProcess() {
		$(document.body).delegate('.flag-link-toggle', 'click', function (e) {
			e.preventDefault();
			if (this.clicked)
				return false;
			this.clicked = true;
			var ele = this;
			$.ajax({
				url : this.href,
				beforeSend: showLoading,
				success : function (res) {
					ele.clicked = false;
					var dataId = $(ele).closest('[data-id]').attr('data-id');

					var status = 0;
					if ($(ele).hasClass('btn-follow')){
						status = 1;
					}
					changePeopleOption(dataId, res, status);
					closeLoading();
				}
			});
		});

		$(document.body).delegate('.group-link-toggle', 'click', function (e) {
			e.preventDefault();
			if (this.clicked)
				return false;
			this.clicked = true;
			var ele = this;
			$.ajax({
				url : this.href,
				success : function (res) {
					if (res.status) {
						ele.href = res.link;
						$(ele).text(res.text);
					};
					ele.clicked = false;
				}
			});
		});

		$(document.body).delegate('.btn-join-group', 'click', function(e) {
			e.preventDefault();
			ele = $(this);
			if(ele.attr('href').indexOf('/leave') > 0) {
				if (ele.data('confirm-text')){
					var conf = confirm(ele.data('confirm-text'));
					if (!conf) {
						return;
					};
				}
			}
			$.getJSON(ele.attr('href'), {}, function(data, status, xhr) {
				if (data.status) {
					var dataId = ele.closest('[data-id]').attr('data-id');
					changePeopleOption(dataId, data, 2);
				} else {
					alert(data.msg);
				}
			});
		});

		$('.btn-block-group').unbind('click').bind('click', function(e) {
			e.preventDefault();
			ele = $(this);
			if (ele.data('confirm-text')){
				var conf = confirm(ele.data('confirm-text'));
				if (!conf) {
					return;
				};
			}
			$.getJSON(ele.attr('href'), {}, function(data, status, xhr) {
				if (data.status) {
					ele.remove();
					window.location.reload();
				}
			});
		});
	};
	function changePeopleOption(dataId, res, status){
		$('[data-id="'+ dataId +'"]').each(function() {
			if (status == 0){
				if (res.score >= 0) {
					if($(this).find('.media-heading').length > 0){
						$(this).find('.media-heading').children().last().text('+' + res.score);
					} else if ($(this).find('span.points').length > 0){
						$(this).find('span.points').text('+' + res.score);
					} else {
						$(this).find('span.number').text(res.score);
						$(this).find('span.nb-of-likes').text(res.score);
					}
				}
				if (res.status && res["html_link"]) {
					var aTagLike = $(this).find('a.btn-like');
					var aTagChanged = $(res["html_link"]);

					aTagLike.attr('title', aTagChanged.attr('title'));
					aTagLike.attr('href', aTagChanged.attr('href'));
					aTagLike.text(aTagChanged.text());
				}
			} else if (status == 1){
				var aTagChanged = $(res["html_link"]);
				var aTagFollow = $(this).find('a.btn-follow');

				aTagFollow.attr('title', aTagChanged.attr('title'));
				aTagFollow.attr('href', aTagChanged.attr('href'));
				aTagFollow.text(aTagChanged.text());

				if ($(this).find('.connection').length > 0){
					var aConnectTag = $(this).find('.connection').children().first().children();
					aConnectTag.text(res.connection);
				} else{
					$(this).find('span.nb-of-connections').text(res.connection);
				}

				if (res.follower >= 0){
					$(this).find('span.nb-of-followers').text(res.follower);
				}
			} else{
				var aTag = $(this).find('a.btn-join-group');
				aTag.attr('href', res.link);
				aTag.text(res.text);

				if (res.members >= 0){
					$(this).find('span.nb-of-members').text(res.members);
				}
			}
		});
	};
	function loadPeopleInfo(){
		$('.popup').each(function(e){
			var elm = $(this);
			elm.popover({
				content: function(){
					elm.attr("data-content", "<div class='ajax-loading'><img src='/sites/all/themes/gain/images/loading.gif'></img></div>");
					 $.ajax({
					 	type: 'POST',
		            	url: elm.attr('data-url'),
		            	success: function (res) {
		            		var result = res.content;
		            		elm.attr("data-content", result);
		            		elm.data('popover').tip().find('.popover-content').empty().append(result);
		            	}
					 });
				},
    			stayWhenHover: true,
				delay: { show: 100, hide: 200 }
    		});
		});
	};
	function initVideoPlayer() {
		$('video').each(function(){
			var that = $(this);
			that.mediaelementplayer({
				features: ['playpause','progress', 'fullscreen'],
				flashName: that.data('swf') + 'swf/flashmediaelement.swf'
			});
		});
	};
	function showLoading(){
		var overlay = null;
		var loadingCls = null;
		if ($('div[class="overlay"]').length == 0){
			overlay = $('<div>').addClass('overlay').css('zIndex', 1001).appendTo('body').fadeOut(0);
			loadingCls = '<div class="icon-loading"><span>&nbsp;</span></div>';
			//reposition($(loadingCls));
			$(loadingCls).css({
				height: $(window).height(),
				width: $(window).width()
			})
			overlay.append(loadingCls);
		}else{
			overlay = $('div[class="overlay"]');
		}
		overlay.fadeIn(400, 'linear');
	};
	function closeLoading(){
		$('div[class="overlay"]').fadeOut(0);
	};
	function initSendMsgForm(){
		var item1 = '<div class="media">' +
						'<a class="pull-left" href="javascript:void(0);" title="Erik Adigard">' +
							'<img class="media-object" src="../images/upload/avatar-9.jpg" alt="Avatar">' +
						'</a>' +
						'<div class="media-body">' +
								'<h4 class="media-heading"><a href="javascript:void(0);" title="Aardvark O’Leary" class="color-5">Aardvark O’Leary</a></h4>' +
								'<p>Senior Consultant, Atlas Foundation</p>' +
						'</div>' +
					'</div>';
		var item2 = '<div class="media">' +
						'<a class="pull-left" href="javascript:void(0);" title="Erik Adigard">' +
							'<img class="media-object" src="../images/upload/avatar-9.jpg" alt="Avatar">' +
						'</a>' +
						'<div class="media-body">' +
								'<h4 class="media-heading"><a href="javascript:void(0);" title="David Schole" class="color-5">David Schole</a></h4>' +
								'<p>Senior Consultant, Atlas Foundation</p>' +
						'</div>' +
					'</div>';
		var item3 = '<div class="media">' +
						'<a class="pull-left" href="javascript:void(0);" title="Erik Adigard">' +
							'<img class="media-object" src="../images/upload/avatar-9.jpg" alt="Avatar">' +
						'</a>' +
						'<div class="media-body">' +
								'<h4 class="media-heading"><a href="javascript:void(0);" title="Dann Lee" class="color-5">Dann Lee</a></h4>' +
								'<p>Senior Consultant, Atlas Foundation</p>' +
						'</div>' +
					'</div>';
		var item4 = '<div class="media">' +
						'<a class="pull-left" href="javascript:void(0);" title="Erik Adigard">' +
							'<img class="media-object" src="../images/upload/avatar-9.jpg" alt="Avatar">' +
						'</a>' +
						'<div class="media-body">' +
								'<h4 class="media-heading"><a href="javascript:void(0);" title="Frank Micheal" class="color-5">Frank Micheal</a></h4>' +
								'<p>Senior Consultant, Atlas Foundation</p>' +
						'</div>' +
					'</div>';
		var availableTags = [
				"<div style='height: 50px'>ActionScript</div>",
				"AppleScript",
				"Asp",
				"BASIC",
				"C",
				"C++",
				"Clojure",
				"COBOL",
				"ColdFusion",
				"Erlang",
				"Fortran",
				"Groovy",
				"Haskell",
				"Java",
				"JavaScript",
				"Lisp",
				"Perl",
				"PHP",
				"Python",
				"Ruby",
				"Scala",
				"Scheme",
				"Drupal Worpress",
				".Net"
		];
		availableTags = [item1, item2, item3, item4];
		$("#address-text").smAutocomplete({
			//source: availableTags,
			urlAjax: $('#address-autocomplete').val(),
			appendTo: 'body',
			autoFocus: true,
			activeClass: 'active',
			typeData: 'json',
			remote: true,
			multipleValue: true,
			change:function(){
				//console.log('abc');
			}
		});
	};
	function initBookmarkForm(){
		var form = $('#gblock-popup-bookmark-form'),
			input = $('#name'),
			container = $('#tags'),
			addBnt = $('#btn-add'),
			hidden = $('#all_name'),
			errText = null,
			infoText = null,
			successText = null;
		initAddTagFunction(input, container, addBnt, hidden);
		errText = form.find('.text-error');
		infoText = form.find('.text-info');
		successText = form.find('.text-success');

		errText.addClass('hidden');
		infoText.addClass('hidden');
		successText.addClass('hidden');

		var btnSubmit = form.find('#btn-submit');
		var btnCancel = form.find('#btn-cancel');



		btnSubmit.unbind('click.bookmarkfrm').bind('click.bookmarkfrm', function(){
			if (hidden.val() == ''){
				hidden.val(input.val());
			}
			$.ajax({
				'url':$('#ajax_link').val(),
				'type': 'post',
				'data':{
					'all_name':$('#all_name').val(),
					'data_entity_type':$('#data_entity_type').val(),
					'data_bundle':$('#data_bundle').val(),
					'data_entity_id':$('#data_entity_id').val()
				},
				success: function(result){
					if(result == 1){
						successText.removeClass('hidden');
						container.children().remove();
						hidden.val('');
					} else{
						errText.removeClass('hidden');
					}
				},
				error: function() {
					errText.removeClass('hidden');
				}
			});
			return false;
		});

		btnCancel.unbind('click.bookmarkfrm').bind('click.bookmarkfrm', function(e){
			form.find('.popover').fadeOut(200);
			return false;
		});
	};
	function initAddTagFunction(input, container, addBnt, hidden){
		container.delegate('a.btn').click(function (e) {
			e.preventDefault();
			if(e.target.tagName.toLowerCase() == 'a'){
				var targetVal = decodeURIComponent($(e.target).attr('data-content'));
				var hiddenVal = hidden.val();
				var val = hiddenVal.replace(targetVal + ',', '');
				if(val == hiddenVal) {
					val = val.replace(',' + targetVal, '');
					if(val == hiddenVal) val = val.replace(targetVal, '');
				}
				$(e.target).remove();
				hidden.val(val);
			}
		});

		input.keydown(function (e) {
			if ((e.keyCode || e.which) == 13) {
				$(this).blur();
				addBnt.click();
			}
		});

		addBnt.click(function (e) {
			e.preventDefault();
			if(input.val()==input.attr('placeholder')) return;
			var arrText = $.trim(input.val()).split(',');
			for (var i = 0, l = arrText.length; i < l; i++) {
				var text = $.trim(arrText[i]);
				if (text.length > 0 && (findAttr(container.find('a'), encodeURIComponent(text)))) {
					container.append('<a data-content="' + encodeURIComponent(text) + '" href="javascript:void(0);" title="' + text + '" class="btn btn-default">' + '<i class="icon-ban-circle"></i>' + text + '</a>');
					if (hidden.val() == '')
						hidden.val(text);
					else hidden.val(hidden.val() + ',' + text);
				}
			}
			input.val((ie && !ie10 && input.attr('placeholder')) || '');
		});

		function findAttr(list, selector) {
			for (var i = 0; i < list.length; i++) {
				if (list.eq(i).attr('data-content') == selector)
					return false;
			}
			return true;
		};
	};
	function initBookmarkBtn(){
		var form = $('#gblock-popup-bookmark-form');
		var divBookmark = $('#bookmark-region .popover');

		$('a.btn-bookmark').unbind('click.showpopover').bind('click.showpopover', function(e){
			divBookmark.find('.text-error').addClass('hidden');
			divBookmark.find('.text-info').addClass('hidden');
			divBookmark.find('.text-success').addClass('hidden');

			divBookmark.css({
				'left': $(this).offset().left - divBookmark.outerWidth()/2 + $(this).outerWidth()/2,
				'top': $(this).offset().top + $(this).outerHeight()
			});
			$('#data_entity_type').val($(this).attr('data-entity-type'));
			$('#data_bundle').val($(this).attr('data-bundle'));
			$('#data_entity_id').val($(this).attr('data-entity-id'));
			divBookmark.css({
				'display':'block',
				'opacity':0
			}).animate({
				'opacity':1
			},200);
			return false;
		})
	};
	function initEditBookmarkFunction(){
		$('.bookmark').on('shown', function (e) {
		   var editBtn = $(this).next().find('.color-6').filter(function(){return this.title=="Edit"});
		   var deleteBtn = editBtn.next();
		   editBtn.unbind('click').bind('click', function() {
		   		var spanText = $(this).parent().next();
		   		var textInput = spanText.children().filter('input');
		   		var aIcon = spanText.children().filter('a');
		   		var bkmNameTag = spanText.siblings().first();
		   		var saveBkmUrl = $(this).siblings('#save_bookmark');

		   		aIcon.css('display', 'none');
		   		spanText.removeClass('hidden');
		   		textInput.focus().select();

		   		textInput.unbind('blur.bookmark').bind('blur.bookmark', function(){
		   			$.ajax({
		   				'url': saveBkmUrl.val(),
		   				'type': 'post',
		   				'data':{
							'name': textInput.val()
						},
		   				success: function(result) {
		   					if (result == 1){
		   						bkmNameTag.text(textInput.val());
		   						aIcon.fadeIn();
		   					}
		   				}
		   			})
		   		});
		   });

		   deleteBtn.unbind('click').bind('click', function() {
		   		var elem = $(this);
		   		var removBkmUrl = $(this).siblings('#remove_bookmark');
				var conf = confirm(elem.data('confirm-text'));
				if (!conf) {
					return;
				};
		   		$.ajax({
	   				'url': removBkmUrl.val(),
	   				'type': 'post',
	   				success: function(result) {
	   					if (result == 1){
	   						elem.closest('li').remove();
	   					}
	   				}
	   			})
		   })
		});
	};
	function initListMsgForm(){
		var inboxForm = $('#my-messages-form');
		var actionBtns = inboxForm.find('a').filter(function(){
			var type = $(this).data('action');
			return (type == 'delete'||type == 'mark-as-read'||type == 'mark-as-unread');
		})
		actionBtns.unbind('click').bind('click', function(){
			var checkedItem = inboxForm.find(':checkbox').filter(function(){
				return $(this).is(':checked');
			});
			console.log(checkedItem);			
		})
	};
	return {
		init : function () {
			initFormRegister();
			initFormEditProfile();
			initFormEditAccount();
			initFormChangePassword();
			initFormCreateGroup();
			ajaxLinkProcess();
			accodionPlus();
			loadPeopleInfo();
			initVideoPlayer();
			initSendMsgForm();
			initBookmarkForm();
			initBookmarkBtn();
			initEditBookmarkFunction();
			initListMsgForm();
		}
	};
})(jQuery);

jQuery(document).ready(function ($) {

	$('.bookmark').popover({
		// options = $.extend({}, $.fn[this.type].defaults, options, this.$element.data());
	});
	$('.showpopover').popover();

	$(".collapse").collapse();
	$('.carousel').carousel();
	$('.popup-pwd').popover();
	$('.showmore').click(function (e) {
		e.preventDefault();
		$(this).parent().hide().next().removeClass('hidden');
	});
	Placeholder.init();
	Gain.init();
});
