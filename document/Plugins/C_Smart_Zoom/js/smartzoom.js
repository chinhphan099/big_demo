;(function($) {
  'use strict';
  var pluginName = 'smartzoom';

  function format(str) {
    for (var i = 1; i < arguments.length; i++) {
      str = str.replace('%' + (i - 1), arguments[i]);
    }
    return str;
  }

  function SmartZoom(jWin, opts) {
    var sImg = $('img', jWin),
      img1,
      img2,
      zoomDiv = null,
      $mouseTrap = null,
      lens = null,
      $tint = null,
      softFocus = null,
      $ie6Fix = null,
      zoomImage,
      controlTimer = 0,
      cw, ch,
      destU = 0,
      destV = 0,
      currV = 0,
      currU = 0,
      filesLoaded = 0,
      mx, my,
      ctx = this, zw;

    var ie6FixRemove = function() {
      if ($ie6Fix !== null) {
        $ie6Fix.remove();
        $ie6Fix = null;
      }
    };

    this.removeBits = function() {
      //$mouseTrap.unbind();
      if (lens) {
        lens.remove();
        lens = null;
      }

      if ($tint) {
        $tint.remove();
        $tint = null;
      }

      if (softFocus) {
        softFocus.remove();
        softFocus = null;
      }

      ie6FixRemove();

      $('.cloud-zoom-loading', jWin.parent()).remove();
    };

    this.destroy = function() {
      jWin.data('zoom', null);

      if ($mouseTrap) {
        $mouseTrap.unbind();
        $mouseTrap.remove();
        $mouseTrap = null;
      }

      if (zoomDiv) {
        zoomDiv.remove();
        zoomDiv = null;
      }

      //ie6FixRemove();
      this.removeBits();
    };

    this.fadedOut = function() {
      if (zoomDiv) {
        zoomDiv.remove();
        zoomDiv = null;
      }

      this.removeBits();
      //ie6FixRemove();
    };

    this.controlLoop = function() {
      if (lens) {
        var x = (mx - sImg.offset().left - (cw * 0.5)) >> 0,
          y = (my - sImg.offset().top - (ch * 0.5)) >> 0;

        if (x < 0) {
          x = 0;
        }
        else if (x > (sImg.outerWidth() - cw)) {
          x = (sImg.outerWidth() - cw);
        }

        if (y < 0) {
          y = 0;
        }
        else if (y > (sImg.outerHeight() - ch)) {
          y = (sImg.outerHeight() - ch);
        }

        lens.css({
          left: x,
          top: y
        });
        lens.css('background-position', (-x) + 'px ' + (-y) + 'px');

        destU = (((x) / sImg.outerWidth()) * zoomImage.width) >> 0;
        destV = (((y) / sImg.outerHeight()) * zoomImage.height) >> 0;
        currU += (destU - currU) / opts.smoothMove;
        currV += (destV - currV) / opts.smoothMove;

        zoomDiv.css('background-position', (-(currU >> 0) + 'px ') + (-(currV >> 0) + 'px'));
      }
      controlTimer = setTimeout(function() {
        ctx.controlLoop();
      }, 30);
    };

    this.init2 = function(img, id) {
      filesLoaded++;
      if (id === 1) {
        zoomImage = img;
      }

      if (filesLoaded === 2) {
        this.init();
      }
    };

    this.init = function() {
      $('.cloud-zoom-loading', jWin.parent()).remove();

      var $m = $('<div class="mousetrap"></div>');
      $mouseTrap = jWin.parent().append($m).find(':last');
      $mouseTrap = $('.mousetrap', jWin.parent());

      $mouseTrap.bind('mousemove touchmove', this, function(event) {
        if(event.type === 'touchstart' || event.type === 'touchmove' || event.type === 'touchend' || event.type === 'touchcancel'){
          var touch = event.originalEvent.touches[0] || event.originalEvent.changedTouches[0];
          mx = touch.pageX;
          my = touch.pageY;
        }
        else if (event.type === 'mousedown' || event.type === 'mouseup' || event.type === 'mousemove' || event.type === 'mouseover'|| event.type === 'mouseout' || event.type === 'mouseenter' || event.type === 'mouseleave') {
          mx = event.pageX;
          my = event.pageY;
        }

        document.ontouchmove = function(e){
          e.preventDefault();
        };
      });

      $mouseTrap.bind('mouseleave touchend', this, function() {
        clearTimeout(controlTimer);
        //event.data.removeBits();
        if(lens) {
          lens.fadeOut(299);
        }

        if($tint) {
          $tint.fadeOut(299);
        }

        if(softFocus) {
          softFocus.fadeOut(299);
        }

        if (zoomDiv) {
          zoomDiv.fadeOut(300, function() {
            ctx.fadedOut();
          });
        }

        document.ontouchmove = function(){return true;};

        return false;
      });

      $mouseTrap.bind('mouseenter touchstart', this, function(event) {
        if(event.type === 'touchstart' || event.type === 'touchmove' || event.type === 'touchend' || event.type === 'touchcancel'){
          var touch = event.originalEvent.touches[0] || event.originalEvent.changedTouches[0];
          mx = touch.pageX;
          my = touch.pageY;
        }
        else if (event.type === 'mousedown' || event.type === 'mouseup' || event.type === 'mousemove' || event.type === 'mouseover'|| event.type === 'mouseout' || event.type === 'mouseenter' || event.type === 'mouseleave') {
          mx = event.pageX;
          my = event.pageY;
        }

        zw = event.data;
        if (zoomDiv) {
          zoomDiv.stop(true, false);
          zoomDiv.remove();
        }

        var xPos = opts.adjustX,
          yPos = opts.adjustY,
          siw = sImg.outerWidth(),
          sih = sImg.outerHeight(),
          w = opts.zoomWidth,
          h = opts.zoomHeight;

        if (opts.zoomWidth === 'auto') {w = siw;}
        if (opts.zoomHeight === 'auto') {h = sih;}

        var appendTo = jWin.parent(); // attach to the wrapper
        switch (opts.position) {
          case 'top':
            yPos -= h; // + opts.adjustY;
            break;
          case 'right':
            xPos += siw; // + opts.adjustX;
            break;
          case 'bottom':
            yPos += sih; // + opts.adjustY;
            break;
          case 'left':
            xPos -= w; // + opts.adjustX;
            break;
          case 'inside':
            w = siw;
            h = sih;
            break;
          default:
            appendTo = $('#' + opts.position);
            if (!appendTo.length) {
              appendTo = jWin;
              xPos += siw; //+ opts.adjustX;
              yPos += sih; // + opts.adjustY;
            } else {
              w = appendTo.innerWidth();
              h = appendTo.innerHeight();
            }
        }

        zoomDiv = appendTo.append(format('<div class="smart-zoom-big" style="display:none;left:%0px;top:%1px;width:%2px;height:%3px;background-image:url(\'%4\');"></div>', xPos, yPos, w, h, zoomImage.src)).find(':last');
        zoomDiv = $('.smart-zoom-big', appendTo);

        if (sImg.attr('title') && opts.showTitle) {
          zoomDiv.append(format('<div class="smart-zoom-title">%0</div>', sImg.attr('title')));
          $('.smart-zoom-title', zoomDiv).css('opacity', opts.titleOpacity);
        }

        var browserCheck = /(msie) ([\w.]+)/.exec(navigator.userAgent);
        if (browserCheck) {
          if ((browserCheck[1] || '') === 'msie' && (browserCheck[2] || '0' ) < 7) {
            $ie6Fix = $('<iframe frameborder="0" src="#"></iframe>').css({
              position: 'absolute',
              left: xPos,
              top: yPos,
              zIndex: 99,
              width: w,
              height: h
            }).insertBefore(zoomDiv);
          }
        }

        zoomDiv.fadeIn(500);

        if (lens) {
          lens.remove();
          lens = null;
        }

        cw = (sImg.outerWidth() / zoomImage.width) * zoomDiv.width();
        ch = (sImg.outerHeight() / zoomImage.height) * zoomDiv.height();

        lens = jWin.append(format('<div class="smart-zoom-lens" style="width:%0px;height:%1px;"></div>', cw, ch)).find(':last');
        lens = $('.smart-zoom-lens', jWin);

        var noTrans = false;

        if (opts.tint) {
          lens.css('background', 'url("' + sImg.attr('src') + '")');
          $tint = jWin.append(format('<div class="smart-zoom-tint" style="width:%0px; height:%1px; background-color:%2;" />', sImg.outerWidth(), sImg.outerHeight(), opts.tint)).find(':last');
          $tint = $('.smart-zoom-tint',jWin);

          $tint.css('opacity', opts.tintOpacity);
          noTrans = true;
          $tint.fadeIn(500);
        }

        if (opts.softFocus) {
          lens.css('background', 'url("' + sImg.attr('src') + '")');
          jWin.append(format('<div class="smart-zoom-soft" style="width:%0px; height:%1px;" />', sImg.outerWidth() - 2, sImg.outerHeight() - 2, opts.tint));
          softFocus = $('.smart-zoom-soft', jWin);
          softFocus.css('background', 'url("' + sImg.attr('src') + '")');
          softFocus.css('opacity', 0.5);
          noTrans = true;
          softFocus.fadeIn(500);
        }

        if (!noTrans) {lens.css('opacity', opts.lensOpacity);}
        if ( opts.position !== 'inside' ) {lens.fadeIn(500);}

        // Start processing.
        zw.controlLoop();

        document.ontouchmove = function(e) {e.preventDefault();};

        return;
      });
    };

    img1 = new Image();
    $(img1).load(function() {ctx.init2(this, 0);});
    img1.src = sImg.attr('src');

    img2 = new Image();
    $(img2).load(function() {ctx.init2(this, 1);});
    img2.src = jWin.attr('href');
  }

  $.fn[pluginName] = function(options) {
    try {
      document.execCommand('BackgroundImageCache', false, true);
    }
    catch (e) {}

    this.each(function() {
      var relOpts, opts, a;
      eval('a = {' + $(this).attr('rel') + '}');
      relOpts = a;
      if ($(this).data('smartzoom') === '') {
        if ($(this).parent().attr('class') !== 'smart-wrap') {
          $(this).wrap('<div class="smart-wrap"></div>');
        }

        opts = $.extend({}, $.fn[pluginName].defaults, options);
        opts = $.extend({}, opts, relOpts);
        $(this).data('zoom', new SmartZoom($(this), opts));
      }
      else if ($(this).data('smartzoom') === 'gallery') {
        opts = $.extend({}, relOpts, options);
        $(this).data('relOpts', opts);
        $(this).bind('click.changeSmartZoomImage', $(this), function(event) {
          event.preventDefault();
          var data = event.data.data('relOpts');

          $('#' + data.useZoom).data('zoom').destroy();
          $('#' + data.useZoom).attr('href', event.data.attr('href'));
          $('#' + data.useZoom + ' img').animate({'opacity': '0.2'}, function() {
            $(this).attr('src', event.data.data('relOpts').smallImage).animate({'opacity': '1'}, 'fast', function() {
              $('#' + event.data.data('relOpts').useZoom)[pluginName]();
            });
          });

          return false;
        });
      }
    });
    return this;
  };

  $.fn[pluginName].defaults = {
    zoomWidth: 'auto',
    zoomHeight: 'auto',
    position: 'right',
    tint: false,
    tintOpacity: 0.5,
    lensOpacity: 0.5,
    softFocus: false,
    smoothMove: 3,
    showTitle: true,
    titleOpacity: 0.5,
    adjustX: 0,
    adjustY: 0
  };

  $(function() {
    $('[data-' + pluginName + ']')[pluginName]();
  });

})(jQuery);
