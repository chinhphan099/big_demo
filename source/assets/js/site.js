/**
 * @name Site
 * @description Global variables and functions
 * @version 1.0
 */

var Site = (function($, window, undefined) {
  'use strict';

  var win = $(window),
    doc = $(document),
    //standarTransitionend = (!!window.URL || !!window.webkitURL) ? 'webkitTransitionEnd' : 'transitionend';
    resize = ('onorientationchange' in window) ? 'orientationchange' : 'resize';

  var userAgent = navigator.userAgent,
    MOBILE = {
      isAndroid: function() {
        return userAgent.match(/Android/i) ? true : false;
      },
      isBlackBerry: function() {
        return userAgent.match(/BlackBerry|BB/i) ? true : false;
      },
      isiOS: function(name) {
        if (name === 'iPhone') {
          return userAgent.match(/iPhone|iPod/i) ? true : false;
        }
        else {
          if (name === 'iPad') {
            return userAgent.match(/iPad/i) ? true : false;
          }
          else {
            return userAgent.match(/iPhone|iPod|iPad/i) ? true : false;
          }
        }
      },
      isWindows: function() {
        return userAgent.match(/IEMobile/i) ? true : false;
      },
      isMobile: function() {
        return (MOBILE.isAndroid() || MOBILE.isBlackBerry() || MOBILE.isiOS('iPhone') || MOBILE.isWindows());
      },
      isAny: function() {
        return (MOBILE.isAndroid() || MOBILE.isBlackBerry() || MOBILE.isiOS() || MOBILE.isWindows());
      }
    };
  console.log('Mobile: ' + MOBILE.isAny());

  var topInner = 0;
// globalFct
  var globalFct = function() {
  // Tab call Ajax
    $('.tab').off('click.loadAjax', 'li a', loadAjax).on('click.loadAjax', 'li a', loadAjax);

  // Loading
    var start;
    clearTimeout(start);
    start = setTimeout(function(){
      $('.loading').fadeOut();
    }, 1000);

  // Check IMG Loaded
    imgIsLoaded($('.grade-selection img'), function() {console.log('imgIsLoaded(elm, callback)');});

  // Header
    getTopDocument();
    $('#header').on('tap click', function() {
      $('.wrapper').toggleClass('opening');
      if($('.wrapper').hasClass('opening')) {
        win.off('scroll.win');
        $('.container > .inner').css({'top': -topInner});
      }
      else {
        getTopDocument();
        window.scrollBy(0, topInner);
      }
    });

  // Date value
    setDateValue();
    $('input[type="date"]').on('change.date', function() {
      var data = new Date($(this).val());

      //console.log(data);
      if(data !== '') {
        $(this)
          .data('date', getDateValue(data))
          .attr('data-date', getDateValue(data));
      }
    });

  // TransitionEnd Event
    console.log('transEndEventName() -> ' + transEndEventName());

  // Resize Window Event
    win.on(resize + '.window', function() {
    });
  };

// Detect transitionend Event
  var transEndEventName = function() {
    var fakeElm = document.createElement('fake'),
      transEndEventNames = {
        'WebkitTransition' : 'webkitTransitionEnd',// Saf 6, Android Browser
        'MozTransition'    : 'transitionend',      // only for FF < 15
        'transition'       : 'transitionend'       // IE10, Opera, Chrome, FF 15+, Saf 7+
      };

    for(var key in transEndEventNames){
      if(fakeElm.style[key] !== undefined) {
        return transEndEventNames[key];
      }
    }
  };

// Check IMG Load
  var imgIsLoaded = function(elm, callback) {
    // $('.grade-selection img') -> elm
    var images_loaded = 0,
      total_images = elm.length;

    elm.each(function() {
      var fakeSrc = $(this).attr('src');

      $('<img/>')
        .attr('src', fakeSrc).css('display', 'none')
        .load(function() {
          images_loaded++;
          if (images_loaded === total_images) {
            $.isFunction(callback) && callback();
          }
        })
        .error(function(){
          images_loaded++;
          if (images_loaded === total_images) {
            $.isFunction(callback) && callback();
          }
        });
    });
  };

// getTopDocument
  var getTopDocument = function (){
    win.on('scroll.win', function(){
      topInner = win.scrollTop();
    });
  };

// Date value
  var setDateValue = function() {
    $('input[type="date"]').each(function() {
      var str = $(this).data('date').toString(),
        dd = str.slice(0, 2),
        mm = str.slice(2, 4),
        yyyy = str.slice(4, 8);

      $(this).val(yyyy + '-' + mm + '-' + dd);
    });
  };

  var getDateValue = function(data) {
    var dd = data.getDate(),
      mm = data.getMonth() + 1,
      yyyy = data.getFullYear();

    if(dd < 10) {
      dd = '0' + dd;
    }
    if(mm < 10) {
      mm = '0' + mm;
    }

    return dd.toString() + mm.toString() + yyyy.toString();
  };

//- Cookie
  var createCookie = function(name, value, days) {
    var expires = '';
    if(days) {
      var date = new Date();
      date.setTime(date.getTime() + (days*24*60*60*1000));
      expires = '; expires=' + date.toGMTString();
    }
    document.cookie = name + '=' + value + expires + '; path=/';
  };

  var readCookie = function(name) {
    var nameEQ = name + '=',
      ca = document.cookie.split(';');

    for(var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) === ' ') {
        c = c.substring(1,c.length);
      }
      if (c.indexOf(nameEQ) === 0) {
        return c.substring(nameEQ.length,c.length);
      }
    }
    return null;
  };

  var eraseCookie = function(name) {
    createCookie(name, '', -1);
  };
  console.log(createCookie(), readCookie(), eraseCookie());

// Load Tab Ajax
  var loadAjax = function() {
    $('.ajax-contents').empty().append('Loading');
    $.ajax({
      type: 'get',
      url: this.href,
      success: function(data){
        $('.ajax-contents').fadeOut().empty().append(data).hide().slideDown();
      }
    });
    return false;
  };

// Ajax
  var callAjax = function() {
    //Load RSS
    $.ajax({
      type: 'GET',
      url: '../data/data.rss',
      dataType: 'text',
      success: function(xml){
        var item = $(xml).find('item');
        for(var i = 0, n = item.length; i < n - n + 5; i++){
          var title = $(item[i]).find('title').text();
          $('#rss-list').append('<li>'+title+'</li>');
        }
        //Each loop
        /*$(xml).find('item').each(function(index){
          if(index <= 5) {
            var title = $(this).find('title').text();
            $('#rss-list').append('<li>'+title+'</li>');
          }
        });*/
      },
      error: function(){
        $('#rss-list').append('<li>Load False</li>');
      }
    });

    //Load RSS Online link
    var feed = 'http://vnexpress.net/rss/the-thao.rss';
    $.ajax({
      type: 'GET',
      url: document.location.protocol + '//ajax.googleapis.com/ajax/services/feed/load?v=1.0&num=-1&callback=?&q=' + encodeURIComponent(feed),
      dataType: 'jsonp',
      success: function(xml){
        var item = xml.responseData.feed.entries;
        for(var i = 0, n = item.length; i < n - n + 5; i++){
          $('#rss-online-list').append('<li>'+item[i].title+'</li>');
        }
      },
      error: function(){
        $('#rss-online-list').append('--- Load Fail ---');
      }
    });

    //Load XML
    $.ajax({
      type: 'GET',
      url: '../data/data.xml',
      dataType: 'text',
      success: function(xml){
        $(xml).find('member').each(function(){
          var id = $(this).find('id').text();
          var name = $(this).find('name').text();
          $('#xml-list').append('<li>'+id +' '+name+'</li>');
        });
      },
      error: function(){
        $('#xml-list').append('--- Load Fail ---');
      }
    });

    //Load JSON Local
    $.ajax({
      type: 'GET',
      url: '../data/data.json',
      dataType: 'text',
      success: function(data){
        var json = $.parseJSON(data);
        // Object.keys(obj).length // Get length of Obj
        for(var i = 0, n = json.name.length; i < n; i++){
          $('#json-list').append('<li>'+json.name[i].first+'</li>');
        }
      },
      error: function(){
        $('#json-list').append('--- Load Fail ---');
      }
    });

    //Load JSON Online link
    $.ajax({
      url: 'http://bigeye.vn/ajax/getdata1.php?mod=product',
      dataType: 'text',
      success: function(data){
        var json = $.parseJSON(data);
        for(var i = 0, n = json.length; i < n - n + 5; i++){
          $('#img-list').append('<li>'+json[i].image+'</li>');
          // $('#img-list').append('<li><img src="http://bigeye.vn/phn_image/product/' + json[i].image + '"></li>');
          //<img src="http://bigeye.vn/phn_image/product/
        }
      },
      error: function(){
        $('#img-list').append('--- Load Fail ---');
      }
    });
  };

// handlebarJS
  var handlebarJS = {
    compileData: function(resJSON) {
      var templateSource = $('.source-handle').html(),
        template = Handlebars.compile(templateSource),
        studentHTML = template(resJSON);
      $('.handlebar').html('').html(studentHTML);
    },
    loadJson: function() {
      if($('.source-handle').length) {
        $.ajax({
          url: $('.source-handle').data('json-url'),
          method: 'get',
          success: handlebarJS.compileData
        });
        doc.on('complete.Handlebars', function() {
          console.log(0);
        });
      }
    }
  };

// Return
  return {
    win: win,
    resize: resize,
    globalFct: globalFct,
    imgIsLoaded: imgIsLoaded,
    callAjax: callAjax,
    handlebarJS: handlebarJS.loadJson
  };

})(jQuery, window);

jQuery(function() {
  Site.globalFct();
  Site.callAjax();
  Site.handlebarJS();
});
