/**
 *  @name validates
 *  @description Use jQuery Validation
 *  @version 1.0
 *  @options
 *    option
 *  @methods
 *    init
 *    reset
 */

;(function($, window, undefined) {
  'use strict';

  var pluginName = 'validates';
  var getRules = function(formId) {
    var rules = {};
    switch (formId) {
      case 'business_form':
        rules = $.extend(rules, {
          business_name: {required: true},
          select_2: {required: true},
          select_3: {required: true},
          locate: {required: true}
        });
        break;
      default:
    }
    return rules;
  },
  messages = function(formId) {
    var messages = {};
    switch (formId) {
      case 'business_form':
        messages = $.extend(messages, {
          business_name: {required: L10n.validateMess.required},
          select_2: {required: L10n.validateMess.required},
          select_3: {required: L10n.validateMess.required},
          locate: {required: L10n.validateMess.required}
        });
        break;
      default:
    }
    return messages;
  };

  function Plugin(element, options) {
    this.element = $(element);
    this.options = $.extend({}, $.fn[pluginName].defaults, this.element.data(), options);
    this.init();
  }

  Plugin.prototype = {
    init: function() {
      var that = this;
      this.formId = this.element.attr('id');

      $('#' + this.formId).validate({
        rules: getRules.call(that, that.formId),
        messages: messages.call(that, that.formId),
        errorElement: 'p',
        highlight: function(element) {
          $(element).addClass('error').closest('fieldset').addClass('errors');
        },
        unhighlight: function(element) {
          $(element).removeClass('error');
          if(!$(element).closest('fieldset').find(that.options.groupCls).hasClass('error')) {
            $(element).removeClass('error').closest('fieldset').removeClass('errors');
          }
        },
        errorPlacement: function(error, element) {
          if ($(element).is('select')) {
            error.insertAfter(element.closest('.custom-select'));
          }
          else if ($(element).closest('.custom-input').length) {
            error.insertAfter(element.closest('.custom-input'));
          }
          else if ($(element).closest('.radio-list').length) {
            error.insertAfter(element.closest('.radio-list'));
          }
          else if($(element).attr('type') === 'date') {
            error.insertAfter(element.closest('.custom-date'));
            return false;
          }
          else if($(element).attr('type') === 'file') {
            error.insertAfter(element.closest('.custom-file'));
            return false;
          }
          else if($(element).is(':checkbox')) {
            error.insertAfter(element.closest('.custom-checkbox'));
          }
          else if($(element).is(':radio')) {
            error.insertAfter(element.closest('.radio-group'));
          }
          else {
            error.insertAfter(element);
          }
        }
      });
    },
    reset: function() {
      $('#' + this.formId).resetForm();
    }
  };

  $.fn[pluginName] = function(options, params) {
    return this.each(function() {
      var instance = $.data(this, pluginName);
      if (!instance) {
        $.data(this, pluginName, new Plugin(this, options));
      } else if (instance[options]) {
        instance[options](params);
      }
    });
  };

  $.fn[pluginName].defaults = {
    groupCls: '.valid-group'
  };

  $(function() {
    $('[data-' + pluginName + ']')[pluginName]();
  });

}(jQuery, window));
