/**
 *  @name loaddata
 *  @description description
 *  @version 1.0
 *  @options
 *    option
 *  @events
 *    event
 *  @methods
 *    init
 *    publicMethod
 *    destroy
 */
;(function($, window, undefined) {
  'use strict';

  var pluginName = 'loadmore',
    win = $(window);

  function Plugin(element, options) {
    this.element = $(element);
    this.options = $.extend({}, $.fn[pluginName].defaults, this.element.data(), options);
    this.init();
  }

  Plugin.prototype = {
    init: function() {
      var that = this;
      this.var = {
        startAt: 0,
        isLoading: false,
        isEnd: false,
        init: 0,
        count: this.options[pluginName].count
      };
      this.template =
        '<div class="col-xs-4 animated fadeIn">' +
          '<a href="{{url}}" title="{{productName}}" class="item-cate">' +
              '<div class="tmb-img">' +
                '<img src="{{thumbnail}}" alt="{{productName}}" title="{{productName}}">' +
                '<i class="icon-heart"></i>' +
                '<span class="discount">{{discount}}</span>' +
              '</div>' +
              '<div class="info">' +
                '<p class="price">' +
                  '<span class="new-price">{{newPrice}}</span>' +
                  '<span class="old-price">{{oldPrice}}</span>' +
                '</p>' +
                '<p class="title">{{productName}}</p>' +
                '<p class="rating">' +
                  '<span class="star" data-star="{{star}}">' +
                    '<i class="icon-star"></i>' +
                    '<i class="icon-star"></i>' +
                    '<i class="icon-star"></i>' +
                    '<i class="icon-star"></i>' +
                    '<i class="icon-star"></i>' +
                  '</span>' +
                  '<span class="count">({{count}})</span>' +
                  '<span class="statistic">{{turnBuy}} Lượt mua</span>' +
                '</p>' +
              '</div>' +
            '</a>' +
          '</div>';

      setTimeout(function() {
        win.scroll(function() {
          var bottomList = that.element.height() + that.element.offset().top,
            offsetWin = win.scrollTop() + win.height();

          if(bottomList < offsetWin - 150 && !that.var.isEnd && !that.var.isLoading) {
            that.var.isLoading = true;
            that.loadData();
          }
        });
      }, 600);
    },
    loadData: function() {
      var that = this, i, item;

      $('#load-product').removeClass('hidden');
      $.ajax({
        type: 'GET',
        url: that.options[pluginName].url,
        dataType: 'json',
        success: function(data) {
          that.var.init += that.var.count;
          for(i = that.var.startAt; i < that.var.init; i++) {
            if(i === data.length) {
              that.var.isEnd = true;
              $('#load-product').addClass('hidden');
              return false;
            }

            item = that.template;
            item = item.replace(/\{\{url\}\}/, data[i].url);
            item = item.replace(/\{\{productName\}\}/gi, data[i].productName);
            item = item.replace(/\{\{thumbnail\}\}/, data[i].thumbnail);
            item = item.replace(/\{\{discount\}\}/, data[i].discount);
            item = item.replace(/\{\{newPrice\}\}/, data[i].newPrice);
            item = item.replace(/\{\{oldPrice\}\}/, data[i].oldPrice);
            item = item.replace(/\{\{star\}\}/, data[i].star);
            item = item.replace(/\{\{count\}\}/, data[i].count);
            item = item.replace(/\{\{turnBuy\}\}/, data[i].turnBuy);
            that.element.append(item);

            $.isFunction($('[data-star]').star()) && $('[data-star]').star();
          }
          that.var.startAt = i;
          that.var.isLoading = false;
          $('#load-product').addClass('hidden');
        },
        error: function() {
          that.var.isEnd = true;
          //that.element.append('<p class="grid">Kết nối thất bại !</p>');
          $('#load-product').addClass('hidden');
          that.var.isLoading = false;
        }
      });
    },
    destroy: function() {
      $.removeData(this.element[0], pluginName);
    }
  };

  $.fn[pluginName] = function(options, params) {
    return this.each(function() {
      var instance = $.data(this, pluginName);
      if (!instance) {
        $.data(this, pluginName, new Plugin(this, options));
      } else if (instance[options]) {
        instance[options](params);
      }
    });
  };

  $.fn[pluginName].defaults = {};

  $(function() {
    win.ready(function() {
      $('[data-' + pluginName + ']')[pluginName]();
    });
  });

}(jQuery, window));
